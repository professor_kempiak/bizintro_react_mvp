# Bizintro

To run this application, execute the following commands:

  1. Install NPM modules

    ```
    $ npm install
    ```

  2. Start the server in production mode:

    ```
    $ npm start
    ```

  3. Open another terminal window and start the front end application in development mode:

    ```
    $ npm run dev
    ```
