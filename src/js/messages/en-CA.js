module.exports = {
  Grommet: 'Grommet',
  Username: 'Username',
  Password: 'Password',
  Login: 'Login',
  Skip: 'Skip',
  Tasks: 'Tasks',
  'Remember me': 'Remember me',
  'Log In': 'Log In',
  'Skip to': 'Skip to',
  'Gateway Timeout': 'Gateway Timeout',
};
