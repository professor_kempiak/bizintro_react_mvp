export const CONFIG = {
	clientId : '278687638784-1dc2761h9c2dbljtu6o78bihma7glaie.apps.googleusercontent.com',
	redirectUri : process.env.NODE_ENV === 'development' ? 'http://localhost:3000/google_return' : 'http://mvp.bizintro.com/google_return',
	redirectUriBIZ: 'http://mvp.bizintro.com/google_return'
	// clientId : '345306632996-4e6na6efsvi30oq140hdo9pjltsv7nak.apps.googleusercontent.com',
	// redirectUri : process.env.NODE_ENV === 'development' ? 'http://localhost:3000/google_return' : 'http://mvp.bizintro.com/google_return',
	// redirectUriBIZ: 'http://mvp.bizintro.com/google_return'
}
