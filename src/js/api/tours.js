import { headers, parseJSON } from './utils';

export function fetchTours(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };
    return fetch('/api/tours', options).then(parseJSON);
}