import { headers, parseJSON } from './utils';

export function getIntroductionList(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };
    return fetch('/api/introduction_list', options).then(parseJSON);
}


export function getIntroductionStatus(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };
    return fetch('/api/manageIntroductions', options).then(parseJSON);
}

export function getIntroductionsByPage(token, page, perpage) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token, page, perpage })
    };
    return fetch('/api/manageIntroductions', options).then(parseJSON);
}

export function nudgeIntroduction(intro_uuid, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ intro_uuid, token })
    };

    return fetch('/api/nudgeIntroduction', options)
        .then(parseJSON);
}

export function archiveIntroduction(intro_id, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ intro_id, token })
    };

    return fetch('/api/archiveIntroduction', options)
        .then(parseJSON);
}
