import { headers, parseJSON } from './utils';

export function updateRankContact(rankArray, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ rankArray, token })
  }

  return fetch('/api/saveRank', options)
    .then(parseJSON);
}

export function loadContact(id, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ id, token })
  }

  return fetch('/api/getContact', options)
    .then(parseJSON);
}

export function loadContacts(token, page) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ token, page })
  };

  return fetch('/api/getContacts', options)
    .then(parseJSON);
}

export function createContact(contact, token) {
  let fname = contact.first_name;
  let lname = contact.last_name;
  let email = contact.email;
  let mobile = contact.mobile;
  let address = contact.address;
  let title = contact.title;
  let company = contact.company;
  let bio = contact.bio;
  let tags = contact.tags;
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ fname, lname, email, mobile, address, title, company, bio, tags, token })
  };

  return fetch('/api/createContact', options)
    .then(parseJSON);
}

export function saveContact(contact, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ contact, token })
  };

  return fetch('/api/updateContact', options)
    .then(parseJSON);
}


export function makeIntroductions(contact_a, contact_b, msg_a, msg_b, token) {
  let contact_a_id = contact_a.id;
  let contact_b_id = contact_b.id;
  let recipient_a_email = contact_a.email;
  let recipient_b_email = contact_b.email;
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ contact_a_id, contact_b_id, recipient_a_email, recipient_b_email, msg_a, msg_b, token })
  };

  return fetch('/api/makeIntroductions', options)
    .then(parseJSON);
}

export function postContactList(formData, token) {
  const options = {
    method: 'POST',
    body: formData
  };

  return fetch('/api/importContacts', options)
    .then(parseJSON);
}

export function postContactSearch(tags, page, token, rank=false) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ tags, page, token, rank })
  };

  return fetch('/api/searchContacts', options)
    .then(parseJSON);
}

export function inviteToFillOutProfile(id, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({id, token})
  };

  return fetch('/api/inviteToFillProfile', options)
    .then(parseJSON);
}