import { headers, parseJSON } from './utils';

export function getAutoLoginToken(uuid, type, done) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ uuid, type })
  };

  let a = fetch('/api/autoLogin', options).then(parseJSON);
  done(a);
  return a;
}

export function startAppointmentRequest(intro_uuid, contact_id, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ intro_uuid, contact_id, token })
  };

  return fetch('/api/appointmentrequest', options)
    .then(parseJSON);
}

export function postAppointmentRequest(request) {
  let {
    intro_uuid,
    contact_id,
    recipient_email,
    appt_length,
    appt_type,
    location,
    appt_title,
    message,
    timeslots_available,
    token } = request;

  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ intro_uuid, contact_id, recipient_email, appt_length, appt_type, location, appt_title, message, timeslots_available, token })
  };

  return fetch('/api/apptrequest', options)
    .then(parseJSON);
}

export function postAppointmentReRequest(uuid, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ uuid, token })
  };

  return fetch('/api/appt_reRequest', options)
    .then(parseJSON);
}



export function postAppointmentAccept(uuid, timeslot, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ uuid, timeslot, token })
  };

  return fetch('/api/appointmentAccept', options)
    .then(parseJSON);
}

export function getApptRequestList(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };

    return fetch('/api/appt_request_list', options)
        .then(parseJSON);
}

export function getApptMapList(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };

    return fetch('/api/appt_request_list', options)
        .then(parseJSON);
}

export function getApptListAPI(token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ token })
  };

  return fetch('/api/appt_list', options)
    .then(parseJSON);
}
