import { headers, parseJSON } from './utils';
import { CONFIG } from "../config"

export function postSession(email, password) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ email, password })
  };

  return fetch('/api/verifyLogin', options)
    .then(parseJSON);
}

export function postGoogleLogin(code) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({
      code,
      clientId: CONFIG.clientId,
      redirect_uri: CONFIG.redirectUri
    })
  };

  return fetch('/api/google_code', options)
    .then(parseJSON);
}

export function getAutoLoginToken(uuid, type, done) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ uuid, type })
  };

  let a = fetch('/api/autoLogin', options).then(parseJSON);
  done(a);
  return a;
}

export function postUserRegistration(user) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify(user)
  };

  return fetch('/api/registerUser', options)
    .then(parseJSON);
}

export function verifyUserToken(id, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({id, token})
  };

  return fetch('/api/verifyToken', options)
    .then(parseJSON);
}

export function updateProfile(user, token) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({user, token})
  };

  return fetch('/api/updateProfile', options)
    .then(parseJSON); 
}