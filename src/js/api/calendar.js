import { headers, parseJSON } from './utils';

export function getApiCalendarEvents(token) {
	const options = {
		headers: headers(),
		method: 'POST',
		body: JSON.stringify({ token })
	};

	return fetch('/api/get_events', options)
    .then(parseJSON);
}