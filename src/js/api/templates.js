import { headers, parseJSON } from './utils';

export function fetchTemplates(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };
    return fetch('/api/fetchTemplates', options).then(parseJSON);
}

export function fetchMainTemplates(token, type) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ type, token })
    };
    return fetch('/api/main_templates', options).then(parseJSON);
}

export function createTemplate(template, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ template, token })
    };
    return fetch('/api/createTemplate', options).then(parseJSON);
}


export function updateTemplate(template, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ template, token })
    };
    return fetch('/api/updateTemplate', options).then(parseJSON);
}


export function deleteTemplate(id, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ id, token })
    };
    return fetch('/api/deleteTemplate', options).then(parseJSON);
}