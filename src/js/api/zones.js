import { headers, parseJSON } from './utils';


export function fetchZones(token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ token })
    };
    return fetch('/api/fetchZones', options).then(parseJSON);
}

export function createZone(zone, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ zone, token })
    };
    return fetch('/api/createZone', options).then(parseJSON);
}


export function updateZone(zone, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ zone, token })
    };
    return fetch('/api/updateZone', options).then(parseJSON);
}


export function deleteZone(id, token) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({ id, token })
    };
    return fetch('/api/deleteZone', options).then(parseJSON);
}