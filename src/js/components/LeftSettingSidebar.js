/* eslint-disable indent */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

// import Box from 'grommet/components/Box';
// import Header from 'grommet/components/Header';
// import Sidebar from 'grommet/components/Sidebar';
// import Title from 'grommet/components/Title';
// import Menu from 'grommet/components/Menu';
// import Anchor from 'grommet/components/Anchor';
// import CloseIcon from 'grommet/components/icons/base/Close';
// import Button from 'grommet/components/Button';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MenuItem from '@material-ui/core/MenuItem';

const drawerWidth = 300;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    closeIcon: {
        color: 'white'
    },
    link: {
        textDecoration: 'none'
    },
    brandEmail: {
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        transform: 'none !important'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        backgroundColor: '#1b39a8',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
});

class LeftSettingSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this._responsive = Responsive.start(this._onResponsive);
    }

    componentWillUnmount() {
        this._responsive.stop();
    }

    _onResponsive(isLayoutSmall) {
    }

    render() {
        const { classes, theme } = this.props;

        return (
            <div className={classes.root}>
                <Drawer
                    variant="persistent"
                    anchor='left'
                    // open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={() => { this.props.handleClose(); }}>
                            <CloseIcon className={classes.closeIcon} />
                        </IconButton>
                    </div>
                    <div>
                        <Link to='/contacts' className={classes.link}><MenuItem>Contacts</MenuItem></Link>
                        <Link to='/appointment' className={classes.link}><MenuItem>Calendar</MenuItem></Link>
                        <Link to='/manage' className={classes.link}><MenuItem>Introductions</MenuItem></Link>
                        <Link to='/templates' className={classes.link}><MenuItem>Templates</MenuItem></Link>
                        <Link to='/profile' className={classes.link}><MenuItem>Profile</MenuItem></Link>
                        <Link to='/settings' className={classes.link}><MenuItem>Settings</MenuItem></Link>
                        <Link to='/profile' className={classes.link}><MenuItem>Profile</MenuItem></Link>
                        <Link to='/#' className={classes.link} onClick={this._onLogout}><MenuItem>Logout</MenuItem></Link>
                    </div>
                </Drawer>
            </div>
        );
    }
}

const select = state => ({
    session: state.session
});

export default connect(select)(LeftSettingSidebar);