import React, { Component } from 'react';
import ReactQuill from 'react-quill';

class CustomReactQuillAppointments extends Component {
  constructor(props) {
    super(props);

    let _self = this;

    let template_list = this.props.templates.map((template) => template.name);
    if (template_list.length === 0) { template_list = ['No Templates Found'] }

    this.state = {
      modules: {
        toolbar: {
          container:
            [
              [{ 'templates': template_list }],
              [{ 'header': 1 }, { 'header': 2 }],
              ['bold', 'italic', 'underline', 'strike'],
              ['blockquote'],
              [{ 'indent': '-1' }, { 'indent': '+1' }], 
              [{ 'fields': ['Primary - First Name', 'Primary - Last Name', 'Secondary - First Name', 'Secondary - Last Name'] }],
            ],
          handlers: {
            "templates": function (value) {
              if (value !== 'No Templates Found') {
                let quill = this.quill;
                let template = _self.props.templates.filter((t) => t.name === value);
                let template_body = template[0].body;
                quill.clipboard.dangerouslyPasteHTML(template_body);
              } 
            },
            "fields": function (value) {
              let quill = this.quill;
              let cursorPosition = quill.getSelection().index;
              switch (value) {
                case 'Primary - First Name':
                  quill.insertText(cursorPosition, '{primary_contact_first_name}');
                  break;
                case 'Primary - Last Name':
                  quill.insertText(cursorPosition, '{primary_contact_last_name}');
                  break;
                case 'Secondary - First Name':
                  quill.insertText(cursorPosition, '{secondary_contact_first_name}');
                  break;
                case 'Secondary - Last Name':
                  quill.insertText(cursorPosition, '{secondary_contact_last_name}');
                  break;
              }
            }
          }
        }
      }
    }

    this._renderFieldsButton = this._renderFieldsButton.bind(this);
    this._renderTemplateButton = this._renderTemplateButton.bind(this);
  }

  _renderTemplateButton() {
    const templatePickerItems = Array.prototype.slice.call(document.querySelectorAll('.ql-templates .ql-picker-item'));
    templatePickerItems.forEach(item => item.textContent = item.dataset.value);
    document.querySelector('.ql-templates .ql-picker-label').innerHTML = 'Template&nbsp;&nbsp;&nbsp;&nbsp;' + document.querySelector('.ql-templates .ql-picker-label').innerHTML;
  }

  _renderFieldsButton() {
    const templatePickerItems = Array.prototype.slice.call(document.querySelectorAll('.ql-fields .ql-picker-item'));
    templatePickerItems.forEach((item) => {
      item.textContent = item.dataset.value
      item.classList.remove('ql-selected');
    });
    document.querySelector('.ql-fields .ql-picker-label').innerHTML = '+Fields&nbsp;&nbsp;&nbsp;&nbsp;' + document.querySelector('.ql-fields .ql-picker-label').innerHTML;
  }

  componentDidMount() {
    this._renderTemplateButton();
    this._renderFieldsButton();
  }

  render() {
    return (
      <div>
        <ReactQuill
          theme="snow"
          modules={this.props.modules ? this.props.modules : this.state.modules}
          onChange={this.props.onChange}
          value={this.props.value} />
      </div>
    );
  }
}



export default CustomReactQuillAppointments;
