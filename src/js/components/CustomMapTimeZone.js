import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Delete from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';


class CustomMapTimeZone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: null,
        }
    }

    render() {
        const deleteButton = this.props.zone_id;

        return (
            <div className={'formTiles'}>
                <div className={'formsubTile'}>
                    <div>
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>What is the name of your zone?</span>
                        </Typography>
                        <form className={'timezoneForm'}>
                            <TextField
                                className={classes.margin}
                                label="Zone Name"
                                id="newZoneName"
                                name="newZoneName"
                                value={this.props.value}
                                placeHolder={'What do you want to call your zone?'}
                                onChange={this.props.handleInputChange}
                            />
                        </form>
                    </div>
                    <div className={'formBottomBtns'}>
                        {deleteButton ? <IconButton onClick={this.props.deleteZone}><Delete /></IconButton> : null}
                        <IconButton><NavigateBefore /></IconButton>
                        <IconButton onClick={this.props.nextStep}><NavigateNext /></IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default CustomMapTimeZone;
