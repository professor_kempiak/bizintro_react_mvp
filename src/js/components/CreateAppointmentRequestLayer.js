import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadAcceptTemplate } from '../actions/templates'
import ReactQuill from 'react-quill';
import PlacesAutocomplete from 'react-places-autocomplete';
import moment from 'moment';
import CustomReactQuillAppointments from './CustomReactQuillAppointments';
import { loadTemplates } from '../actions/templates';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  root: {
    flexGrow: 1,
  },

  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
});


class CreateAppointmentRequestLayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      appt_title: null,
      appt_startdate: null,
      appt_starttime: null,
      appt_enddate: null,
      appt_endtime: null,
      appt_type: null,
      appt_length: null,
      appt_location: '',
      appt_message: null,
      appt_timeslots: null,
      appt_events: this._filterNewEvents(this.props.events),
      appt_custom_length: 0,
      is_submitted: false
    }

    this._saveRequest = this._saveRequest.bind(this);
    this._submit = this._submit.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleQuillChange = this._handleQuillChange.bind(this);
    this._handleButtonInput = this._handleButtonInput.bind(this);
    this._handleLocationChange = this._handleLocationChange.bind(this);
    this.handleChangeLength = this.handleChangeLength.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
  }

  componentWillMount() {
    if (sessionStorage.rerequest !== "true") {
      let { dispatch } = this.props;
      const token = localStorage.getItem('token');
      // dispatch(loadAcceptTemplate(token));
    }
  }

  componentWillReceiveProps(newProps) {
      if (newProps.accept.sucess && newProps.accept !== this.state.appt_message) {
          this.setState({appt_message: this.getTemplate(newProps.accept.template.body)})
      }      
  }

  getTemplate(template_body) {
      let username = localStorage.getItem('name');
      let contact_a = JSON.parse(sessionStorage.contact_a);
      let contact_b = JSON.parse(sessionStorage.contact_b);
      template_body = template_body.replace(/({primary_contact_first_name})/g, contact_a.first_name);
      template_body = template_body.replace(/({primary_contact_last_name})/g, contact_a.last_name);
      template_body = template_body.replace(/({secondary_contact_first_name})/g, contact_b.first_name);
      template_body = template_body.replace(/({secondary_contact_last_name})/g, contact_b.last_name);
      // template_body = template_body.replace(/({location})/g, username);
      return template_body;
  }

  _filterNewEvents(events) {
    let newEvents = events.filter(event => event.new_event);
    return newEvents;
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    this.setState({is_submitted: false})
  }

  _handleQuillChange(body) {
    this.setState({
      appt_message: body
    })
    this.setState({is_submitted: false})
  }

  _handleLocationChange(location) {
    this.setState({
      appt_location: location
    });
    this.setState({is_submitted: false})
  }

  _saveRequest() {
    this.setState({is_submitted: true})

    let error_title = null , error_length = null , error_message = null, error_type  = null, can_submit = false;

    
    if(this.state.appt_title == null || this.state.appt_title == '') error_title = 'Title is required';
    if(this.state.appt_length == null || this.state.appt_length == '') error_length = 'Duration is required';
    if(this.state.appt_message == null || this.state.appt_message == '') error_message = 'Description is required';
    if(this.state.appt_type == null || this.state.appt_type == '') error_type = 'Type is required'; 

    if (error_type == null && error_message == null && error_length == null && error_title == null)
      can_submit = true;

    if (can_submit == false) return;


    let appt_message = this.state.appt_message;

    this.props.onSave({
      appt_title: this.state.appt_title,
      start: this.state.appt_startdate,
      end: this.state.appt_enddate,
      appt_type: this.state.appt_type,
      location: this.state.appt_location,
      message: appt_message,
      appt_length: this.state.appt_length === 'Other' ? this.state.appt_custom_length : this.state.appt_length,
      appt_events: this.state.appt_events
    });
  }

  _submit() {
    this.setState({is_submitted: true})
  }

  _handleButtonInput(target, value) {
    this.setState({
      [target]: value
    })
    this.setState({is_submitted: false})
  }

  handleChangeLength(target, value) {
    this.setState({ appt_length: value});
  }
  
  handleChangeType(target, value) {
    this.setState({ appt_type: value});
  }

  componentDidMount() {
    if (sessionStorage.rerequest == "true") {
      let appt_request = JSON.parse(sessionStorage.getItem('re_appt'))[0]['fields'];
      this.setState({
        appt_title: appt_request['title'],
        appt_length: appt_request['length'],
        appt_message: appt_request['message'],
        appt_location: appt_request['location'],
        appt_type: appt_request['appt_type'],
      });      
    }
  }

  render() {
    const { classes } = this.props;
    const inputProps = {
      value: this.state.appt_location,
      onChange: this._handleLocationChange,
      placeholder: 'Search Places...',
      autoFocus: true,
    }
    const locationSearchStyles = {
      root: {
        width: '100%',
        color: 'rgba(0, 0, 0, 0.87)',
        display: 'inline-flex',
        position: 'relative',
        fontSize: '1rem',
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
        lineHeight: '1.1875em',
      },
      input: {
        width: '100%',
        font: 'inherit',
        color: 'currentColor',
        border: '0',
        margin: '0',
        padding: '6px 0 7px',
        display: 'inline-block',
        minWidth: '0',
        flexGrow: '1',
        boxSizing: 'content-box',
        background: 'none',
        verticalAlign: 'middle',
        WebkitTapHighlightColor: 'transparent',
      },
      autocompleteContainer: {
        zIndex: 999,
        position: 'absolute',
        left: 0
      },
      autocompleteItem: {
        backgroundColor: '#ffffff',
        padding: '10px',
        color: '#555555',
        cursor: 'pointer',
      },
      autocompleteItemActive: {
        backgroundColor: '#dedede'
      }
    }
    let error_title = null , error_length = null , error_message = null, error_type  = null, can_submit = false;
    if (this.state.is_submitted) {
      if(this.state.appt_title == null || this.state.appt_title == '') error_title = 'Title is required';
      if(this.state.appt_length == null || this.state.appt_length == '') error_length = 'Duration is required';
      if(this.state.appt_message == null || this.state.appt_message == '') error_message = 'Description is required';
      if(this.state.appt_type == null || this.state.appt_type == '') error_type = 'Type is required';      
    }

    if (this.state.is_submitted && error_type == null && error_message == null && error_length == null && error_title == null)
      can_submit = true;

    return (
      <Material.Dialog
        open={true}
        onClose={this.props.onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <Material.DialogTitle id="alert-dialog-title">{"Create Appointment Request"}</Material.DialogTitle>
        <Material.DialogContent>
          <Material.Grid container spacing={24}>
            <Material.Grid item xs={12}>
              <Material.TextField
                id="title"
                label="title"
                value={this.state.appt_title}
                onChange={this._handleInputChange}
                margin="normal"
                fullWidth={true}
              />
            </Material.Grid>
            <Material.Grid item xs={12}>
              <Material.FormLabel component="legend">Appointment Length</Material.FormLabel>
              <Material.RadioGroup
                aria-label="gender"
                name="gender2"
                className={classes.group}
                value={this.state.appt_length}
                onChange={this.handleChangeLength}
                row
              >
                <Material.FormControlLabel
                  value="15"
                  control={<Material.Radio color="primary" />}
                  label="15 Mins"
                />
                <Material.FormControlLabel
                  value="30"
                  control={<Material.Radio color="primary" />}
                  label="30 Mins"
                />
                <Material.FormControlLabel
                  value="60"
                  control={<Material.Radio color="primary" />}
                  label="60 Mins"
                />
                <Material.FormControlLabel
                  value="90"
                  control={<Material.Radio color="primary" />}
                  label="90 Mins"
                  
                />
                <Material.FormControlLabel
                  value="Other"
                  control={<Material.Radio color="primary" />}
                  label="Other"
                />
              </Material.RadioGroup>
            </Material.Grid>
            
            <Material.Grid item xs={12}>
              <CustomReactQuillAppointments
                name='appt_message'
                className='rich-editor'
                placeholder='message'
                templates={this.props.templates}
                value={this.state.appt_message}
                onChange={this._handleQuillChange} />
            </Material.Grid>
            <Material.Grid item xs={12}>
              <Material.FormLabel component="legend">Appointment Type</Material.FormLabel>
              <Material.RadioGroup
                className={classes.group}
                value={this.state.appt_type}
                onChange={this.handleChangeType}
                row
              >
                <Material.FormControlLabel
                  value="Phone"
                  control={<Material.Radio color="secondary" />}
                  label="Phone"
                />
                <Material.FormControlLabel
                  value="Online"
                  control={<Material.Radio color="secondary" />}
                  label="Online"
                />
                <Material.FormControlLabel
                  value="Location"
                  control={<Material.Radio color="secondary" />}
                  label="Location"
                />
              </Material.RadioGroup>
            </Material.Grid>
            {
              this.state.appt_length && this.state.appt_length === 'Other' ?
                <Material.Grid item xs={12}>
                  <Material.TextField
                    id="appt_custom_length"
                    label="Custom Appointment Length"
                    value={this.state.appt_custom_length}
                    onChange={this._handleInputChange}
                    margin="normal"
                    fullWidth={true}
                  />
                </Material.Grid>
                : null
            }
            {
              this.state.appt_type == 'Location' ?
                <Material.Grid item xs={12}>
                  <Material.FormLabel component="legend">Location</Material.FormLabel>
                  <PlacesAutocomplete className={'MuiInput-root-327 MuiInput-fullWidth-334 MuiInput-formControl-328 MuiInput-underline-331'} styles={locationSearchStyles} inputProps={inputProps} />
                </Material.Grid>            
              : ''
            }

          </Material.Grid>
        </Material.DialogContent>
        <Material.DialogActions>
          <Material.Button onClick={this._saveRequest} color="primary">
            Confirm
          </Material.Button>
        </Material.DialogActions>
      </Material.Dialog>
      
    );
  }
}

const select = state => ({
  session: state.session,
  events: state.calendar.events,
  templates: state.templates.list,
  accept: state.templates.accept
});

export default withStyles(styles)(connect(select)(CreateAppointmentRequestLayer));
