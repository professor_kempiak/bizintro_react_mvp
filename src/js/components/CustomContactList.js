import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactQuill from 'react-quill';
import CustomContactCard from './CustomContactCard';
import { headers, parseJSON } from '../api/utils';
import { selectContact, deselectContact } from '../actions/contacts';

import ReactTagSearch from 'react-tag-autocomplete';

import { logout } from '../actions/session';
import { updateSearchTags } from '../actions/search';
import { remoteContactSearch } from '../actions/contacts';

import SearchIcon from 'grommet/components/icons/base/Search';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
    list: {
        width: '100%'
    }
});

class CustomContactList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tags: [],
            page: 1,
            perpage: 25,
            hasMoreItems: false,
            pageLoaded: false,
            end_of_pages: false,
            showLoading: false
        }

        this._isSelected = this._isSelected.bind(this);
        this._toggleSelection = this._toggleSelection.bind(this);
        this._searchContacts = this._searchContacts.bind(this);
    }


    _isPrimarySelection(contact) {
        let { selected } = this.props;
        if (selected.primary.includes(contact)) {
            return true;
        }
        return false;
    }

    _isSecondarySelection(contact) {
        let { selected } = this.props;
        if (selected.secondary.includes(contact)) {
            return true;
        }
        return false;
    }

    _isSelected(contact) {
        if (this._isPrimarySelection(contact) || this._isSecondarySelection(contact)) {
            return true;
        }
        return false;
    }

    _toggleSelection(contact) {
        let { dispatch, selected } = this.props;
        if (this._isPrimarySelection(contact) || this._isSecondarySelection(contact)) {
            dispatch(deselectContact(contact));
        } else {
            dispatch(selectContact(contact, selected));
        }
    }
    _searchContacts(event){
        console.log('');
    }
    _handleTagDelete(i) {
        let { dispatch } = this.props;
        const tags = this.state.tags.slice(0);
        tags.splice(i, 1);
        this.setState({ tags });
        this.setState({page: 1, showLoading: true, hasMoreItems: false});
        dispatch(updateSearchTags(tags));
    }

    _handleTagAddition(tag) {
        let { dispatch } = this.props;
        const tags = [].concat(this.state.tags, tag);
        this.setState({ tags });
        this.setState({page: 1, showLoading: true, hasMoreItems: false});
        dispatch(updateSearchTags(tags));
        // this.getSearchContacts(tags);
    }

    componentDidMount() {
        if (this.props.contacts.length >= this.state.perpage) {
            this.setState({hasMoreItems: true});
        }
    }

    componentWillReceiveProps(newProps) {
        if ((newProps.contacts.length - this.props.contacts.length) >= this.state.perpage) {
            this.setState({hasMoreItems: true, showLoading: false});
        } else {
            this.setState({page: 1});
            this.setState({hasMoreItems: false, showLoading: false});
        }
    }

    _onMore() {
      if (this.state.hasMoreItems){
        this.setState({ showLoading: true, hasMoreItems: false });
        let { dispatch } = this.props;
        const tags = []
        this.state.tags.map((tag) => tags.push(tag.name));
        let page = this.state.page + 1;
        this.setState({ page: page })
        dispatch(remoteContactSearch(
          tags, 
          page, 
          localStorage.token, () => {}));
      }
    }


    render() {
        const { classes } = this.props;
        let { contacts , selected , search} = this.props;
        let contactItems = contacts.map((contact, index) => {
            return (
                <CustomContactCard 
                    key={'custom_contact_list_key'+ index}
                    contact={contact} selected={selected} />
            )
        });
        return (
            <div className={'searchWrapper'}>
                <ReactTagSearch
                    tags={this.state.tags}
                    delimiters={[9, 13, 32]}
                    handleDelete={this._handleTagDelete.bind(this)}
                    handleAddition={this._handleTagAddition.bind(this)}
                    placeholder='Search'
                    allowNew={true}
                />
                <List selectable={true} className={classes.list}>
                    {contactItems}
                </List>
                <div style={{margin: 'auto',marginTop: '1em'}}>
                    {this.state.showLoading || this.props.contact_search_loading ? <div><CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} /></div> : null}
                {
                    this.state.hasMoreItems ? 
                    <div align='center'>
                        <Button variant="outlined" size="small" color="primary" onClick={this._onMore.bind(this)} className={classes.button}>
                            Load More
                        </Button>
                    </div> : 
                    null
                }
                </div>
            </div>

        );
    }
}

const select = state => ({
    contacts: state.contacts.list,
    selected: state.contacts.selected
});

export default withStyles(styles)(connect(select)(CustomContactList));