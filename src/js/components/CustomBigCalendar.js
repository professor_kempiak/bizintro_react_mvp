import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Box } from 'grommet';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import SetAppointmentFlow from './SetAppointmentFlow';
import { setCalendarDate, setCalendarView, getCalendarEvents} from '../actions/calendar';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import EventComponent from '../components/EventComponent';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
});
class CustomBigCalendar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentView: 'month',
            showLayer: false,
            top: 0,
            left: 0,
            selectedDay:  new Date(),
            selectedDate: new Date(),
            isSetDatePicker: false,
            anchorEl: null,
            currentEvent: {}
        }

        BigCalendar.setLocalizer(BigCalendar.momentLocalizer(moment));

        this._calendarNext = this._calendarNext.bind(this);
        this._calendarPrev = this._calendarPrev.bind(this);
        this._calendarToday = this._calendarToday.bind(this);
        this._gatherEvents = this._gatherEvents.bind(this);
        this._setMonthView = this._setMonthView.bind(this);
        this._setWeekView = this._setWeekView.bind(this);
        this._setDayView = this._setDayView.bind(this);

        this.CustomToolbar = this.CustomToolbar.bind(this);
        this.eventSelect = this.eventSelect.bind(this);

    }

    _gatherEvents(start, end, timezone, callback) {
        let { calendar } = this.props;
        callback(calendar.events);
    }

    _calendarNext() {
        let { calendar, dispatch } = this.props;
        let date = moment(calendar.defaultDate);
        if (calendar.defaultView == 'month') {
            date.add(1, 'months');
        } else if (calendar.defaultView == 'week') {
            date.add(1, 'weeks');
        } else {
            date.add(1, 'days');
        }
        dispatch(setCalendarDate(date));
    }

    _calendarPrev() {
        let { calendar, dispatch } = this.props;
        let date = moment(calendar.defaultDate);
        if (calendar.defaultView == 'month') {
            date.subtract(1, 'months');
        } else if (calendar.defaultView == 'week') {
            date.subtract(1, 'weeks');
        } else {
            date.subtract(1, 'days');
        }
        dispatch(setCalendarDate(date));
    }

    _calendarToday() {
        let { dispatch } = this.props;
        let date = moment();
        dispatch(setCalendarDate(date));
    }

    _setMonthView() {
        let { dispatch } = this.props;
        dispatch(setCalendarView('month'));
    }

    _setWeekView() {
        let { dispatch } = this.props;
        dispatch(setCalendarView('week'));
    }

    _setDayView() {
        let { dispatch } = this.props;
        dispatch(setCalendarView('day'));
    }
    onClose() {
        this.setState({
            showLayer: false
        })
    }

    onSelect(slotInfo) {
        if (this.props.isSetting == false) return;
        if(slotInfo.start.getTime() < (new Date()).getTime()) return;

        let dom = document.getElementsByClassName(slotInfo.start.toISOString());

        if (dom.length > 1)
            dom = dom[1];
        else
            dom = dom[0];

        if ($(dom).offset() == undefined)
            return;

        let y = $(dom).offset().top + 5,
            x = $(dom).offset().left + 5,
            width = $(window).width(),
            height = $(window).height() - 450,
            cellWidth = $('.rbc-events-container').width();

        if ((width - x) < 455) {
            x = x - 455 + cellWidth;
        }

        if (height < y) {
            y = height
        }

        this.setState({
            top: y,
            left: x,
            isSetDatePicker: false,
            showLayer: true,
            currentEvent: slotInfo
        });
    }

    addEvent(event, sendOrNot) {
        this.props.onSelect(event, this.state.currentView, sendOrNot)
    }
    eventSelect(e){
        let dom = document.getElementsByClassName(e.start.toISOString());
        let calendar = this.props.calendar;

        if (dom.length > 1)
            dom = dom[1];
        else
            dom = dom[0];

        if ($(dom).offset() == undefined)
            return;

       let y = $(dom).offset().top + 5,
        x = $(dom).offset().left + 5,
        width = $(window).width(),
        height = $(window).height() - 450,
        cellWidth = $('.rbc-events-container').width();

        if ((width - x) < 455) {
            x = x - 455 + cellWidth;
        }

        if (height < y) {
            y = height
        }

        this.setState({
            showLayer : true,
            currentEvent: e,
            isSetDatePicker: false,
            top: y,
            left: x
        }) ;
    }

    eventStyleGetter(event, start, end, isSelected) {
        if (event.new_event) {
            return {
                style: {
                    backgroundColor: 'gray'
                }
            }
        }else{ 
            if (event.google == true) {
                return {
                    style: {
                        backgroundColor: '#3174ad'
                    }
                }
            }
            if (event.confirmed == true) {
                return {
                    style: {
                        backgroundColor: '#425562'
                    }
                }
            } else {
                return {
                    style: {
                        backgroundColor: 'gray'
                    }
                }
            }
        }        
    }

    handleMenuClick = event => {
      this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuClose = () => {
      this.setState({ anchorEl: null });
    };

    CustomToolbar() {
        let { calendar, classes } = this.props;
        const { anchorEl } = this.state;
        let calendarTitle = moment(calendar.defaultDate);
        if (calendar.defaultView == 'month') {
            calendarTitle = calendarTitle.format('MMMM YYYY');
        } else if (calendar.defaultView == 'week') {
            let startDate = moment(calendar.defaultDate).startOf('week');
            let endDate = moment(calendar.defaultDate).endOf('week');
            calendarTitle = startDate.format('MMM') + ' ' + startDate.format('D') + '-' + endDate.format('D') + ', ' + endDate.format('YYYY');
        } else {
            calendarTitle = calendarTitle.format('MMMM D, YYYY');
        }
        return (
            <Box pad='small' direction='row' colorIndex='light-2' responsive={false} justify='between' align='center' className={'calendarControlPanel'}>
                <Box direction='row' responsive={false} alignSelf='start' className='calendar-buttons-left-container'>
                    <Material.Button variant="contained" className={classes.button}  onClick={this._calendarPrev}>
                    Back
                    </Material.Button>

                    <Material.Button variant="contained" className={classes.button} onClick={this._calendarNext}>
                    Forward
                    </Material.Button>                    
                </Box>
                <Box direction='row' responsive={false} alignSelf='start' className='calendar-buttons-middle-container'>
                    <Material.Button
                      aria-owns={anchorEl ? 'simple-menu' : null}
                      aria-haspopup="true"
                      onClick={this.handleMenuClick}
                    >
                      {calendarTitle}
                      <Material.Icon>arrow_drop_down</Material.Icon>
                    </Material.Button>
                    <Material.Menu
                      id="simple-menu"
                      anchorEl={anchorEl}
                      open={Boolean(anchorEl)}
                      className={'marBot'}
                      onClose={this.handleMenuClose}
                    >
                      <Material.MenuItem className={'calMenuItem'} onClick={this._calendarToday}>Today</Material.MenuItem>
                    </Material.Menu>                    
                </Box>
                <Box direction='row' responsive={false} alignSelf='center' className='calendar-buttons-right-container'>
                    <Material.Button variant="contained" color="primary" className={classes.button} onClick={this._setMonthView}>
                    Month
                    </Material.Button>
                    <Material.Button variant="contained" color="primary" className={classes.button} onClick={this._setWeekView}>
                    Week
                    </Material.Button>
                    <Material.Button variant="contained" color="primary" className={classes.button} onClick={this._setDayView}>
                    Day
                    </Material.Button>                    
                </Box>
            </Box>
        )
    }
    componentWillMount(){
        moment.locale('us');
        BigCalendar.momentLocalizer(moment);
    }

    componentWillReceiveProps(newProps) {
        if ((newProps.isShowApptLayer != this.props.isShowApptLayer) && newProps.isShowApptLayer == true && this.state.showLayer == false) {
            let width = $(window).width() / 2, height = $(window).height() / 2;
            let today = new Date();
            let endDate = today.setHours(today.getHours() + 2);
            this.setState({
                top: height,
                left: width,
                currentEvent: {
                    start: new Date(),
                    end: endDate
                },
                isSetDatePicker: true,
                showLayer: true
            });
        }
    }


    render() {
        let { calendar, classes } = this.props;
        let formats = {
            dayFormat: (date, culture, localizer) =>
                localizer.format(date, 'ddd M/DD', culture),
        }
        let defaultDate = moment(calendar.defaultDate).toDate();
        return (
            <div  className='big-calendar-container'>
                {this.state.showLayer? <SetAppointmentFlow top={this.state.top} left={this.state.left} onClose={this.onClose.bind(this)} event={this.state.currentEvent} onSave={this.addEvent.bind(this)} isSetDatePicker={this.state.isSetDatePicker} /> : null}
                <BigCalendar
                    selectable
                    className='big-calendar'
                    events={calendar.events}
                    defaultView={calendar.defaultView}
                    view = {calendar.defaultView}
                    defaultDate={defaultDate}
                    components={{
                        toolbar: this.CustomToolbar,
                        event: EventComponent
                    }}
                    eventPropGetter={this.eventStyleGetter}
                    onView={view => this.setState({currentView: view})}
                    slotPropGetter={(date) => ({ className: date.toISOString() })}
                    onSelectSlot={this.onSelect.bind(this)}
                    date={defaultDate}
                    scrollToTime={new Date()}
                    onSelectEvent={this.eventSelect}
                    onNavigate={(date) => { this.setState({ selectedDate: date })}}
                    formats={formats} />

            </div>
        );
    }
}

const select = state => ({
    calendar: state.calendar,
});

export default withStyles(styles)(connect(select)(CustomBigCalendar));
