/* eslint-disable indent */
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
// import Responsive from 'grommet/utils/Responsive';
import { connect } from 'react-redux';

// import Box from 'grommet/components/Box';
// import Header from 'grommet/components/Header';
// import Sidebar from 'grommet/components/Sidebar';
// import Title from 'grommet/components/Title';
// import Menu from 'grommet/components/Menu';
// import Anchor from 'grommet/components/Anchor';
// import CloseIcon from 'grommet/components/icons/base/Close';
// import Button from 'grommet/components/Button';
// import Value from 'grommet/components/Value';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuItem from '@material-ui/core/MenuItem';

const drawerWidth = 300;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    closeIcon: {
        color: 'white'
    },
    link: {
        textDecoration: 'none'
    },
    brandEmail: {
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        transform: 'none !important'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        backgroundColor: '#1b39a8',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
});

class LeftSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { classes, theme } = this.props;
       
        return (
            <div className={classes.root}>
                <Drawer
                    variant="persistent"
                    anchor='left'
                    // open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={() => { this.props.handleClose(); }}>
                            <ChevronRightIcon className={classes.closeIcon} />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        <Link to='/' className={classes.link}><MenuItem>Home</MenuItem></Link>
                        <Link to='/schedule' className={classes.link}><MenuItem>Schedule</MenuItem></Link>
                        <Link to='/templates' className={classes.link}><MenuItem>Templates</MenuItem></Link>
                        <Link to='/profile' className={classes.link}><MenuItem>Profile</MenuItem></Link>
                        <Link to='/rank' className={classes.link}><MenuItem>Rank</MenuItem></Link>
                    </List>
                    <Divider />
                    <List>
                        <Link to='/' onClick={this.props.onLogout} className={classes.link}>
                            <MenuItem>Logout</MenuItem>
                        </Link>
                    </List>
                    <div className={classes.brandEmail}>
                        {this.props.session.email }
                    </div>
                </Drawer>
            </div>
        );
    }
}

const select = state => ({
    session: state.session
});

export default withStyles(styles, { withTheme: true })(connect(select)(LeftSidebar));


// <Anchor href='/landingpage'>Landing Page</Anchor>
// <Anchor href='/landingpage2'>Landing Page 2</Anchor>
// <Anchor href='/contacts'>Contacts</Anchor>
// <Anchor href='/contactdetails'>Contact Details</Anchor>
// <Anchor href='/appointment'>Calendar</Anchor>
// <Anchor href='/map'>Map</Anchor>
// <Anchor href='/manage'>Introductions</Anchor>