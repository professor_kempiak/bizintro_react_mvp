import React, { Component } from 'react';
import { connect } from 'react-redux';
import FullCalendar from 'fullcalendar-reactwrapper';
import moment from 'moment';
import { setCalendarDate, setCalendarView } from '../actions/calendar';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
});

class CustomFullCalendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      layer: null,
      notifications: null
    };

    this._calendarNext = this._calendarNext.bind(this);
    this._calendarPrev = this._calendarPrev.bind(this);
    this._calendarToday = this._calendarToday.bind(this);
    this._gatherEvents = this._gatherEvents.bind(this);
    this._setMonthView = this._setMonthView.bind(this);
    this._setWeekView = this._setWeekView.bind(this);
    this._setDayView = this._setDayView.bind(this);
  }

  _gatherEvents(start, end, timezone, callback) {
    let { calendar } = this.props;
    callback(calendar.events);
  }

  _calendarNext() {
    let { calendar, dispatch } = this.props;
    let date = moment(calendar.defaultDate);
    if (calendar.defaultView == 'month') {
      date.add(1, 'months');
    } else if (calendar.defaultView == 'basicWeek') {
      date.add(1, 'weeks');
    } else {
      date.add(1, 'days');
    }
    dispatch(setCalendarDate(date));
  }

  _calendarPrev() {
    let { calendar, dispatch } = this.props;
    let date = moment(calendar.defaultDate);
    if (calendar.defaultView == 'month') {
      date.subtract(1, 'months');
    } else if (calendar.defaultView == 'basicWeek') {
      date.subtract(1, 'weeks');
    } else {
      date.subtract(1, 'days');
    }
    dispatch(setCalendarDate(date));
  }

  _calendarToday() {
    let { dispatch } = this.props;
    let date = moment();
    dispatch(setCalendarDate(date));
  }

  _setMonthView() {
    let { dispatch } = this.props;
    dispatch(setCalendarView('month'));
  }

  _setWeekView() {
    let { dispatch } = this.props;
    dispatch(setCalendarView('basicWeek'));
  }

  _setDayView() {
    let { dispatch } = this.props;
    dispatch(setCalendarView('basicDay'));
  }

  render() {
    let { calendar } = this.props;
    let calendarTitle = moment(calendar.defaultDate);
    if (calendar.defaultView == 'month') {
      calendarTitle = calendarTitle.format('MMMM YYYY');
    } else if (calendar.defaultView == 'basicWeek') {
      let startDate = moment(calendar.defaultDate).startOf('week');
      let endDate = moment(calendar.defaultDate).endOf('week');
      calendarTitle = startDate.format('MMM') + ' ' + startDate.format('D') + '-' + endDate.format('D') + ', ' + endDate.format('YYYY');
    } else {
      calendarTitle = calendarTitle.format('MMMM D, YYYY');
    }
    return (
      <div>
        <div>
          <div>
            <Button onClick={this._calendarPrev}><NavigateBefore /></Button>
            <Button onClick={this._calendarToday}>Today</Button>
            <Button onClick={this._calendarNext}><NavigateNext /></Button>
          </div>
          <div>
            <Button onClick={this._setMonthView}>Month</Button>&nbsp;
            <Button onClick={this._setWeekView}>Week</Button>&nbsp;
            <Button onClick={this._setDayView}>Day</Button>
          </div>
        </div>
        <div>
          <Typography gutterBottom variant="headline" component="h2">
            <span style={{ fontWeight: '600' }}>{calendarTitle}</span>
          </Typography>
          <FullCalendar
            header={false}
            defaultView={calendar.defaultView}
            defaultDate={calendar.defaultDate}
            editable={true}
            eventLimit={true} // allow "more" link when too many events
            selectable={true}
            select={this.props.onSelect}
            events={this._gatherEvents}
            eventColor={'#00e7ff'}
          />
        </div>
      </div>
    );
  }
}

const select = state => ({
  calendar: state.calendar
});

export default withStyles(styles)(connect(select)(CustomFullCalendar));
