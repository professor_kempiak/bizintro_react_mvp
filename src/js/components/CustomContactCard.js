import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import SelectContactButton from './SelectContactButton';
import { logout } from '../actions/session';

import { deselectContact, selectContact } from "../actions/contacts";

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
});

class CustomContactCard extends Component {
    constructor(props) {
        super(props);

        this.addContact = this.addContact.bind(this);
    }

    addContact(){

        let {dispatch, selected ,contact} = this.props;
        if (selected.primary.includes(contact) || (selected.secondary.includes(contact))) {
            dispatch(deselectContact(contact));
        }
        else {
            dispatch(selectContact(contact, selected));
        }
    }

    includesElement(arrObj, obj) {
      let _obj = arrObj.filter((o) => o.id == obj.id);
      return _obj.length > 0 ? true : false;
    }

    render() {

        const { contact , selected, classes } = this.props;
        let isChecked = false;
        if (this.includesElement(selected.primary, contact) || this.includesElement(selected.secondary,contact)) {
            isChecked = true;
        }
        let title = contact.occupation ? contact.occupation.split(' at ')[0] : 'Title Not Specified';
        let company = contact.occupation ? contact.occupation.split(' at ')[1] : 'Company Not Specified';
        return (
            <ListItem button>
                <Avatar alt="Remy Sharp" src={contact.avatar_link} />
                <ListItemText primary={`${ contact.first_name } ${contact.last_name}`} />
                <ListItemText primary={"CEO"} />
                <ListItemSecondaryAction>
                    <Checkbox
                        onChange={this.addContact}
                        checked={isChecked}
                    />
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}

const select = state => ({
    session: state.session,
    selected: state.contacts.selected
});

export default connect(select)(CustomContactCard);