import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTagSearch from 'react-tag-autocomplete';

import { logout } from '../actions/session';
import { updateSearchTags, removeSearchTag, addSearchTag, updateSearchTerm, updateSearchClear } from '../actions/search';
import { remoteContactSearch } from '../actions/contacts';
import Sidebar from 'react-sidebar';
import LeftSidebar from '../components/LeftSidebar';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import classNames from 'classnames';

const drawerWidth = 240;
const realWidth = 160;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: 80,
        width:  `calc(100% - 80px)`,
        boxShadow: '0 1px 11px rgba(133, 133, 133, 0.43)',
        backgroundColor: '#425563',
        paddingRight: 200
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
});

class HeaderBar1 extends Component {
    constructor(props) {
        super(props);

        let ReactTags;

        this.state = {
            open: false
        }

        this._onLogout = this._onLogout.bind(this);
        this.sidebar = <LeftSidebar close={this.closeModal} onLogout={this._onLogout.bind(this)} />;
        this.onSetOpen = this.onSetOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setOfferSidebar = this.setOfferSidebar.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    _onLogout(event) {
        event.preventDefault();
        this.props.dispatch(logout());
    }

    _handleTagDelete(i) {
        let { dispatch } = this.props;
        dispatch(removeSearchTag(i));
    }

    _handleTagAddition(tag) {
        let { dispatch } = this.props;
        dispatch(addSearchTag(tag.name));
        dispatch(updateSearchTerm(''));
    }

    onSetOpen(open) {
        this.setState({ open });
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    closeModal() {
        this.setState({ open: false });
    }

    handleClose() {
        this.setState({ isShowingModal: false });
    }

    setOfferSidebar() {
        this.sidebar = <LeftSidebar handleClose={() => { this.closeModal(); }} onLogout={this._onLogout.bind(this)} />;
        this.setState({ open: true });
        this.setState({ isShowingModal: false });
    }

    _clearSearchBar() {
        this.ReactTags.state.query = '';
        this.ReactTags.input.input.focus();
        this.props.dispatch(updateSearchClear(false));
    }

    componentDidUpdate() {
        if (this.props.search_reset) {
            this._clearSearchBar();
        }
    }

    _toggleSearchBarVisibility(e) {
        let searchContainer = document.getElementsByClassName('headerSearchWrapper')[0];
        let searchInput = document.getElementsByClassName('headerSearch')[0].firstChild;
        searchContainer.classList.toggle('visible');
        searchInput.focus();
    }
    toggleDrawer() {
        this.setState({
            open: !this.state.open
        });
    };

    render() { console.log(this.state.open);
        const sidebarProps = {
            sidebar: this.sidebar,
            sidebarClassName: 'custom-sidebar-class',
            open: this.state.open,
            onSetOpen: this.onSetOpen,
        };


        const tags = this.props.tags.map(tag => {
            return { name: tag }
        });

        const { dropAlign, colorIndex, session: { name: userName }, classes } = this.props;
        return (
                <AppBar position="static"
                        className={classNames(classes.appBar, {
                            [classes.appBarShift]: this.props.opne,
                            [classes[`appBarShift-${this.props.anchor}`]]: this.props.open,
                        })}>
                    <Toolbar>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            {
                                this.props.hideNavBar ?
                                    <IconButton className='bizintro-logo'>
                                        Bizintro
                                    </IconButton> :
                                    <IconButton className='bizintro-logo' href='/'>
                                        Bizintro
                                    </IconButton>
                            }
                        </Typography>
                        {
                            this.props.searchBar ?
                                <SearchIcon onClick={this._toggleSearchBarVisibility} className='headerSearchIcon' />
                                :
                                ''
                        }
                        {
                            this.props.searchBar ?
                                <ReactTagSearch
                                    ref={(ip) => this.ReactTags = ip}
                                    classNames={{ root: 'react-tags headerSearchContainer', searchInput: 'react-tags__search-input headerSearch' }}
                                    tags={tags}
                                    delimiters={[9, 13, 32]}
                                    handleDelete={this._handleTagDelete.bind(this)}
                                    handleAddition={this._handleTagAddition.bind(this)}
                                    handleInputChange={this.props.searchInputChange ? this.props.searchInputChange : null}
                                    placeholder='Make Intro or Search'
                                    allowNew={true} />
                                :
                                ''
                        }
                    </Toolbar>
                    <Tabs value={this.props.activeTab} onChange={this.props.handleChange} className={'tobbarTab'}>
                        <Tab label="History" />
                        <Tab label="Contacts" />
                        <Tab label="Archives" className={'topBarRight'}/>
                    </Tabs>
                </AppBar>
        );
    }
}

const select = state => ({
    session: state.session,
    tags: state.search.tags,
    search_reset: state.search.reset
});

export default withStyles(styles)(connect(select)(HeaderBar1));