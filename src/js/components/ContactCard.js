import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { Card, Box, Anchor,Menu } from 'grommet';
// import EditIcon from 'grommet/components/icons/base/Edit';
import SelectContactButton from './SelectContactButton';
import { logout } from '../actions/session';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import ClockIcon from '@material-ui/icons/Alarm';
import EmailIcon from '@material-ui/icons/Email';
import ChatIcon from '@material-ui/icons/Message';

const styles = {
    card: {
        maxWidth: 345,
        minWidth: 345,
        minHeight: 280
    },
    cardTitle: {
        backgroundColor: '#00a981',
        padding: '8px',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};

class ContactCard extends Component {
    constructor(props) {
        super(props);
        this.state={
            ishover: false
        }
        this._onMouseOver = this._onMouseOver.bind(this);
        this._onMouseOut = this._onMouseOut.bind(this);
    }
    _onMouseOver(){
        this.setState({
            ishover: true
        })
    }
    _onMouseOut(){
        this.setState({
            ishover: false
        })
    }

    render() {
        const { contact , selected, classes } = this.props;
        let title = contact.occupation ? contact.occupation.split(' at ')[0] : 'Title Not Specified';
        let company = contact.occupation ? contact.occupation.split(' at ')[1] : 'Company Not Specified';
        let tags = contact.tags ;
        let tagContent = tags.reduce((tagContent, item, i) => {
            if (item === '' || i > 3) {
                return tagContent;
            } else {
                tagContent.push(item);
                return tagContent;
            }
        }, []);

        let hoverClassName = '';
        if(this.state.ishover){
            hoverClassName = 'showPencil';
        }
        
        return (
            <Card className={classes.card}>
                <div className={classes.cardTitle} onClick={(e) => this.props.toggleSeclect(contact)}>
                    <Typography gutterBottom variant="headline" component="h2">
                        <span style={{ color: 'white', fontWeight: '600' }}>{contact.first_name.slice(0, 1).toUpperCase() + contact.last_name.slice(0, 1).toUpperCase()}</span>
                    </Typography>
                </div>
                <CardContent>
                    <Typography gutterBottom variant="headline" component="h2">
                        <span onClick={(e) => this.props.toggleSeclect(contact)}>
                            {contact.first_name} {contact.last_name}
                        </span>
                    </Typography>
                    <Typography variant="subheading" component="h3">
                        {company ? company : 'Company Not Specified'}
                    </Typography>
                    <Typography variant="subheading" component="h3">
                        {title ? title : 'Title Not Specified'}
                    </Typography>
                    <Typography component="p">
                        {
                            tagContent.map((tag, i) => {
                                return (
                                    <span
                                        key={i}
                                        className={'tagBtn'}
                                        style={{
                                            color: 'white',
                                            backgroundColor: '#00a981',
                                            padding: '5px', borderRadius: '3px',
                                            fontweight: '500',
                                            paddingTop: '5px'
                                        }}
                                    >
                                        {tag}
                                    </span>
                                )
                            })
                        }
                    </Typography>
                </CardContent>
                <CardActions>
                    <Typography component="p">
                        <EmailIcon /> <span>{contact.count_emails.toString()}</span>
                    </Typography>
                    <Typography component="p">
                        <ClockIcon /> <span>{contact.count_events.toString()}</span>
                    </Typography>
                    <Typography component="p">
                        <ChatIcon /> <span>{contact.count_intros.toString()}</span>
                    </Typography>
                    <Typography component="p">
                        <Button onClick={(e) => this.props.editContact(contact, e)}>
                            <EditIcon />
                        </Button>
                    </Typography>
                </CardActions>
            </Card>
            // <Box onMouseOver={this._onMouseOver} onMouseOut={this._onMouseOut} className={hoverClassName}>
            //     <Tiles fill={false} className={'contactTiles'}>
            //         <Tile separator='top'
            //             align='start'
            //             basis='1/2'
            //             className='imageTile'>
            //             <Box className={"layerImageWrapper"}>
            //                 {/* <Image src={contact.avatar_link} /> */}
            //                 <Box flex='grow'
            //                     className='contactInitials'
            //                     onClick={(e) => this.props.toggleSeclect(contact)}>
            //                     {contact.first_name.slice(0, 1).toUpperCase() + contact.last_name.slice(0, 1).toUpperCase()}
            //                 </Box>
            //             </Box>
            //         </Tile>
            //         <Tile separator='top'
            //             align='start'
            //             basis='1/2'
            //             className='infoTile'>
            //             <Box justify='start'
            //                 align='start'
            //                 basis="full"
            //                 className={"layerCardWrapper"}>
            //                 {/*<Box direction='row'*/}
            //                 {/*alignSelf='end' >*/}
            //                 {/*<Label  margin="none"   align="end">*/}
            //                 {/*{'TOP 25'}*/}
            //                 {/*</Label>*/}
            //                 {/*</Box>*/}
            //                 <Box direction='row'
            //                     justify='start'
            //                     align='start'
            //                     onClick={(e) => this.props.toggleSeclect(contact)}>
            //                     <Label margin="none" className={"userName"} align="start">
            //                         {contact.first_name} {contact.last_name}
            //                     </Label>
            //                 </Box>
            //                 <Box direction='row'
            //                     justify='start'
            //                     align='start' >
            //                     <Label margin="none" align="start">
            //                         {title ? title : 'Title Not Specified'}
            //                     </Label>
            //                 </Box>
            //                 <Box direction='row'
            //                     justify='start'
            //                     align='start'>
            //                     <Label margin="none" align="start" size="small">
            //                         {company ? company : 'Company Not Specified'}
            //                     </Label>
            //                 </Box>
            //                 <Box direction='row'
            //                     justify='start'
            //                     align='start'
            //                     wrap={true}>
            //                     {tagContent}
            //                 </Box>
            //                 <Box direction='row'
            //                     alignSelf='end' className={'manageTool'}>
            //                     <Menu icon={<MoreIcon />}>
            //                         <Anchor href='#' >Appointment</Anchor>
            //                         <Anchor href='#' >Alert </Anchor>
            //                         <Anchor href='#'>Rank </Anchor>
            //                         <Anchor href='#'>Help </Anchor>
            //                         <Anchor href='#' onClick={(e) => this.props.editContact(contact, e)}>Edit </Anchor>
            //                     </Menu>
            //                 </Box>
            //             </Box>
            //         </Tile>

            //         <Box direction='row'
            //             justify='between'
            //             basis='full'
            //             align='start'
            //             pad='small'
            //             colorIndex='light-2'
            //             className={'paddingRight50'}
            //             wrap={true}>
            //             <Anchor icon={<MailOptionIcon />}
            //                 label={contact.count_emails.toString()}
            //                 href='#' />
            //             <Anchor icon={<ClockIcon />}
            //                 label={contact.count_events.toString()}
            //                 href='#' />
            //             <Anchor icon={<ChatIcon />}
            //                 label={contact.count_intros.toString()}
            //                 href='#' />
            //         </Box>
            //     </Tiles>
            //     <Anchor icon={<EditIcon colorIndex={'light-1'} />}
            //         className={'editContactIcon'}
            //         href='#' onClick={(e) => this.props.editContact(contact, e)} />
            // </Box>
        );
    }
}

const select = state => ({
    session: state.session,
    selected: state.contacts.selected
});

export default withStyles(styles)(connect(select)(ContactCard));
