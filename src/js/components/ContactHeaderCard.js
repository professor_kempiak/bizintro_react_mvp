import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactQuill from 'react-quill';
import { sendNewIntroductions, clearSelectedContacts } from '../actions/contacts'
import { addToast } from '../actions/toasts';
import CustomReactQuillIntroductions from './CustomReactQuillIntroductions';

import CustomContactList from '../components/CustomContactList';
import ContactHeader from "./ContactHeader";

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Grid from '@material-ui/core/Grid';


const styles = {
    card: {
        display: 'flex',
        margin: '2%',
        maxWidth: 300,
        minWidth: 115,
        minHeight: 90
    },
    cardTitle: {
        backgroundColor: '#1b39a8',
        padding: '8px',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    headerCard: {
        width: 'auto',
        borderRadius: 3,
        backgroundColor: '#425563',
        padding: 5,
        marginRight: 30,
    },
    headerCardCircle: {
        width: 29,
        height: 29,
        borderRadius: 15,
        padding: 5,
        color:'#ffffff',
        backgroundColor: '#425563',
    },
    PaddingLeft15: {
        paddingLeft: 10,
    },
    userName: {
        color: '#ffffff',
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 400,
        paddingLeft:15,
    },
    userFullName: {
        color: '#ffffff',
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 400,
    }
};

class ContactHeaderCard extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        const { contact, contacts, classes } = this.props;
        // let secondary_contacts = contacts.list.filter((contact) => {
        //     if(contact.id == contactId){
        //         return true;
        //     }
        // });
        // let contact = secondary_contacts[0];
        let title = contact.occupation ? contact.occupation.split(' at ')[0] : 'Title Not Specified';
        return (
            <div className={'contactHederCard'}>
                <Grid container className={classes.headerCard} alignItems='center'>
                    <Grid item className={classes.PaddingLeft15}>
                            <span className={classes.headerCardCircle}>
                                {contact.first_name.slice(0, 1).toUpperCase() + contact.last_name.slice(0, 1).toUpperCase()}
                            </span>
                    </Grid>
                    <Grid item xs container direction="column" spacing={16} className={classes.userName}>
                        <Typography className={classes.userFullName}>
                            {contact.first_name} {contact.last_name}
                        </Typography>
                    </Grid>
                    <Grid item className={classes.PaddingLeft15}>
                        <i className="material-icons" onClick={() => { this.props.removeContact(contact) }}>
                            more_vert
                        </i>
                    </Grid>
                </Grid>
            </div>

        );
    }
}

const select = state => ({
    session: state.session,
    contacts: state.contacts,
    selected: state.contacts.selected
});

export default withStyles(styles)(connect(select)(ContactHeaderCard));