import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({

})

class CustomMapDrawBoundry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: null,
        }
    }

    componentDidMount() {
        this.props.activateZoneToolbar();
        this.props.verifyAndRenderCurrentBoundaries();
    }

    componentWillUnmount() {
        this.props.activateZoneToolbar();
    }

    _saveBoundariesAndPrev() {
        this.props.saveBoundaries();
        this.props.preStep();
    }

    _saveBoundariesAndNext() {
        this.props.nextStep();
    }


    render() {
        return (
            <div className={'formTiles'}>
                <div className={'formsubTile'}>
                    <div>
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>Awesome!<br /><br />Now let's draw your zone boundries.</span>
                        </Typography>
                    </div>
                    <div className={'formBottomBtns'}>
                        <IconButton onClick={this._saveBoundariesAndPrev.bind(this)}><NavigateBefore /></IconButton>
                        <IconButton onClick={this._saveBoundariesAndNext.bind(this)}><NavigateNext /></IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(CustomMapDrawBoundry);
