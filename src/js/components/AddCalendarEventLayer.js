import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
// import { Layer, Form, Header, Heading, FormField, FormFields, TextInput, Footer, Button, Box, Select, DateTime, Columns, List, ListItem, Table, TableRow, Title } from 'grommet';
// import AddIcon from 'grommet/components/icons/base/Add';
// import SubtractIcon from 'grommet/components/icons/base/Subtract';

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const styles = (props) => {

}

class AddCalendarEventLayer extends Component {
  constructor(props) {
    super(props);

    let startDate = moment(this.props.startDate);
    let endDate = moment(this.props.endDate);
    let date = startDate;
    let difference = endDate.diff(startDate, 'days') + 1;
    let timeSlots = {};

    for (let x = 0; x < difference; x++) {
      if (this.props.currentView === 'month') {
        timeSlots[date.format('YYYY-MM-DD')] = [];
      } else {
        let timeString = startDate.format('HH:mm') + '_' + endDate.format('HH:mm');
        timeSlots[date.format('YYYY-MM-DD')] = [timeString];
      }
      date.add(1, 'days');
    }

    this.state = {
      selectedDates: timeSlots
    }

    this._saveSend = this._saveSend.bind(this);
    this._saveClose = this._saveClose.bind(this);
    this._addTimeSlot = this._addTimeSlot.bind(this);
    this._removeTimeSlot = this._removeTimeSlot.bind(this);
    this._updateTimeSlot = this._updateTimeSlot.bind(this);
    this._renderTimeSlots = this._renderTimeSlots.bind(this);
  }

  _updateTimeSlot(date, index, type, value) {
    let { selectedDates } = this.state;
    let split = selectedDates[date][index].split('_');

    if (type === 'start') {
      selectedDates[date][index] = value + '_' + split[1];
    } else {
      selectedDates[date][index] = split[0] + '_' + value;
    }

    this.setState({
      selectedDates
    })
  }

  _addTimeSlot(date) {
    let { selectedDates } = this.state;

    selectedDates[date].push('09:00_10:00');

    this.setState({
      selectedDates
    })
  }

  _removeTimeSlot(date, index) {
    let { selectedDates } = this.state;
    
    delete selectedDates[date][index];
    
    this.setState({
      selectedDates
    })
  }

  _saveClose() {
    let { onSave, onSend, onClose } = this.props;

    onSave(this.state.selectedDates);
    onClose();
  }

  _saveSend() {
    let { onSave, onSend, onClose } = this.props;

    onSave(this.state.selectedDates);
    onSend();
  }

  _renderTimeSlots() {
    let selectedDates = Object.keys(this.state.selectedDates);
    let listItems = selectedDates.map((date) => {
      return(
        <List key={date}>
          <ListItem justify='between'>
            <Title>{ moment(date).format('MMM DD YYYY') }</Title>
            <div>
              <Button onClick={() => this._addTimeSlot(date)}><AddIcon /></Button>
            </div>
          </ListItem>
          {
            this.state.selectedDates[date].map((slot, index) => {
              let times = slot.split('_');
              return (
                <ListItem justify='around' key={date + '_' + index}>
                  <div pad='small'>
                    <Select placeHolder='Start Time' value={times[0]} onChange={(target) => { this._updateTimeSlot(date, index, 'start', target.value) }}
                      options={['23:00', '22:00', '21:00', '20:00', '19:00', '18:00', '17:00', '16:00', '15:00', '14:00', '13:00', '12:00', '11:00', '10:00', '09:00', '08:00', '07:00', '06:00', '05:00', '04:00', '03:00', '02:00', '01:00', '00:00']} />
                  </div>
                  <div pad='small'>
                    <Select placeHolder='End Time' value={times[1]} onChange={(target) => { this._updateTimeSlot(date, index, 'end', target.value) }}
                      options={['23:00', '22:00', '21:00', '20:00', '19:00', '18:00', '17:00', '16:00', '15:00', '14:00', '13:00', '12:00', '11:00', '10:00', '09:00', '08:00', '07:00', '06:00', '05:00', '04:00', '03:00', '02:00', '01:00', '00:00']} />
                  </div>
                  <div>
                    <Button href='#' onClick={() => { this._removeTimeSlot(date, index) }}><RemoveIcon /></Button>
                  </div>
                </ListItem>
              );
            })
          }
        </List>
      );
    });
    return listItems;
  }

  render() {
    let listItems = this._renderTimeSlots();
    const { classes, ...other } = this.props;
    
    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <Typography gutterBottom variant="headline" component="h2">
          <span style={{ color: 'white', fontWeight: '600' }}>Add Calendar Event</span>
        </Typography>
        <div>
          <form>
            { listItems }
            <div>
              <div pad='medium' direction='row'>
                <Button
                  color="primary"
                  onClick={this._saveClose}
                >Save & Send All</Button>&nbsp;
                <Button label='Save & Send All'
                  color="primary"
                  onClick={this._saveSend}
                >Save & Send All</Button>
              </div>
            </div>
          </form>
        </div>
      </Dialog>
    );
  }
}

const select = state => ({
  calendar: state.calendar
});

export default withStyles(styles)(connect(select)(AddCalendarEventLayer));
