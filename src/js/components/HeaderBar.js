import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTagSearch from 'react-tag-autocomplete';

import { logout } from '../actions/session';
import { updateSearchTags, removeSearchTag, addSearchTag, updateSearchTerm, updateSearchClear } from '../actions/search';
import { remoteContactSearch } from '../actions/contacts';
import Sidebar from 'react-sidebar';
import LeftSidebar from '../components/LeftSidebar';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class HeaderBar extends Component {
    constructor(props) {
        super(props);

        let ReactTags;

        this.state = {
            open: false
        }

        this._onLogout = this._onLogout.bind(this);
        this.sidebar = <LeftSidebar close={this.closeModal} onLogout={this._onLogout.bind(this)} />;
        this.onSetOpen = this.onSetOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setOfferSidebar = this.setOfferSidebar.bind(this);
    }

    _onLogout(event) {
        event.preventDefault();
        this.props.dispatch(logout());
    }

    _handleTagDelete(i) {
        let { dispatch } = this.props;
        dispatch(removeSearchTag(i));
    }

    _handleTagAddition(tag) {
        let { dispatch } = this.props;
        dispatch(addSearchTag(tag.name));
        dispatch(updateSearchTerm(''));
    }

    onSetOpen(open) {
        this.setState({ open });
    }

    openModal() {
        this.setState({ modalIsOpen: true });
    }

    closeModal() {
        this.setState({ open: false });
    }

    handleClose() {
        this.setState({ isShowingModal: false });
    }

    setOfferSidebar() {
        this.sidebar = <LeftSidebar handleClose={() => { this.closeModal(); }} onLogout={this._onLogout.bind(this)} />;
        this.setState({ open: true });
        this.setState({ isShowingModal: false });
    }

    _clearSearchBar() {
        this.ReactTags.state.query = '';
        this.ReactTags.input.input.focus();
        this.props.dispatch(updateSearchClear(false));
    }

    componentDidUpdate() {
        if (this.props.search_reset) {
            this._clearSearchBar();
        }
    }

    _toggleSearchBarVisibility(e) {
        let searchContainer = document.getElementsByClassName('headerSearchWrapper')[0];
        let searchInput = document.getElementsByClassName('headerSearch')[0].firstChild;
        searchContainer.classList.toggle('visible');
        searchInput.focus();
    }

    render() {
        const sidebarProps = {
            sidebar: this.sidebar,
            sidebarClassName: 'custom-sidebar-class',
            open: this.state.open,
            onSetOpen: this.onSetOpen,
        };


        const tags = this.props.tags.map(tag => {
            return { name: tag }
        });

        const { dropAlign, colorIndex, session: { name: userName }, classes } = this.props;
        return (
            <Sidebar {...sidebarProps} pullLeft size='large'>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.setOfferSidebar}>
                            {
                                this.props.hideNavBar ?
                                    '' :
                                    <MenuIcon />
                            }
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            {
                                this.props.hideNavBar ?
                                    <IconButton className='bizintro-logo'>
                                        Bizintro
                                    </IconButton> :
                                    <IconButton className='bizintro-logo' href='/'>
                                        Bizintro
                                    </IconButton>
                            }
                        </Typography>
                        {
                            this.props.searchBar ?
                                <SearchIcon onClick={this._toggleSearchBarVisibility} className='headerSearchIcon' />
                                :
                                ''
                        }
                        {
                            this.props.searchBar ?
                                <ReactTagSearch
                                    ref={(ip) => this.ReactTags = ip}
                                    classNames={{ root: 'react-tags headerSearchContainer', searchInput: 'react-tags__search-input headerSearch' }}
                                    tags={tags}
                                    delimiters={[9, 13, 32]}
                                    handleDelete={this._handleTagDelete.bind(this)}
                                    handleAddition={this._handleTagAddition.bind(this)}
                                    handleInputChange={this.props.searchInputChange ? this.props.searchInputChange : null}
                                    placeholder='Make Intro or Search'
                                    allowNew={true} />
                                :
                                ''
                        }
                    </Toolbar>
                    <Tabs value={this.props.activeTab} onChange={this.props.handleChange} className={'tobbarTab'}>
                        <Tab label="History" />
                        <Tab label="Contacts" />
                        <Tab label="Archives" className={'topBarRight'}/>
                    </Tabs>
                </AppBar>
            </Sidebar>
        );
    }
}

const select = state => ({
    session: state.session,
    tags: state.search.tags,
    search_reset: state.search.reset
});

export default withStyles(styles)(connect(select)(HeaderBar));