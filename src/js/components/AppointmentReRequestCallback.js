import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { getIntroductionToken } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { addToast } from '../actions/toasts';
import { startAppointmentRequest, postAppointmentReRequest } from '../api/appointments';

class AppointmentReRequestCallback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      intro_uuid: this.props.match.params.intro_uuid,
      appt_uuid: this.props.match.params.appt_uuid,

    };

    sessionStorage.intro_uuid = this.state.intro_uuid;
  }

  componentWillMount() {
    const { dispatch, history } = this.props;
    const { router } = this.context;
    const intro_uuid = this.state.intro_uuid;
    const appt_uuid = this.state.appt_uuid;

    dispatch(getIntroductionToken(appt_uuid, "appt_request", (payload) => {
      localStorage.setItem('newUser', payload.new);
      localStorage.setItem('username', payload.first_name);
      localStorage.setItem('email', payload.email);

      if (payload.token) {
        postAppointmentReRequest(appt_uuid, payload.token).then((payload) => {
          if (payload.status && payload.status === 'success') {
            sessionStorage.setItem('recipient_email', payload.email);
            sessionStorage.setItem('contact_id', payload.contact_id);
            sessionStorage.setItem('re_appt', payload.appt)
            sessionStorage.setItem('rerequest', true);
            dispatch(addToast('Please select a date from the calendar to complete your request!', 'ok'));
            history.push('/appointment');
          } else {
            dispatch(addToast(payload.message, 'critical'));
            history.push('/login');
          }
        });
      }
    }));
  }

  render() {
    return (
      <LoadingSpinner />
    );
  }
}

export default connect()(AppointmentReRequestCallback);
