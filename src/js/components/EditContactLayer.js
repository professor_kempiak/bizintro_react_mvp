import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { editContact, inviteToFillProfile } from '../actions/contacts';
import { addToast } from '../actions/toasts';
import CustomReactQuill from './CustomReactQuill';
import NumberFormat from 'react-number-format';
const ReactTags = require('react-tag-autocomplete')

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

const variantIcon = {
  error: ErrorIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

class EditContactLayer extends Component {
  constructor(props) {
    super(props);

    let occupation = this.props.contact.occupation;
    let title = occupation ? occupation.split(' at ')[0] : '';
    let company = occupation ? occupation.split(' at ')[1] : '';
    let tags = [];

    if (this.props.contact.tags !== undefined ) {
      this.props.contact.tags.map((tag, index) => {
        if (tag.length > 0) {
          tags.push({
            id: index, 
            name: tag
          })
        }
      });      
    }

    this.state = {
      notification: null,
      id: this.props.contact.id,
      first_name: this.props.contact.first_name,
      last_name: this.props.contact.last_name,
      title: title ? title.trim() : title,
      company: company ? company.trim() : company,
      email: this.props.contact.email,
      mobile: this.props.contact.phone_number,
      address: this.props.contact.address,
      bio: this.props.contact.bio,
      tags: tags,
      token: this.props.token ? this.props.token : localStorage.getItem('token'),
      suggestions: [
        { id: 3, name: "Developer"},
        { id: 4, name: "Designer"},
        { id: 5, name: "QA"},
        { id: 6, name: "Project Manager"},
        { id: 7, name: "Sales Analyst"},
        { id: 8, name: "Master Data Departmental"},
        { id: 9, name: "Finance Departmental "},
        { id: 10, name: "Cost Analyst"},
        { id: 11, name: "Finance Manager"},
        { id: 12, name: "Finance Director"},      
        { id: 13, name: "Marketing Manager"},
        { id: 14, name: "Export Order Manager"}            
      ]  
    }

    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleQuillChange = this._handleQuillChange.bind(this);
    this._submitEditContactForm = this._submitEditContactForm.bind(this);
    this._handlePhoneChange = this._handlePhoneChange.bind(this);
  }

  _submitEditContactForm() {
    let { session, dispatch } = this.props;
    let tags = [];
    this.state.tags.map(tag => {
      tags.push(tag.name);
    })
    let contact = {
      id: this.state.id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      title: this.state.title,
      company: this.state.company,
      primary_email: this.state.email,
      emails: [this.state.email],
      primary_phone_number: this.state.mobile,
      phone_numbers: [this.state.mobile],
      primary_address: this.state.address,
      addresses: [this.state.address],
      tags: tags,
      bio: this.state.bio
    }
    dispatch(editContact(contact, this.state.token, (response) => {
      if (response.success) {
        dispatch(addToast('Contact updated successfully!', 'ok'));
        this.props.onClose();
      } else {
        if (!response.message) { response.message = 'Error updating contact' }
        dispatch(addToast(response.message, 'critical'));
      }
    }));
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  
  _handlePhoneChange(event) {
      this.setState({
          mobile: event.formattedValue
      }); 
  }

  _handleQuillChange(value) {
    this.setState({
      bio: value
    });
  }

  handleDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }
  
  handleAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
  }

  inviteToFillProfile() {
    let { session, dispatch } = this.props;
    dispatch(inviteToFillProfile(this.props.contact.id, this.state.token, (response) => {
      if (response.success) {
        if (!response.message) dispatch(addToast('Sent email successfully!', 'ok'));
        dispatch(addToast(response.message, 'ok'));
      } else {
        if (!response.message) { response.message = 'Error sending invitation email' }
        dispatch(addToast(response.message, 'critical'));
      }
    }))
  }


  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <DialogTitle id="form-dialog-title">Edit Contact</DialogTitle>
        <DialogContent>
          {
            this.props.showShareButton ?
              <a href="JavaScript:Void(0);" onClick={this.inviteToFillProfile.bind(this)}>
                Invite contact to add information to my contact card.
              </a> :
              null
          }
          <form onSubmit={this._submitEditContactForm}>
            {
              this.state.notification ?
                <div>
                  <Snackbar
                    open={true}
                    // onClose={this.handleClose}
                    className={classes.snackbar}
                    autoHideDuration={2000}
                  >
                    <MySnackbarContentWrapper
                      // onClose={this.handleClose}
                      variant="error"
                      message={this.state.notification}
                    />
                  </Snackbar>
                </div> :
                null
            }
            <div className="">
              <div className="first-name">
                <TextField
                  className={classes.margin}
                  label="First Name"
                  id="firstName"
                  name="first_name"
                  placeholder="John"
                  value={this.state.first_name}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="last-name">
                <TextField
                  className={classes.margin}
                  label="Last Name"
                  id="lastName"
                  name="last_name"
                  placeholder="Doe"
                  value={this.state.last_name}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="email">
                <TextField
                  className={classes.margin}
                  label="Email"
                  id="email"
                  name="email"
                  placeholder="johnDoe@gmail.com"
                  value={this.state.email}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="mobile">
                <NumberFormat format="(###) ###-####" value={this.state.mobile} onValueChange={this._handlePhoneChange} mask="_" />
              </div>
              <div className="address">
                <TextField
                  className={classes.margin}
                  label="Address"
                  id="address"
                  name="address"
                  placeholder="123 Steel Street"
                  value={this.state.address}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="title">
                <TextField
                  className={classes.margin}
                  label="Title"
                  id="title"
                  name="title"
                  placeholder="Electrician"
                  value={this.state.title}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="company">
                <TextField
                  className={classes.margin}
                  label="Company"
                  id="company"
                  name="company"
                  placeholder="Vancouver Electric"
                  value={this.state.company}
                  onChange={this._handleInputChange}
                />
              </div>
              <div className='no-overflow'>
                <ReactTags
                  classNames={{ root: 'react-tags tagsContainer', searchInput: 'react-tags__search-input tagsSearchInput', selectedTag: 'react-tags__selected-tag tagsSelectedTag', selectedTagName: 'react-tags__selected-tag-name tagsSelectedTagName' }}
                  tags={this.state.tags}
                  allowNew={true}
                  // suggestions={this.state.suggestions}
                  handleDelete={this.handleDelete.bind(this)}
                  handleAddition={this.handleAddition.bind(this)} />
              </div>
              <div>
                <div pad='small'>
                  <CustomReactQuill
                    onChange={this._handleQuillChange}
                    value={this.state.bio} />
                </div>
              </div>
            </div>
            <div className={classes.buttonContainer}>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className={classes.button}
                onClick={this._submitEditContactForm}
              >
                Save Changes
              </Button>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    );
  }
}

const select = state => ({
  session: state.session
});

export default withStyles(styles)(connect(select)(EditContactLayer));

// { this.props.showShareButton ? <Anchor href={'contact-details/' + this.state.id + '/' + this.state.token} label='Invite contact to add information to my contact card.' /> : null }