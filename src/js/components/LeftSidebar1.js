/* eslint-disable indent */
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
// import Responsive from 'grommet/utils/Responsive';
import { connect } from 'react-redux';

// import Box from 'grommet/components/Box';
// import Header from 'grommet/components/Header';
// import Sidebar from 'grommet/components/Sidebar';
// import Title from 'grommet/components/Title';
// import Menu from 'grommet/components/Menu';
// import Anchor from 'grommet/components/Anchor';
// import CloseIcon from 'grommet/components/icons/base/Close';
// import Button from 'grommet/components/Button';
// import Value from 'grommet/components/Value';



import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';

import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';

const drawerWidth = 300;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    closeIcon: {
        color: 'white'
    },
    link: {
        textDecoration: 'none'
    },
    brandEmail: {
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        transform: 'none !important'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    userName: {
        color: '#425563',
        fontFamily: 'Roboto',
        fontSize: 15,
        fontWeight: 700
    },
    userEmail: {
        color: '#425563',
        fontFamily: 'Roboto',
        fontSize: 12,
        fontWeight: 400
    }
});

class LeftSidebar1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { classes, theme , curPage} = this.props;
        let homeClass = '';
        if(curPage == 'home'){
            homeClass = 'curpage'
        }
        let scheduleClass = '';
        if(curPage == 'schedule'){
            scheduleClass = 'curpage'
        }
        let templateClass = '';
        if(curPage == 'template'){
            templateClass = 'curpage'
        }
        let rankingClass = '';
        if(curPage == 'ranking'){
            rankingClass = 'curpage'
        }
       
        return (
                <Drawer
                    variant="persistent"
                    anchor={this.props.anchor}
                    open={this.props.open}
                    className={'leftDrawerClass'}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.props.handleDrawerClose}>
                            <i className="material-icons">
                                menu
                            </i>
                        </IconButton>
                        {this.props.open ? <div >
                                                <Typography className={classes.userName}>
                                                    Hi Justin!
                                                </Typography>
                                                <Typography className={classes.userEmail}>
                                                    6bitjs@gmail.com
                                                </Typography>
                                            </div>
                            : null}

                    </div>
                    <Link to='/' className={classes.link}>
                        <MenuItem className={homeClass}>
                            <i className="material-icons">
                                home
                            </i>
                            {this.props.open ? <ListItemText primary="Home" /> : null}
                        </MenuItem>
                    </Link>
                    <Link to='/schedule' className={classes.link}>
                        <MenuItem className={scheduleClass}>
                            <i className="material-icons">
                                calendar_today
                            </i>
                            {this.props.open ? <ListItemText primary="Calendar" /> : null}
                        </MenuItem>
                    </Link>
                    <Link to='/templates' className={classes.link}>
                        <MenuItem className={templateClass}>
                            <i className="material-icons">
                                looks_one
                            </i>
                            {this.props.open ? <ListItemText primary="Templates" /> : null}
                        </MenuItem>
                    </Link>
                    <Link to='/profile' className={classes.link}>
                        <MenuItem className={rankingClass}>
                            <i className="material-icons">
                                people
                            </i>
                            {this.props.open ? <ListItemText primary="Ranking" /> : null}
                        </MenuItem>
                    </Link>
                </Drawer>
        );
    }
}

const select = state => ({
    session: state.session
});

export default withStyles(styles, { withTheme: true })(connect(select)(LeftSidebar1));


// <Anchor href='/landingpage'>Landing Page</Anchor>
// <Anchor href='/landingpage2'>Landing Page 2</Anchor>
// <Anchor href='/contacts'>Contacts</Anchor>
// <Anchor href='/contactdetails'>Contact Details</Anchor>
// <Anchor href='/appointment'>Calendar</Anchor>
// <Anchor href='/map'>Map</Anchor>
// <Anchor href='/manage'>Introductions</Anchor>