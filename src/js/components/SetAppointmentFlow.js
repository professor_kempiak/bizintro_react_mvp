import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { addToast } from '../actions/toasts';
import { getIntroductionToken } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { startAppointmentRequest } from '../api/appointments';
// import { Card, Box, Anchor, Heading, Layer, Label, Select, Button, List, ListItem,Title, AddIcon , SubtractIcon, DateTime} from 'grommet';
import moment from 'moment';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});


class SetAppointmentFlow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      styles:{
        left: 0,
        top: 0,
      },
      startStyles: {
        left: 0,
        top: 0,
      },
      isShowStart: false,
      date: '',
      baseDateString: '',
      startTime: '',
      endTime: '',
      startTimeValue: {},
      endTimeValue: {},
      endTimeList: [
        {index: 0, value: '00:00am'},{index: 1, value: '00:30am'},
        {index: 2, value: '01:00am'},{index: 3, value: '01:30am'},
        {index: 4, value: '02:00am'},{index: 5, value: '02:30am'},
        {index: 6, value: '03:00am'},{index: 7, value: '03:30am'},
        {index: 8, value: '04:00am'},{index: 9, value: '04:30am'},
        {index: 10, value: '05:00am'},{index: 11, value: '05:30am'},
        {index: 12, value: '06:00am'},{index: 13, value: '06:30am'},
        {index: 14, value: '07:00am'},{index: 15, value: '07:30am'},
        {index: 16, value: '08:00am'},{index: 17, value: '08:30am'},
        {index: 18, value: '09:00am'},{index: 19, value: '09:30am'},
        {index: 20, value: '10:00am'},{index: 21, value: '10:30am'},
        {index: 22, value: '11:00am'},{index: 23, value: '11:30am'},
        {index: 24, value: '12:00pm'},{index: 25, value: '12:30pm'},
        {index: 26, value: '01:00pm'},{index: 27, value: '01:30pm'},
        {index: 28, value: '02:00pm'},{index: 29, value: '02:30pm'},
        {index: 30, value: '03:00pm'},{index: 31, value: '03:30pm'},
        {index: 32, value: '04:00pm'},{index: 33, value: '04:30pm'},
        {index: 34, value: '05:00pm'},{index: 35, value: '05:30pm'},
        {index: 36, value: '06:00pm'},{index: 37, value: '06:30pm'},
        {index: 38, value: '07:00pm'},{index: 39, value: '07:30pm'},
        {index: 40, value: '08:00pm'},{index: 41, value: '08:30pm'},
        {index: 42, value: '09:00pm'},{index: 43, value: '09:30pm'},
        {index: 44, value: '10:00pm'},{index: 45, value: '10:30pm'},
        {index: 46, value: '11:00pm'},{index: 47, value: '11:30pm'}
      ],
      startTimeList: [
        {index: 0, value: '00:00am'},{index: 1, value: '00:30am'},
        {index: 2, value: '01:00am'},{index: 3, value: '01:30am'},
        {index: 4, value: '02:00am'},{index: 5, value: '02:30am'},
        {index: 6, value: '03:00am'},{index: 7, value: '03:30am'},
        {index: 8, value: '04:00am'},{index: 9, value: '04:30am'},
        {index: 10, value: '05:00am'},{index: 11, value: '05:30am'},
        {index: 12, value: '06:00am'},{index: 13, value: '06:30am'},
        {index: 14, value: '07:00am'},{index: 15, value: '07:30am'},
        {index: 16, value: '08:00am'},{index: 17, value: '08:30am'},
        {index: 18, value: '09:00am'},{index: 19, value: '09:30am'},
        {index: 20, value: '10:00am'},{index: 21, value: '10:30am'},
        {index: 22, value: '11:00am'},{index: 23, value: '11:30am'},
        {index: 24, value: '12:00pm'},{index: 25, value: '12:30pm'},
        {index: 26, value: '01:00pm'},{index: 27, value: '01:30pm'},
        {index: 28, value: '02:00pm'},{index: 29, value: '02:30pm'},
        {index: 30, value: '03:00pm'},{index: 31, value: '03:30pm'},
        {index: 32, value: '04:00pm'},{index: 33, value: '04:30pm'},
        {index: 34, value: '05:00pm'},{index: 35, value: '05:30pm'},
        {index: 36, value: '06:00pm'},{index: 37, value: '06:30pm'},
        {index: 38, value: '07:00pm'},{index: 39, value: '07:30pm'},
        {index: 40, value: '08:00pm'},{index: 41, value: '08:30pm'},
        {index: 42, value: '09:00pm'},{index: 43, value: '09:30pm'},
        {index: 44, value: '10:00pm'},{index: 45, value: '10:30pm'},
        {index: 46, value: '11:00pm'},{index: 47, value: '11:30pm'}
      ],
      showAddTime: false,
      selectedDates: {}
    }
  }

  componentWillMount() {
  }

  componentWillReceiveProps(newProps) {
  }

  componentDidMount() {
    let date = moment(this.props.event.start).format('MMMM DD , YYYY'),
        baseDateString = moment(this.props.event.start).format('YYYY-MM-DD'),
        startTime=moment(this.props.event.start),
        startTime1=moment(this.props.event.start),        
        endTime=moment(this.props.event.end),
        diff=endTime.diff(startTime, 'minutes', true),
        startTimeList=[{value:startTime.format('HH:mma'), index:0, date: startTime}],
        endTimeList=[];

    let selectedDates = this.getTimeSlots();

    this.setState({
      styles: {
        top: this.props.top,
        left: this.props.left,
        position: 'absolute',
        zIndex: '10',
        height: '296px',
        background: 'none',
        width: '482px'
      },
      date: date,
      baseDateString: baseDateString,
      selectedDates: selectedDates,
      startTimeValue: {value:startTime.format('h:mm a'), index:0, date:startTime},
      endTimeValue: {value: endTime.format('h:mm a'), index:0, date:endTime}
    });
    setTimeout(() => {
      $('ul.grommetux-list li').first().css('display', 'none');
    }, 50);

    $('.datepicker_modal .grommetux-date-time__input').attr('readonly', true).prop('readonly', true)
    $('.datepicker_modal .grommetux-date-time__input').click(() => {
      $('.datepicker_modal .grommetux-button').click();
    })
  }

  showStartTime(e) {
    let x=$(e.target).position().left,
        y=$(e.target).position().top + 20
    this.setState({
      startStyles:{
        left: x,
        top: y, 
        position: 'absolute',
        zIndex: '11',
        background: '#ccc'
      },
      isShowStart: true
    })
  }

  onSelectStartTime(e) {
    this.setState({
      startTimeValue: e.target.value,
    });
    this._updateTimeSlot(this.state.baseDateString, 0, 'start', this.get24HourTimes(e.target.value));
  }

  onSelectendTime(e) {
    this.setState({endTimeValue: e.target.value})
    this._updateTimeSlot(this.state.baseDateString, 0, 'end', this.get24HourTimes(e.target.value));
  }

  getTimeSlots() {
    let startTime = moment(this.props.event.start);
    let endTime = moment(this.props.event.end);
    let date = startTime;
    let difference = endTime.diff(startTime, 'days') + 1;
    let timeSlots = {};

    for (let x = 0; x < difference; x++) {
      if (this.props.currentView === 'month') {
        timeSlots[date.format('YYYY-MM-DD')] = [];
      } else {
        let timeString = startTime.format('HH:mm') + '_' + endTime.format('HH:mm');
        timeSlots[date.format('YYYY-MM-DD')] = [timeString];
      }
      date.add(1, 'days');
    }

    return timeSlots;
  }

  onOnlySave() {
    this.props.onSave(this.state.selectedDates, false);
    this.props.onClose();
  }

  onSave() {
    this.props.onClose();
    this.props.onSave(this.state.selectedDates, true);
  }

  _updateTimeSlot(date, index, type, value) {
    let { selectedDates } = this.state;
    let split = selectedDates[date][index].split('_');

    if (type === 'start') {
      selectedDates[date][index] = value + '_' + split[1];
    } else {
      selectedDates[date][index] = split[0] + '_' + value;
    }

    this.setState({
      selectedDates
    })
  }

  _addTimeSlot(date) {
    let { selectedDates } = this.state;

    selectedDates[date].push('09:00_10:00');

    this.setState({
      selectedDates
    })
  }

  _removeTimeSlot(date, index) {
    let { selectedDates } = this.state;
    
    selectedDates[date] = selectedDates[date].splice(index, 1);
    
    this.setState({
      selectedDates
    })
  }

  _renderTimeSlots() {
    let selectedDates = Object.keys(this.state.selectedDates);
    if (selectedDates == undefined)
      return ;

    let listItems = selectedDates.map((date) => {
      return(
        <List key={date}>
          <ListItem justify='between'>
            <div>
              <Button onClick={() => this._addTimeSlot(date)}><AddIcon /></Button>
            </div>
          </ListItem>
          {
            this.state.selectedDates[date].slice(1).map((slot, index) => {
              let times = slot.split('_');
              const options = ['11:00pm', '10:00pm', '09:00pm', '08:00pm', '07:00pm', '06:00pm', '05:00pm', '04:00pm', '03:00pm', '02:00pm', '01:00pm', '12:00pm', '11:00am', '10:00am', '09:00am', '08:00am', '07:00am', '06:00am', '05:00am', '04:00am', '03:00am', '02:00am', '01:00am', '00:00am']
              return (
                <ListItem justify='around' key={date + '_' + index}>
                  <div>
                    <Select placeHolder='Start Time' value={this.get12HourTimes(times[0])} onChange={(e) => { this._updateTimeSlot(date, index + 1, 'start', this.get24HourTimes(e.target.value)) }}>
                      {
                        options.map((item, index) => {
                          return (
                            <MenuItem value={item}>{item}</MenuItem>
                          )
                        })
                      }
                    </Select>
                  </div>
                  <div>
                    <Select placeHolder='End Time' value={this.get12HourTimes(times[1])} onChange={(e) => { this._updateTimeSlot(date, index + 1, 'end', this.get24HourTimes(e.target.value)) }}>
                     {
                        options.map((item, index) => {
                          return (
                            <MenuItem value={item}>{item}</MenuItem>
                          )
                        })
                      }
                    </Select>
                  </div>
                  <div>
                    <IconButton href='#' onClick={() => { this._removeTimeSlot(date, index + 1) }}><RemoveIcon /></IconButton>
                  </div>
                </ListItem>
              );
            })
          }
        </List>
      );
    });
    return listItems;
  }

  get24HourTimes(value) {
    let isPm = false;
    if (value.indexOf('pm') != -1) {
      isPm = true
    }
    let hour = (parseInt(value.slice(0,2)) + (isPm ? 12 : 0));
    hour = hour.toString().length == 1 ? '0' + hour.toString() : hour.toString();
    return hour + value.slice(2,5);
  }

  get12HourTimes(value) {
    let hour = parseInt(value.slice(0,2));
    if (hour > 12) {
      return (hour-12).toString() + value.slice(2) + 'pm';
    } else {
      return hour.toString() + value.slice(2) + 'am';
    }
  }

  onAdditionalTime() {
    $('ul.grommetux-list li > div > button').click();
  }

  onChangeDate(value) {
    let renameProp = (
        oldProp,
        newProp,
    { [oldProp]: old, ...others }
    ) => ({
        [newProp]: old,
        ...others
    });

    this.setState({date: value});
    let selectedDates = this.state.selectedDates;
    this.setState({selectedDates: renameProp(Object.keys(selectedDates)[Object.keys(selectedDates).length - 1], moment(value).format('YYYY-MM-DD'), selectedDates)});
  }

  render() {
    let { classes } = this.props;
    let listItems = this._renderTimeSlots();

    let dateElement = this.props.isSetDatePicker ? <DateTime className='datepicker_modal' value={this.state.date} format='MMMM DD, YYYY' onChange={this.onChangeDate.bind(this)} /> : this.state.date;


    let startElement = <Select inline={false}
                        placeHolder='start time'
                        multiple={false}
                        value={this.state.startTimeValue}
                        onChange={this.onSelectStartTime.bind(this)}
                        options={this.state.startTimeList}>
                          {
                            this.state.startTimeList.map((item, index) => {
                              return (
                                <MenuItem value={item.value}>{item.value}</MenuItem>
                              )
                            })
                          }
                        </Select>

    let endElement = <Select inline={false}
                        placeHolder='end time'
                        multiple={false}
                        value={this.state.endTimeValue}
                        onChange={this.onSelectendTime.bind(this)}
                        options={this.state.endTimeList}>
                          {
                            this.state.endTimeList.map((item, index) => {
                              return (
                                <MenuItem value={item.value}>{item.value}</MenuItem>
                              )
                            })
                          }
                        </Select>

    return(
        <Material.Paper style={this.state.styles} className="grommetux-layer custom-appointment">
          <div className="grommetux-layer__container" id="889365262" style={{padding: 0,height:'296px !important'}}>
            <a tabIndex="-1" style={{outline: 'none'}}></a>
            <div className="grommetux-layer__closer">
              <button type="button" className="grommetux-button grommetux-button--plain" onClick={this.props.onClose}>
                <span className="grommetux-button__icon">
                  <svg version="1.1" viewBox="0 0 24 24" width="24px" height="24px" role="img" className="grommetux-control-icon grommetux-control-icon-close grommetux-control-icon--responsive" aria-label="Close  Layer">
                    <path fill="none" stroke="#000" strokeWidth="2" d="M3,3 L21,21 M3,21 L21,3"></path>
                  </svg>
                </span>
              </button>
            </div>
            <div className="grommetux-box grommetux-box--direction-column grommetux-box--responsive grommetux-box--pad-medium" style={{height:'296px'}}>
              <div className="grommetux-box grommetux-box--direction-column grommetux-box--responsive grommetux-box--pad-small">
              <Typography gutterBottom variant="headline" component="h2">
                <span style={{ fontWeight: '600' }}>I am available to meet with Justin on</span>
              </Typography>
              <Typography gutterBottom variant="headline" component="h2">
                <span style={{ fontWeight: '600' }}>{dateElement} between {startElement} and {endElement}.</span>
              </Typography>
              </div>
              <div className="grommetux-box grommetux-box--direction-row grommetux-box--responsive grommetux-box--pad-small" style={{width: '100%'}}>
                <Material.Button variant="contained" fullWidth={true} className={classes.button}  onClick={this.onOnlySave.bind(this)}>
                  Add a different time range
                </Material.Button>               
              </div>
              <div className="grommetux-box grommetux-box--direction-row grommetux-box--responsive grommetux-box--pad-small" style={{width: '100%'}}>
                <Material.Button color='primary' variant="contained" fullWidth={true} className={classes.button}  onClick={this.onSave.bind(this)}>
                  Finalize appointment details
                </Material.Button> 
              </div>
              
            </div>
          </div>            
        </Material.Paper>
      )
  }
}

const select = state => ({

});

export default withStyles(styles)(connect(select)(SetAppointmentFlow));
