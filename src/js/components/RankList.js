import React, { Component } from 'react';
import { connect } from 'react-redux';
import Box from 'grommet/components/Box';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import ChipInput from 'material-ui-chip-input'

import { gatherContacts, remoteContactSearch, saveRankContacts } from '../actions/contacts';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';
import { addToast } from '../actions/toasts';

const styles = theme => ({
  root: {
    display: 'flex',
    padding: '12px',
    flexFlow: 'column',    
  },
  tabBar: {
    marginTop: '4.1em',
    padding: '0 1.5em'
  },
  avatar: {
    margin: 10,
  },
  row: {
  	marginTop: '5px',
  	marginBottom: '5px',
  	zIndex: '10'
  }
});

const SortableItem = SortableElement(({contact}) =>
  <li>
    <Material.Paper className='drop_row'>
      <Material.Grid container spacing={8} direction="row" justify="center"  alignItems="center">

        <Material.Grid item xs={1}>
          <Material.Avatar alt="Remy Sharp" src={contact.avatar_link} className={'drop_row_avatar'} />
        </Material.Grid>

        <Material.Grid item xs={4}>
          <Material.Typography gutterBottom variant="subheading">
                  {contact.first_name} {contact.last_name}
                </Material.Typography>
        </Material.Grid>

        <Material.Grid item xs={3}>
          <Material.Typography  gutterBottom variant="subheading">
                  {contact.occupation == "" ? '' : contact.occupation.split('at')[0]}
                </Material.Typography>
        </Material.Grid>

        <Material.Grid item xs={3}>
          <Material.Typography  gutterBottom variant="subheading">
                  {contact.occupation == "" ? '' : contact.occupation.split('at')[1]}
                </Material.Typography>
        </Material.Grid>

        <Material.Grid item xs={1}>
          <Material.Typography  gutterBottom variant="subheading">
                  {contact.global_rank == 100000 ? '' : contact.global_rank}
                </Material.Typography>
        </Material.Grid>
      </Material.Grid>
    </Material.Paper>
  </li>
);

// 
class RankList extends Component { 
	constructor(props) {
    super(props);

    this.state = {
    	page: 1,
    	perpage: 25,
    	hasMoreItems: false,
    	pageLoaded: false,
    	end_of_pages: false,
    	showLoading: true,
      tagSearch: false,
      tags: [],
      contacts: [{
        "phone_number": "(773) 463-3806",
        "bio": "",
        "localized_address": {
          "latitude": "-87.72445",
          "longitude": "41.991449",
          "address": "3800 W. Peterson Ave.\nChicago 60659\nUSA"
        },
        "addresses": [
          "205 West Wacker Drive\nSuite 1750\nChicago, IL 60606\nUnited States of America",
          "180 N. LaSalle\nSuite 2620\nChicago, IL 60606\nUnited States of America",
          "3800 W. Peterson Ave.\nChicago 60659\nUSA"
        ],
        "count_events": 0,
        "registered": false,
        "count_intros": 0,
        "last_name": "Cendrowski 1",
        "address": "3800 W. Peterson Ave.\nChicago 60659\nUSA",
        "localized_addresses": [
          {
            "latitude": "-87.634319",
            "longitude": "41.886599",
            "address": "205 West Wacker Drive\nSuite 1750\nChicago, IL 60606\nUnited States of America"
          }
        ],
        "phone_numbers": [
          "(248) 540-5760",
          "248.765.5179",
          "(773) 463-3806",
          "(866) 717-1607"
        ],
        "emails": [
          "hc@cendsel.com"
        ],
        "tags": [],
        "occupation": " Active Trustee at Cendrowski Selecky PC",
        "first_name": "Harry",
        "avatar_link": "https://www.gravatar.com/avatar/897b5bbe176da35e83dcb591735085ad?s=80&d=mm",
        "is_bizintro": false,
        "count_emails": 511,
        "id": 7501,
        "occupations": [
          " Active Trustee at Cendrowski Selecky PC",
          " Officer, Founding Member at Cendrowski Selecky PC"
        ],
        "extra_data": {
          "avatar": ""
        },
        "email": "hc@cendsel.com"
      }]
    }

    this._onMore = this._onMore.bind(this);
    this._gatherContacts = this._gatherContacts.bind(this);
    this.gatherMoreContacts = this.gatherMoreContacts.bind(this);
  }

  _onMore() {
    this.setState({ showLoading: true });
    if (this.state.hasMoreItems 
        || this.props.search.length > 0){
      this.gatherMoreContacts();
    }
  }

  _gatherContacts(page, search=[], search_term='') {
    const { dispatch, session } = this.props,
          searchTagsAndTerm = [...search, search_term];

    this.setState({tagSearch: search.length == 0 ? false : true});
    dispatch(remoteContactSearch(
        searchTagsAndTerm || [], 
        1, 
        localStorage.token,         
        (response) => {
          if (!Array.isArray(response)) {
            dispatch(addToast('Error loading existing contacts', 'critical'));
          }
          this.setState({ 
            hasMoreItems: response.length < this.state.perpage ? false : true , 
            pageLoaded: true 
          });
        },
        true,
    ));
  }

  gatherMoreContacts() {
    const { dispatch, search, search_term } = this.props;
    if (this.state.hasMoreItems && this.state.pageLoaded) {
      let page = this.state.page + 1,
          searchTagsAndTerm = [...search, ...this.state.tags, search_term];
      
      this.setState({hasMoreItems: false, tagSearch: this.state.tags.length == 0 ? false : true});

      dispatch(remoteContactSearch(
        searchTagsAndTerm || [], 
        page, 
        localStorage.token , 
        (response) => {
          this.setState({ showLoading: false });
          if (response.length === 0 || 
              response.length < this.state.perpage) { 
            this.setState({ hasMoreItems: false });
          } else {
            this.setState({ page: page, hasMoreItems: true });
          }
        },
        true,
      ));
    }
  }

  componentWillReceiveProps(newProps) {
    if ((newProps.search !== this.props.search) || 
        (newProps.search_term !== this.props.search_term)) {
      this.setState({page: 1});
      this._gatherContacts(1, newProps.search, newProps.search_term);
    }

    if (newProps.contacts != this.props.contacts) {
      this.setState({contacts: newProps.contacts});
      if (newProps.contacts.length == 0) {
        this.setState({showLoading: false});
      }
    }
  }

  componentDidMount() {
    this._gatherContacts(this.state.page, this.props.search, this.props.search_term);
    this.setState({contacts: this.props.contacts});
    if (this.props.contacts.length == 0) {
        this.setState({showLoading: false});
      }
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    let newArray = arrayMove(this.state.contacts, oldIndex, newIndex)
    if (this.state.tagSearch == true) {
      debugger
      for (let i=oldIndex; i < newIndex+1; i++) {
        let newRank = this.state.contacts[i].global_rank;
        newArray[i].global_rank = newRank;
      }
    } else {
      newArray.map((item, index) => {
        item.global_rank = index + 1
        return item
      })      
    }

    this.setState({
      contacts: newArray
    });
  };

  onSave() {
    let rankArray = this.state.contacts.map(item => Object({id: item.id, rank: item.global_rank}));

    console.log(rankArray);
    let { dispatch } = this.props;
    let token = localStorage.getItem('token');
    this.setState({contacts: [], page: 1,showLoading: true, tags: []});
    dispatch(saveRankContacts(rankArray, token, (response) => {
      if (response.result == true) {
        dispatch(addToast('Saved Successfully', 'ok'));
        this._gatherContacts(1, this.props.search, this.props.search_term);  
        this.setState({showLoading: false});
      }
    }));
  }

  onSearchTags(tags) {
    console.log(tags)
    this.setState({contacts: [], page: 1,showLoading: true, tags: tags});
    this._gatherContacts(1, tags, '');
    this.setState({showLoading: false});
  }

	render() { 
		const { classes } = this.props;

		let SortableList, contactsHtml = '';
		if (this.state.contacts.length == 0) {
			contactsHtml = 
				<Material.Paper key={0} className={classes.row}>
					<Material.Grid container spacing={8} direction="row" justify="center"  alignItems="center">
						<Material.Grid item xs={2}>
							<Material.Typography  gutterBottom variant="subheading" noWrap={true}>
					            {this.state.showLoading ? 'Loading...' : 'Nothing to display'}
	          	</Material.Typography>
						</Material.Grid>
					</Material.Grid>
				</Material.Paper>
		} else {
      contactsHtml = null;      
		}

    SortableList = SortableContainer(({items}) => {
      return (
        <ul>
          {items.map((contact, index) => (
            <SortableItem key={`item-${index}`} index={index} contact={contact} />
          ))}
        </ul>
      );
    });
		return (
		<div className={'rank_container'}>
      
      <Material.Grid container spacing={8} justify="flex-end">
        <Material.Grid item xs={4}>
          <ChipInput
            fullWidth={true}
            placeholder={'Search Tags'}
            onChange={(chips) => this.onSearchTags(chips)}
          />
        </Material.Grid>
        <Material.Grid item xs={1}>
          <Material.Button variant="contained" color="primary" onClick={this.onSave.bind(this)}> Save </Material.Button>
        </Material.Grid>
      </Material.Grid>

			<Material.Paper elevation={0}>
				<Material.Grid container spacing={8} direction="row" justify="center"  alignItems="center">
					<Material.Grid item xs={5}>
						<Material.Typography  gutterBottom variant="subheading">
				            Base Info
			          	</Material.Typography>
					</Material.Grid>

					<Material.Grid item xs={3}>
						<Material.Typography  gutterBottom variant="subheading">
				            Title
			          	</Material.Typography>
					</Material.Grid>

					<Material.Grid item xs={3}>
						<Material.Typography  gutterBottom variant="subheading">
				            Company
			          	</Material.Typography>
					</Material.Grid>

					<Material.Grid item xs={1}>
						<Material.Typography  gutterBottom variant="subheading">
				            Rank
			          	</Material.Typography>
					</Material.Grid>
				</Material.Grid>
			</Material.Paper>

      {contactsHtml}
			<SortableList items={this.state.contacts} onSortEnd={this.onSortEnd} />

      {this.state.showLoading || this.props.contact_search_loading ? <Box align='center'><CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} /></Box> : null}

			{
			    this.state.hasMoreItems ?
			    <div style={{ display: 'flex', justifyContent: 'center' }}>
			        <Button variant="contained" size="large" color="primary" className={classes.button} onClick={this._onMore}>
			            Load More
			        </Button>
			    </div> :
			    null
			}
		</div>
		)

	}
}


const select = state => ({
    contacts: state.contacts.list,
    search: state.search.tags,
    search_term: state.search.term,
    contact_search_loading: state.contacts.loading
  
});

export default withStyles(styles)(connect(select)(RankList));