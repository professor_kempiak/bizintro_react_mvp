import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { addToast } from '../actions/toasts';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({

})

class CustomMapCompleteStep extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: null,
        }
        this.createAnother = this.createAnother.bind(this);
        this.createAppointments = this.createAppointments.bind(this);
    }

    componentDidMount() {
        this.props.saveNewZone();
    }

    createAnother() {
        this.props.anotherZone();
    }
    createAppointments() {
        console.log('create appointments');
    }
    render() {
        return (
            <div className={'formTiles'}>
                <div className={'formsubTile'}>
                    <div>
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>All set! What's next ?</span>
                        </Typography>
                        <Button variant="contained" color="primary" className={'anotherBtn'} onClick={this.createAnother}>Create another</Button>
                        <Button variant="contained" color="primary" onClick={this.createAppointments}>Create Appointment</Button>
                    </div>
                    <div className={'formBottomBtns'}>
                        <IconButton onClick={this.props.preStep}><NavigateBefore /></IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(CustomMapCompleteStep);
