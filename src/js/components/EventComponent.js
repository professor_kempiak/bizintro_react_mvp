import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { Layer, Form, Header, Heading, FormField, FormFields, TextInput, Footer, Button, Box, Notification, Anchor, Label } from 'grommet';
import { editContact } from '../actions/contacts';
import { addToast } from '../actions/toasts';
import CustomReactQuill from './CustomReactQuill';
import NumberFormat from 'react-number-format';
// import FormTrashIcon from 'grommet/components/icons/base/FormTrash';
import moment from "moment/moment";
import CustomSetAppointmentFlow from './CustomSetAppointmentFlow';
import {addCalendarEvent , removeCalendarEvent} from "../actions/calendar";
import DetailAppointmentLayer from '../components/DetailAppointmentLayer';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
  }
});

class EventComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLayer : false,
            top: 0,
            left: 0,
            appointment: {}
        }
        this.eventSelect = this.eventSelect.bind(this);
        this.deleteEvent = this.deleteEvent.bind(this);
    }
    deleteEvent(event, e){
        let { dispatch } = this.props;
        dispatch(removeCalendarEvent(event));
        e.stopPropagation();
    }
    eventSelect(e){
        let dom = document.getElementsByClassName(e.start.toISOString());
        let calendar = this.props.calendar;
        let apptList = this.props.apptList;
        let calendarEvent = this.props.event;
        let appointment = {}

        if (calendarEvent.uuid != undefined) {
            let _appointment = apptList.filter((appt) => appt.fields.uuid == calendarEvent.uuid)[0]

            let datetime = ''
            if (_appointment.fields.start_date_time) {
                datetime = (new Date(_appointment.fields.start_date_time)).toDateString() + ' - ' + (new Date(_appointment.fields.end_date_time)).toDateString();
            } else {
                datetime = (new Date(_appointment.fields.time_slot[0].fields.start_date_time)).toDateString() + ' - ' + (new Date(_appointment.fields.time_slot[0].fields.end_date_time)).toDateString();
            }
            appointment = {
                title: _appointment.fields.title,
                attendees: _appointment.fields.recipient_user_profile.first_name + ' ' + _appointment.fields.recipient_user_profile.last_name + ' & ' + _appointment.fields.user_profile.first_name + ' ' +  _appointment.fields.user_profile.last_name,
                datetime: datetime,
                location: _appointment.fields.location,
                lat: _appointment.fields.locailized_address.latitude,
                long: _appointment.fields.locailized_address.longitude,
                notes: _appointment.fields.message ? $(_appointment.fields.message).text() : ''
            }
        } else {
            datetime = calendarEvent.start.toDateString() + ' - ' + calendarEvent.end.toDateString();
            appointment = {
                title: calendarEvent.title,
                attendees: 'Google Appointment',
                datetime: datetime,
                location: '',
                lat: '',
                long: '',
                notes: ''
            }
        }

        if (dom.length > 1)
            dom = dom[1];
        else
            dom = dom[0];

        this.setState({
            showLayer : true,
            top: 200,
            left: 200,
            appointment
        });
    }
    onClose() {
        this.setState({
            showLayer: false
        })
    }
    addEvent(event, sendOrNot) {

    }

    render() {
        let calendarEvent = this.props.event;
        console.log(calendarEvent)
        let calendarEndDate = moment(calendarEvent.end);
        calendarEndDate = calendarEndDate.format('h:mm a');
        let calendarStartDate = moment(calendarEvent.start);
        calendarStartDate = calendarStartDate.format('h:mm a');
        return (
            <div className='custom-event-box'>
                {this.state.showLayer? <DetailAppointmentLayer appointment={this.state.appointment} onClose={this.onClose.bind(this)} /> : null}
                {this.props.isSetting ?
                <MenuItem><a className='eventCloseIcon'><FormTrashIcon onClick={(e) => this.deleteEvent(calendarEvent, e)} /></a></MenuItem> : ''
                }
                <Material.Typography variant="body2" gutterBottom onClick={() => this.eventSelect(calendarEvent)}>
                    {calendarStartDate+'-'+calendarEndDate}
                </Material.Typography>
                {calendarEvent.new_event ?
                    <Material.Typography variant="caption" gutterBottom align="center">
                        {calendarEvent.title || 'Appointment'}
                    </Material.Typography>
                :
                    <Material.Typography variant="caption" gutterBottom align="center">
                        {calendarEvent.title || 'Google Appointment'}
                    </Material.Typography>
                }
            </div>

        );
    }
}

const select = state => ({
    session: state.session,
    apptList: state.appointment.apptList,
    isSetting: state.appointment.isSetting
});

export default withStyles(styles)(connect(select)(EventComponent));
