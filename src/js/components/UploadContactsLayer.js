import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { uploadContactList, gatherContacts } from '../actions/contacts';
import { addToast } from '../actions/toasts';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);


class UploadContactsLayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notification: null,
      source: null,
      file: null,
      isUploading: false
    }

    this._handleChange = this._handleChange.bind(this);
    this._uploadContacts = this._uploadContacts.bind(this);
    this._handleFileChange = this._handleFileChange.bind(this);
  }

  _uploadContacts(e) {
    let { dispatch } = this.props;

    e.preventDefault();

    let token = localStorage.getItem('token');

    const formData = new FormData(e);
    formData.append('file', this.state.file);
    formData.append('source', this.state.source.value);
    formData.append('token', token);
    this.setState({isUploading: true})
    dispatch(uploadContactList(formData, token, (payload) => {
      if (payload.success) {
        dispatch(gatherContacts(localStorage.token, 1, (response) => {
          this.setState({isUploading: false});
          dispatch(addToast(payload.message, 'ok'));
          this.props.onClose();
        }));
      } else {
        dispatch(addToast(payload.error, 'critical'));
      }
    }));
  }

  _handleFileChange(e) {
    this.setState({
      file: e.target.files[0]
    })
  }

  _handleChange(e) {
    this.setState({
      source: e.option
    })
  }

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <DialogTitle id="form-dialog-title">Upload Contacts</DialogTitle>
        <DialogContent>
          <form onSubmit={this._uploadContacts}>
            {
              this.state.notification ?
                <div>
                  <Snackbar
                    open={true}
                    // onClose={this.handleClose}
                    className={classes.snackbar}
                    autoHideDuration={2000}
                  >
                    <MySnackbarContentWrapper
                      // onClose={this.handleClose}
                      variant="error"
                      message={this.state.notification}
                    />
                  </Snackbar>
                </div> :
                null
            }
            <Typography variant="p" gutterBottom>
              Here you can upload your existing contact lists. The spreadsheet used for upload must include the following fields: FirstName, LastName and EmailAddress.
            </Typography>
            <div className="form-content">
              <div className="source-list" style={{ margin: '20px 0'}}>
                <InputLabel htmlFor="age-native-simple">Contact List Source</InputLabel>
                <Select
                  native
                  value={this.state.source}
                  onChange={() => this.handleChange('age')}
                  input={<Input id="age-native-simple" />}
                >
                  <option value="e">Other</option>
                  <option value="l">LinkedIn</option>
                  <option value="s">Salesforce</option>
                  <option value="o">Outlook</option>
                </Select>
              </div>
              <div className="upload-contact" style={{ margin: '20px 0' }}>
                <InputLabel htmlFor="age-native-simple">Upload Contact List</InputLabel>
                <input ref='file' id='file' name='file' type='file' onChange={this._handleFileChange} />
              </div>
            </div>
            <div className={classes.buttonContainer}>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className={classes.button}
                onClick={this._uploadContacts}
              >
                Upload Contacts
              </Button>
            </div>
          </form>
        </DialogContent>
      </Dialog>
      // <Layer closer={true} onClose={this.props.onClose} >
      //   <Box pad='medium'>
      //     <Form onSubmit={this._uploadContacts}>
      //       <Header>
      //         <Box pad='small'><Heading>Upload Contacts</Heading></Box>
      //       </Header>
      //       {this.state.notification ? this.state.notification : null}
      //       <FormFields>
      //           <Paragraph>
      //             Here you can upload your existing contact lists. The spreadsheet used for upload must include the following fields: FirstName, LastName and EmailAddress.
      //           </Paragraph>
      //           <Box align='center' justify='center'>
      //             {this.state.isUploading ?
      //             <CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} /> : ''}
      //           </Box>  
      //         <FormField label='Contact List Source' htmlFor='source'>
      //           <Select placeHolder='Source' name='source' onChange={this._handleChange} value={this.state.source}
      //             options={[{value: 'e', label: 'Other'},{value: 'l', label: 'LinkedIn'},{value: 's', label: 'Salesforce'},{value: 'o', label: 'Outlook'}]} />
      //         </FormField>
      //         <FormField label="Upload Contact List" htmlFor="list">
      //           <input ref='file' id='file' name='file' type='file' onChange={this._handleFileChange} />
      //         </FormField>
      //       </FormFields>
      //       <Footer>
      //         <Box pad='medium'>
      //           <Button label='Upload Contacts'
      //             type='submit'
      //             primary={true} />
      //         </Box>
      //       </Footer>
      //     </Form>
      //   </Box>
      // </Layer>
    );
  }
}

const select = state => ({
  session: state.session
});

export default withStyles(styles)(connect(select)(UploadContactsLayer));
