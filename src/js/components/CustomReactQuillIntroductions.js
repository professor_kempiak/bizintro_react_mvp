import React, { Component } from 'react';
import ReactQuill from 'react-quill';

class CustomReactQuillIntroductions extends Component {
  constructor(props) {
    super(props);

    let _self = this;

    let template_list = this.props.templates.map((template) => template.name);
    if (template_list.length === 0) { template_list = ['No Templates Found'] }

    this.state = {
      modules: {
        toolbar: {
          container:
            [
              [{ 'templates': template_list }],
              [{ 'header': 1 }, { 'header': 2 }],
              ['bold', 'italic', 'underline', 'strike'],
              ['blockquote'],
              [{ 'indent': '-1' }, { 'indent': '+1' }], 
              ['bio'],
              [{ 'fields': ['Primary - First Name', 'Primary - Last Name', 'Secondary - First Name', 'Secondary - Last Name'] }],
              ['save-template']
            ],
          handlers: {
            "templates": function (value) {
              if (value !== 'No Templates Found') {
                let quill = this.quill;
                _self.props.addTemplate(value, quill);
              } 
            },
            "bio": function (value) {
              let quill = this.quill;
              _self.props.addBio(quill);
            },
            "save-template": function (value) {
              let quill = this.quill;
              let body = quill.getText();
              _self.props.saveTemplate(body, quill);
            },
            "fields": function (value) {
              let quill = this.quill;
              let cursorPosition = quill.getSelection().index;
              switch (value) {
                case 'Primary - First Name':
                  quill.insertText(cursorPosition, '{primary_contact_first_name}');
                  break;
                case 'Primary - Last Name':
                  quill.insertText(cursorPosition, '{primary_contact_last_name}');
                  break;
                case 'Secondary - First Name':
                  quill.insertText(cursorPosition, '{secondary_contact_first_name}');
                  break;
                case 'Secondary - Last Name':
                  quill.insertText(cursorPosition, '{secondary_contact_last_name}');
                  break;
              }
            }
          }
        }
      }
    }

    this._renderBioButton = this._renderBioButton.bind(this);
    this._renderFieldsButton = this._renderFieldsButton.bind(this);
    this._renderTemplateButton = this._renderTemplateButton.bind(this);
    this._renderSaveTemplateButton = this._renderSaveTemplateButton.bind(this);
  }

  _renderTemplateButton() {
    const templatePickerItems = Array.prototype.slice.call(document.querySelectorAll('.ql-templates .ql-picker-item'));
    templatePickerItems.forEach(item => item.textContent = item.dataset.value);
    document.querySelector('.ql-templates .ql-picker-label').innerHTML = 'Template&nbsp;&nbsp;&nbsp;&nbsp;' + document.querySelector('.ql-templates .ql-picker-label').innerHTML;
  }

  _renderBioButton() {
    document.querySelector('.ql-bio').innerHTML = '+Bio';
  }

  _renderSaveTemplateButton() {
    document.querySelector('.ql-save-template').innerHTML = '+Save Template';
  }

  _renderFieldsButton() {
    const templatePickerItems = Array.prototype.slice.call(document.querySelectorAll('.ql-fields .ql-picker-item'));
    templatePickerItems.forEach((item) => {
      item.textContent = item.dataset.value
      item.classList.remove('ql-selected');
    });
    document.querySelector('.ql-fields .ql-picker-label').innerHTML = '+Fields&nbsp;&nbsp;&nbsp;&nbsp;' + document.querySelector('.ql-fields .ql-picker-label').innerHTML;
  }

  componentDidMount() {
    this._renderTemplateButton();
    this._renderBioButton();
    this._renderSaveTemplateButton();
    this._renderFieldsButton();
  }

  render() {
    return (
      <div>
        <ReactQuill
          theme="snow"
          modules={this.props.modules ? this.props.modules : this.state.modules}
          onChange={this.props.onChange}
          value={this.props.value} />
      </div>
    );
  }
}



export default CustomReactQuillIntroductions;
