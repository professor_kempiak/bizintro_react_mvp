import React, { Component } from 'react';

class CustomReactQuillToolbarBasic extends Component {
  render() {
    return (
        <div id="ql-custom-toolbar">
          <select className="ql-header">
            <option value="1"></option>
            <option value="2"></option>
            <option value="3"></option>
            <option value="4"></option>
          </select>
          <button className="ql-underline"></button>
          <button className="ql-bold"></button>
          <button className="ql-italic"></button>
          <button className="ql-strike"></button>
          <button className="ql-blockquote"></button>
          <button className="ql-list" value="ordered"></button>
          <button className="ql-list" value="bullet"></button>
          <button className="ql-indent" value="-1"></button>
          <button className="ql-indent" value="+1"></button>
          <button className="ql-link"></button>
        </div>
    );
  }
}

export default CustomReactQuillToolbarBasic;
