import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { addToast } from '../actions/toasts';
import ReactMapboxGl, { Feature, GeoJSONLayer, Marker, Cluster } from "react-mapbox-gl";

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

const variantIcon = {
  error: ErrorIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);


const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiamlhbmcyMDIwIiwiYSI6ImNqZWdzNHRrMDF5NHIyeG1rMzVkbWIwZ3AifQ.57jhLiNkcAccrUij-EMt-Q"
});
const InitialUserPostion = [-87.5625333, 41.726845];

class DetailAppointmentLayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: null,
      mapCenter: InitialUserPostion,
    }
  }

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <form onSubmit={this._submitEditContactForm} style={{ paddingTop: 50 }}>
          {
            this.state.notification ?
              <div>
                <Snackbar
                  open={true}
                  // onClose={this.handleClose}
                  className={classes.snackbar}
                  autoHideDuration={2000}
                >
                  <MySnackbarContentWrapper
                    // onClose={this.handleClose}
                    variant="error"
                    message={this.state.notification}
                  />
                </Snackbar>
              </div> :
              null
          }
          <div className="appt_title">
            {/* <TextField
              className={classes.margin}
              label="Title"
              id="title"
              name="title"
              placeHolder='Meeting About VC Funding'
              value={this.props.appointment.title}
              onChange={this._handleInputChange}
            /> */}
            <Typography gutterBottom variant="headline" component="h2">
              <span style={{ color: 'white', fontWeight: '600' }}>{this.props.appointment.title}</span>
            </Typography>
          </div>
          <div className="attendees">
            <TextField
              className={classes.margin}
              label="Attendees"
              id="attendees"
              name="attendees"
              placeHolder="John Smith & Bob Arnold"
              value={this.props.appointment.attendees}
              disabled={true}
            />
          </div>
          <div className="date_time">
            <TextField
              className={classes.margin}
              label="Date & Time"
              id="date_time"
              name="date_time"
              placeHolder="March 12, 2018 3:00 - 5:00 PM"
              value={this.props.appointment.datetime}
              disabled={true}
            />
          </div>
          <div className="notes">
            <TextField
              className={classes.margin}
              label="notes"
              id="notes"
              name="notes"
              placeHolder="123 Steel Street"
              value={this.props.appointment.notes}
              disabled={true}
            />
          </div>
          <div className="location">
            <TextField
              className={classes.margin}
              label="Location"
              id="location"
              name="location"
              placeHolder="123 Steel Street"
              placeHolder="290 S Cass Ave. Westmont, IL 60559"
              disabled={true}
            />
            <Map
               style="mapbox://styles/mapbox/streets-v8"
               center={[this.props.appointment.long, this.props.appointment.lat]}
               zoom={[14]}
               containerStyle={{
                   height: "200",
                   width: "475",
               }}>
           </Map>
          </div>
        </form>
      </Dialog>
    );
  }
}

export default withStyles(styles)(DetailAppointmentLayer);
