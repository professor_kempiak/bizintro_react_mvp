import React, { Component } from 'react';
import ReactQuill from 'react-quill';

class CustomReactQuillTemplates extends Component {
  constructor(props) {
    super(props);

    let _self = this;

    this.state = {
      modules: {
        toolbar: {
          container:
            [
              [{ header: [1, 2, 3, 4] }],
              ['bold', 'italic', 'underline', 'strike'],
              ['blockquote'],
              [{ 'indent': '-1' }, { 'indent': '+1' }],
              [{ 'fields': ['Primary - First Name', 'Primary - Last Name', 'Secondary - First Name', 'Secondary - Last Name'] }]
            ],
          handlers: {
            "fields": function (value) {
              let quill = this.quill;
              let cursorPosition = quill.getSelection().index;
              switch (value) {
                case 'Primary - First Name':
                  quill.insertText(cursorPosition, '{primary_contact_first_name}');
                  break;
                case 'Primary - Last Name':
                  quill.insertText(cursorPosition, '{primary_contact_last_name}');
                  break;
                case 'Secondary - First Name':
                  quill.insertText(cursorPosition, '{secondary_contact_first_name}');
                  break;
                case 'Secondary - Last Name':
                  quill.insertText(cursorPosition, '{secondary_contact_last_name}');
                  break;
              }
            }
          }
        }
      }
    }

    this._renderFieldsButton = this._renderFieldsButton.bind(this);
  }

  _renderFieldsButton() {
    const templatePickerItems = Array.prototype.slice.call(document.querySelectorAll('.ql-fields .ql-picker-item'));
    templatePickerItems.forEach((item) => {
      item.textContent = item.dataset.value
      item.classList.remove('ql-selected');
    });
    document.querySelector('.ql-fields .ql-picker-label').innerHTML = '+Fields&nbsp;&nbsp;&nbsp;&nbsp;' + document.querySelector('.ql-fields .ql-picker-label').innerHTML;
  }

  componentDidMount() {
    this._renderFieldsButton();
  }

  render() {
    return (
      <div>
        <ReactQuill
          theme="snow"
          modules={this.props.modules ? this.props.modules : this.state.modules}
          onChange={this.props.onChange}
          value={this.props.value} />
      </div>
    );
  }
}



export default CustomReactQuillTemplates;
