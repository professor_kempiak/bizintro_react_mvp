import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import CustomReactQuillToolbarBasic from './CustomReactQuillToolbarBasic';

const modules = {
  toolbar: '#ql-custom-toolbar'
};

const formats = [
  'header',
  'underline', 'bold', 'italic', 'strike', 'blockquote',
  'list', 'bullet', 'indent',
  'link', 'image'
];

const defaultToolbar = <CustomReactQuillToolbarBasic />;

class CustomReactQuill extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        { this.props.customToolbar ? this.props.customToolbar : defaultToolbar }
        <ReactQuill
          theme="snow"
          modules={this.props.modules ? this.props.modules : modules}
          formats={this.props.formats ? this.props.formats : formats}
          onChange={this.props.onChange}
          value={this.props.value} />
      </div>
    );
  }
}

export default CustomReactQuill;
