import React, { Component, PropTypes } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Contacts from '../screens/Contacts';
import GoogleAuthCallback from '../components/GoogleAuthCallback';
import NotFound from '../screens/NotFound';
import AppointmentCalendar from '../screens/AppointmentCalendar';
import ManageIntroductions from '../screens/ManageIntroductions';
import AcceptTimeslotCallback from '../components/AcceptTimeslotCallback';
import AppointmentRequestCallback from '../components/AppointmentRequestCallback';
import AppointmentReRequestCallback from '../components/AppointmentReRequestCallback';
import InviteContactCallback from '../components/InviteContactCallback';
import Auth0Callback from '../components/Auth0Callback';
import { connect } from 'react-redux';
import Login from '../screens/Login';
import { clearToasts } from '../actions/toasts';
import { Toast } from 'grommet';
import SignUp from '../screens/SignUp';
import ManageTemplates from '../screens/ManageTemplates';
import ManageProfile from '../screens/ManageProfile';
import ManageContact from '../screens/ManageContact';
import AppointmentMap from '../screens/AppointmentMap';
import EditContactDetailsCallback from './EditContactDetailsCallback';
import Dashboard1 from '../screens/Dashboard1';
import Dashboard from '../screens/Dashboard';
import Schedule from '../screens/Schedule';
import Legal from '../screens/Legal';
import Rank from '../screens/Rank';
import LandingPage from '../screens/LandingPage';
import LandingPage2 from '../screens/LandingPage2';
import ContactDetails from '../screens/ContactDetails';

class Main extends Component {
  constructor(props) {
    super(props);

    this._clearToasts = this._clearToasts.bind(this);
  }

  _clearToasts() {
    this.props.dispatch(clearToasts());
  }

  _renderToasts() {
    let { toasts } = this.props;
    if (toasts) {
      return (
        <div>
          {toasts.map((toast, index) => {
            return <Toast key={index} status={toast.status} onClose={this._clearToasts}>{toast.message}</Toast>
          })}
        </div>
      )
    }
    return null;
  }

  render() {
    return (
      <div>
        {this._renderToasts()}
        <Router>
          <Switch>
            <Route exact={true} path='/' component={Dashboard1} />
            <Route exact={true} path='/dashboard' component={Dashboard} />
            <Route path='/schedule' component={Schedule} />
            <Route path='/login' component={Login} />
            <Route path='/signup' component={SignUp} />
            <Route path='/google_return' component={GoogleAuthCallback} />
            <Route path='/appointment' component={AppointmentCalendar} />
            <Route path='/manage' component={ManageIntroductions} />
            <Route path='/templates' component={ManageTemplates} />
            <Route path='/map' component={AppointmentMap} />
            <Route path='/profile' component={ManageProfile} />
            <Route path='/update_contact' component={ManageContact} />
            <Route path='/legal' component={Legal} />
            <Route path='/rank' component={Rank} />
            <Route path='/landingpage' component={LandingPage} />
            <Route path='/landingpage2' component={LandingPage2} />
            <Route path='/rerequest_appt/:intro_uuid/:appt_uuid' component={AppointmentReRequestCallback} />
            <Route path='/appointmentrequest/:contact_id/:intro_uuid' component={AppointmentRequestCallback} />
            <Route path='/accept_timeslot/:intro_uuid/:time_slot' component={AcceptTimeslotCallback} />
            <Route path='/contacts' component={Contacts} />
            <Route path='/contactdetails' component={ContactDetails} />
            <Route path='/contact-details/:contact_id/:token' component={EditContactDetailsCallback} />
            <Route path='/invite_profile/:contact_id/:token' component={InviteContactCallback} />
            <Route path='/callback' component={Auth0Callback} />
            <Route path='/*' component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

const select = state => ({
  toasts: state.toasts.list
});

export default connect(select)(Main);
