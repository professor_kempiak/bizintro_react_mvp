import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { CompactPicker } from 'react-color';

import CustomMapTimeZone from "./CustomMapTimeZone";

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const styles = (theme) => ({

})

class CustomMapTimeBuffer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notification: null,
            formValue : 15,
            color: this.props.polygonColor ? this.props.polygonColor : '#73d8ff',
        }

        this.changeValue = this.changeValue.bind(this);
        this.colorChange = this.colorChange.bind(this);
    }

    changeValue(event){
        let value = event.target.value;
        if(value > 0)
        this.setState({
            formValue: value
        })
    }

    colorChange(event){
        this.props.setColor(event);

        this.setState({
            color: event.hex,
        });
    }



    render() {
        return (
            <div className={'formTiles'}>
                <div className={'formsubTile'}>
                    <div>
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>What is the your time buffer for {this.props.zone_name}?</span>
                        </Typography>
                        <form className={'bufferForm'}>
                            <div>
                                <TextField
                                    className={classes.margin}
                                    label=""
                                    id="newZoneTimeBuffer"
                                    name="newZoneTimeBuffer"
                                    type="number"
                                    value={this.props.value}
                                    onChange={this.props.handleInputChange}
                                />
                            </div>
                        </form>
                        <br />
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>What color would you like this zone to be?</span>
                        </Typography>
                        <CompactPicker color={this.state.color} onChange={(e)=>this.colorChange(e)} />
                    </div>
                    <div className={'formBottomBtns'}>
                        <IconButton onClick={this.props.preStep}><NavigateBefore /></IconButton>
                        <IconButton onClick={this.props.nextStep}><NavigateNext /></IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(CustomMapTimeBuffer);
