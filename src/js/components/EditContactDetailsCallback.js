import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { addToast } from '../actions/toasts';
import { getIntroductionToken } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { startAppointmentRequest, postAppointmentReRequest } from '../api/appointments';
import EditContactLayer from './EditContactLayer';
import { getContact } from '../actions/contacts';

class EditContactDetailsCallback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contact_id: this.props.match.params.contact_id,
      token: this.props.match.params.token,
      layer: null
    };

  }

  _redirectUser() {
    if (localStorage.getItem('token') && localStorage.getItem('email')) {
      this.props.history.push('/')
    } else {
      this.props.history.push('/signup');
    }
  }

  componentWillMount() {
    const { dispatch, history } = this.props;
    dispatch(getContact(this.state.contact_id, this.state.token, (payload) => {
      this.setState({ layer: <EditContactLayer contact={payload} token={this.state.token} onClose={this._redirectUser.bind(this)} /> });
    }));
  }

  render() {
    return (
      <div>
        {this.state.layer ? this.state.layer : null}
        <LoadingSpinner />
      </div>
    );
  }
}

export default connect()(EditContactDetailsCallback);
