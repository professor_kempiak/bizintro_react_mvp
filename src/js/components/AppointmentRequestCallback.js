import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { addToast } from '../actions/toasts';
import { getIntroductionToken } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { startAppointmentRequest } from '../api/appointments';

// https://beta.bizintro.com/appointmentrequest/7307/f784f95f-94e7-47e8-845f-5c25cc9b5cee

class AppointmentRequestCallback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      intro_uuid: this.props.match.params.intro_uuid,
      contact_id: this.props.match.params.contact_id
    };

    sessionStorage.contact_id = this.state.contact_id;
    sessionStorage.intro_uuid = this.state.intro_uuid;
  }

  componentWillMount() {
    const { dispatch } = this.props;
    const { router } = this.context;
    const intro_uuid = this.state.intro_uuid;
    const contact_id = this.state.contact_id;

    dispatch(getIntroductionToken(intro_uuid, "introduction", (payload) => {
      localStorage.setItem('newUser', payload.new);
      localStorage.username = payload.first_name;
      if (payload.token) {
        startAppointmentRequest(intro_uuid, contact_id, payload.token).then((payload) => {
          if (payload.status && payload.status === 'complete') {
            dispatch(getUserIntroductionList((payload) => {
              if (payload.length > 0) {
                payload.map((val) => {
                  if (val.fields.uuid === sessionStorage.intro_uuid) {
                    sessionStorage.recipient_email = val.fields.recipient_b_email
                    sessionStorage.contact_a = JSON.stringify(val.fields.contact_a)
                    sessionStorage.contact_b = JSON.stringify(val.fields.contact_b)
                    localStorage.email = val.fields.recipient_a_email
                    window.location.href = ('/appointment')
                  }
                })
              }
            }))
          } else {
            dispatch(addToast(payload.message, 'critical'));
            this.props.history.push('/login');
          }
        });
      }
    }));
  }

  render() {
    return (
      <LoadingSpinner />
    );
  }
}

const select = state => ({

});

export default connect(select)(AppointmentRequestCallback);
