import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTooltip from 'react-tooltip'
import moment from 'moment';

import InfiniteScroll from 'react-infinite-scroller';

import { addToast } from '../actions/toasts';
import { getUserIntroductionStatus, postIntroductionNudge, postIntroductionArchive, getIntroductions } from '../actions/introductions';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreHori from '@material-ui/icons/MoreHoriz';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class IntroductionList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showLayer: false,
      notifications: null,
      sortAscending: false,
      sortIndex: 1,
      sortIndexLabels: [],
      page: 1,
      perpage: 10,
      hasMoreItems: true,
      showLoading: false,
      isMobile: false,
      anchorEl: null
    }

    this.localUser = localStorage.getItem('name');

    this._onMore = this._onMore.bind(this);
    this._updateSort = this._updateSort.bind(this);
    this._sortListItems = this._sortListItems.bind(this);
    this._sortByIntroSent = this._sortByIntroSent.bind(this);
    this._sortByPrimaryName = this._sortByPrimaryName.bind(this);
    this._sortBySecondaryName = this._sortBySecondaryName.bind(this);
    this._sortByAppointmentSet = this._sortByAppointmentSet.bind(this);
    this.introClick = this.introClick.bind(this);
    this.nudgeClick = this.nudgeClick.bind(this);
    this.appointmenSetClick = this.appointmenSetClick.bind(this);
  }

  componentWillMount() {
    let { dispatch } = this.props;
    if (this.props.introductions.length == 0) {
      dispatch(getIntroductions(localStorage.getItem('token'), this.state.page, this.state.perpage, (payload) => {
        console.log('getting introductions');
      }))
    }
  }

  componentDidMount() {
    this.checkMobile()
  }

  componentWillReceivProps() {
    this.checkMobile()
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  checkMobile() {
    let isMobile = window.innerWidth > 425 ? false : true;
    this.setState({isMobile});
  }

  _sendIntroductionNudge(uuid) {
    let { dispatch } = this.props;
    let token = localStorage.getItem('token');
    dispatch(postIntroductionNudge(uuid, token, (payload) => {
      if (payload.status == 'success') {
        dispatch(getUserIntroductionStatus(localStorage.getItem('token'), (payload) => {
          dispatch(addToast('Nudge has been successfully dispatched!', 'ok'));
          setTimeout(() => {
            ReactTooltip.rebuild()            
          }, 500)
        }));
      } else {
        dispatch(addToast('There was an error sending a nudge', 'critical'));
      }
    }));
  }

  _updateSort(index, ascending) {
    if (index === this.state.sortIndex) {
      this.setState({ sortAscending: !this.state.sortAscending });
      return true;
    }
    this.setState({ sortIndex: index });
  }

  _sortListItems(a, b) {
    switch (this.state.sortIndex) {
      case 0:
        return this._sortByPrimaryName(a, b);
        break;
      case 1:
        return this._sortByIntroSent(a, b);
        break;
      case 2:
        return this._sortByNudgeSent(a, b);
        break;
      case 3:
        return this._sortByAppointmentSet(a, b);
        break;
      case 4:
        return this._sortBySecondaryName(a, b);
        break;
      default:
        break;
    }
  }

  _sortByPrimaryName(a, b) {
    let name_a = a.contact_a.first_name;
    let name_b = b.contact_a.first_name;

    let comparison = 0;
    if (name_a > name_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }

  _sortBySecondaryName(a, b) {
    let name_a = a.contact_b.first_name;
    let name_b = b.contact_b.first_name;

    let comparison = 0;
    if (name_a > name_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }

  _sortByIntroSent(a, b) {
    let intro_a = a.date_made;
    let intro_b = b.date_made;

    let comparison = 0;
    if (intro_a > intro_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }

  _sortByNudgeSent(a, b) {
    let nudge_a = a.date_last_nudged ? a.date_last_nudged : '';
    let nudge_b = b.date_last_nudged ? b.date_last_nudged : '';

    let comparison = 0;
    if (nudge_a > nudge_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }

  _sortByAppointmentSet(a, b) {
    let appt_a = a.appointment_details ? a.appointment_details.date_made : '';
    let appt_b = b.appointment_details ? b.appointment_details.date_made : '';

    let comparison = 0;
    if (appt_a > appt_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }



  _onMore() {
    this.setState({ showLoading: true, hasMoreItems: false });
    let page = this.state.page + 1;
    this.props.dispatch(getIntroductions(localStorage.getItem('token'), page, this.state.perpage, (response) => {
      this.setState({ showLoading: false, hasMoreItems: true });
      if (response.length < this.state.perpage) {
        this.setState({ hasMoreItems: false });
        return;
      }
      if (this.state.page > 50) {
        this.setState({ hasMoreItems: false });
      } else {
        this.setState({ page: page });
      }
    }));
  }


  introClick() {
    // alert('intro clicked');
  }
  nudgeClick() {
    // alert('nudge clicked');
  }
  appointmenSetClick() {
    // alert('appointment set click');
  }

  getApptTip(introduction) {
    return 'Appointment \r\n' + moment(introduction.appointment_details.date_made).format('MMMM Do, YYYY') + '\r\n' + introduction.location;
  }

  getToolTipByIntroduction(introduction) {
    if (introduction.appointment_details) {
      if (introduction.appointment_details.date_accepted) {
        let locationStr = ''
        if (introduction.location == undefined) {
          locationStr = 'Appointment scheduled';
        } else {
          if (introduction.location.address === '') {
            locationStr = 'Appointment scheduled';
          } else {
            locationStr = 'Appointment scheduled at ' + introduction.location.address;            
          }
        }
        return (
            <ReactTooltip id={'appt' + introduction.id} aria-haspopup='true' role='example'>
               <p className='text-white'>{introduction.appointment_details.title || 'Appointment'}<br/>
               Accepted on {moment(introduction.appointment_details.date_made).format('MMMM Do, YYYY')}<br/> 
               {locationStr} on {moment(introduction.start_date).format('MMMM Do, YYYY')} at {moment(introduction.start_date).format('hh:mm A')}</p>
            </ReactTooltip>
          )
      }
    }

    return ''
  }

  _onArchive(id) {
    let { dispatch } = this.props;
    let token = localStorage.getItem('token');
    dispatch(postIntroductionArchive(id, token, (payload) => {
      if (payload.status == 'success') {
        dispatch(getUserIntroductionStatus(localStorage.getItem('token'), (payload) => {
          dispatch(addToast('Introduction archived successfully!', 'ok'));
          this.setState({page: 1, hasMoreItems: true});
        }));
      } else {
        dispatch(addToast('There was an error archiving introduction', 'critical'));
      }
    }));
  }

  getApptDotStatus(introduction) {
    if (introduction.appointment_details) {
      if (introduction.appointment_details.date_accepted) {
        return true;
      }
    }
    return false;
  }

  render() {
    const { anchorEl } = this.state;
    const { classes } = this.props;
    const loader = <TableRow key='loader'><td colSpan='6'><div className="loader">Loading ...</div></td></TableRow>;
    let { introductions } = this.props;
    if (!introductions) { introductions = [] }

    let v_introductions = this.props.archived ? introductions.filter((e) => e.status == "f") : introductions.filter((e) => e.status != "f")

    let introductionsList = v_introductions.length === 0 ?
      <TableRow>
        <td colSpan='6'>You have made no introductions yet! </td>
      </TableRow>
      :
      v_introductions.sort(this._sortListItems).map((introduction) => {
        return (
          <TableRow key={introduction.id}>
            <td>
              <div>
                <div>
                  <div className='introductionAvatar introductionAvatarInitials'>{introduction.contact_a.first_name.slice(0, 1).toUpperCase() + introduction.contact_a.last_name.slice(0, 1).toUpperCase()}</div>
                </div>
                <div>
                  <span className='introductionContactName'>{introduction.contact_a.first_name + ' ' + introduction.contact_a.last_name}</span>
                  {introduction.contact_a.occupation}
                </div>
              </div>
            </td>
            <td colSpan='3'>
              <ul className={"progress-tracker progress-tracker--text anim-ripple progress-tracker-no-margin"}>
                <li className={introduction.date_made ? "progress-step is-complete" : "progress-step"} onClick={this.introClick} >
                  <span className={"progress-marker"} data-tip={introduction.date_made ? moment(introduction.date_made).format('MMMM Do, YYYY') : null}></span>
                </li>

                <li className={introduction.date_last_nudged ? "progress-step is-complete" : "progress-step"} onClick={this.nudgeClick} >
                  <span className={"progress-marker"} data-tip={introduction.date_last_nudged ? moment(introduction.date_last_nudged).format('MMMM Do, YYYY') : null}></span>
                </li>

                <li className={this.getApptDotStatus(introduction) ? "progress-step is-complete" : "progress-step"} onClick={this.appointmenSetClick} >
                  <span className={"progress-marker"} data-for={'appt' + introduction.id} data-tip></span>
                  {this.getToolTipByIntroduction(introduction)}
                </li>

              </ul>
              <ReactTooltip />
            </td>
            <td>
              <div>
                <div>
                    {/* <Image className='introductionAvatar' src={introduction.contact_a.avatar_link} /> */}
                    <div className='introductionAvatar introductionAvatarInitials'>{introduction.contact_b.first_name.slice(0, 1).toUpperCase() + introduction.contact_b.last_name.slice(0, 1).toUpperCase()}</div>
                </div>
                <div>
                  <span className='introductionContactName'>{introduction.contact_b.first_name + ' ' + introduction.contact_b.last_name}</span>
                  {introduction.contact_b.occupation}
                </div>
              </div>
            </td>
            <td>
              <IconButton
                aria-label="More"
                aria-owns={anchorEl ? 'long-menu' : null}
                aria-haspopup="true"
                onClick={this.handleClick}
              >
                <MoreHori />
              </IconButton>
              { this.state.isMobile ? 
                <Menu 
                  className='responsive-menu-button'
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                >
                  <MenuItem><a href='#' onClick={() => this._sendIntroductionNudge(introduction.uuid)}>Nudge</a></MenuItem>
                  <MenuItem><a href={'mailto:' + introduction.contact_a.email + '?subject=Introduction Follow Up&body=Dear ' + introduction.contact_a.first_name + ',%0D%0A%0D%0AI see that you had a chance to meet with ' + introduction.contact_b.first_name + '. How did it go?%0D%0A%0D%0A- ' + this.localUser}>Email {introduction.contact_a.first_name}</a></MenuItem>
                  <MenuItem><a href={'mailto:' + introduction.contact_b.email + '?subject=Introduction Follow Up&body=Dear ' + introduction.contact_b.first_name + ',%0D%0A%0D%0AI see that you had a chance to meet with ' + introduction.contact_a.first_name + '. How did it go?%0D%0A%0D%0A- ' + this.localUser}>Email {introduction.contact_b.first_name}</a></MenuItem>
                  <MenuItem><a href='#' onClick={() => this._onArchive(introduction.id)}>Archive</a></MenuItem>
                </Menu>
              :              
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={this.handleClose}
                >
                  <MenuItem><a href='#' onClick={() => this._sendIntroductionNudge(introduction.uuid)}>Nudge</a></MenuItem>
                  <MenuItem><a href={'mailto:' + introduction.contact_a.email + '?subject=Introduction Follow Up&body=Dear ' + introduction.contact_a.first_name + ',%0D%0A%0D%0AI see that you had a chance to meet with ' + introduction.contact_b.first_name + '. How did it go?%0D%0A%0D%0A- ' + this.localUser}>Email {introduction.contact_a.first_name}</a></MenuItem>
                  <MenuItem><a href={'mailto:' + introduction.contact_b.email + '?subject=Introduction Follow Up&body=Dear ' + introduction.contact_b.first_name + ',%0D%0A%0D%0AI see that you had a chance to meet with ' + introduction.contact_a.first_name + '. How did it go?%0D%0A%0D%0A- ' + this.localUser}>Email {introduction.contact_b.first_name}</a></MenuItem>
                  <MenuItem><a href='#' onClick={() => this._onArchive(introduction.id)}>Archive</a></MenuItem>
                </Menu>
              }
            </td>
          </TableRow>
        );
      });
    return (
      <div className='dashboardTabContent'>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <CustomTableCell>Primary Contact</CustomTableCell>
                <CustomTableCell>Intro Sent</CustomTableCell>
                <CustomTableCell>Nudge Sent</CustomTableCell>
                <CustomTableCell>Appointment Set</CustomTableCell>
                <CustomTableCell>Secondary Contact</CustomTableCell>
                <CustomTableCell>Actions</CustomTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {/* {data.map(n => {
                return (
                  <TableRow className={classes.row} key={n.id}>
                    <CustomTableCell component="th" scope="row">
                      {n.name}
                    </CustomTableCell>
                    <CustomTableCell numeric>{n.calories}</CustomTableCell>
                    <CustomTableCell numeric>{n.fat}</CustomTableCell>
                    <CustomTableCell numeric>{n.carbs}</CustomTableCell>
                    <CustomTableCell numeric>{n.protein}</CustomTableCell>
                  </TableRow>
                );
              })} */}
              {introductionsList}
            </TableBody>
          </Table>
        </Paper>
        {/* <Table>
          <TableHeader labels={['Primary Contact', 'Intro Sent', 'Nudge Sent', 'Appointment Set', 'Secondary Contact', 'Actions']}
            sortIndex={this.state.sortIndex}
            sortAscending={!this.state.sortAscending}
            onSort={this._updateSort} />
            <tbody>
              {introductionsList}
            </tbody>
        </Table> */}
        {this.state.showLoading ? <div align='center'><CircularProgress className={classes.progress} size={50} /></div> : null}
        {v_introductions.length > 0 && 
          this.state.hasMoreItems ? 
          <div align='center'>
            <Button variant="contained" color="primary" size="large" className={classes.button}>
              Load More
            </Button>
          </div> : 
          null
        }
      </div>
    );
  }
}

const select = state => ({
  introductions: state.introductions.list,
  end_of_results: state.introductions.end_of_results
});


export default withStyles(styles)(connect(select)(IntroductionList));
