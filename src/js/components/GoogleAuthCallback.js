import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loginWithGoogle } from '../actions/session'
import LoadingSpinner from './LoadingSpinner';

class GoogleAuthCallback extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let code = window.location.search.replace('?', '').split('&')[0].split('=')[1];
    this.props.dispatch(loginWithGoogle(code, (response) => {
      if (response.status) {
        window.location = '/';
      } else {
        window.location = '/login';
      }
    }));
  }

  render() {
    return (
      <LoadingSpinner />
    );
  }

}

const select = state => ({
  session: state.session
});

export default connect(select)(GoogleAuthCallback);
