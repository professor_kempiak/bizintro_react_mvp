import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Value from 'grommet/components/Value';

import { addToast } from '../actions/toasts';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class ScheduleOtherAppointment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: null,
      startDate :'2018-06-20',
      endDate : '2018-06-22'
    }
      this._applyApp = this._applyApp.bind(this);
  }
  _applyApp(){
    this.props.updateDay(this.state.startDate, this.state.endDate);
  }

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <DialogTitle id="form-dialog-title">Choose your date range</DialogTitle>
        <DialogContent>
          <TextField
            id="date"
            label="Birthday"
            type="date"
            value={this.state.startDate}
            onChange={(startTime) => this.setState({ startDate: startTime.target.value })}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="date"
            label="Birthday"
            type="date"
            value={this.state.endDate}
            onChange={(endTime) => this.setState({ endDate: endTime.target.value })}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <div direction='row' pad='small' responsive={false} alignSelf='start' justify={'between'} className='calendar-buttons-left-container'>
            <Button variant="contained" onClick={this._applyApp}>Apply</Button>
            <Button variant="contained" onClick={this.props.onClose}>Cancel</Button>
         </div>
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(styles)(ScheduleOtherAppointment);
