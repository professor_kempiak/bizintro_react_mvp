import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addToast } from '../actions/toasts';
import { clearSearch, updateSearchClear } from '../actions/search';
import { gatherContacts, remoteContactSearch } from '../actions/contacts';
import { loadTemplates } from '../actions/templates';
import { selectContact, deselectContact } from '../actions/contacts';

import { headers, parseJSON } from '../api/utils';
import InfiniteScroll from 'react-infinite-scroller';
import ContactCard from './ContactCard';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import Button from '@material-ui/core/Button';
import AccountCircle from '@material-ui/icons/AccountCircle';


import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
    },
    progressPosition: {
        textAlign: 'center'
    }
});

class ContactList extends Component {
  constructor(props) {
      super(props);

      this.state = {
          page: 1,
          perpage: 25,
          hasMoreItems: false,
          pageLoaded: false,
          end_of_pages: false,
          showLoading: false,
          gridStatus: 'grid'
      }
      this._onMore = this._onMore.bind(this);
      this._isSelected = this._isSelected.bind(this);
      this._clearSearchBar = this._clearSearchBar.bind(this);
      this._gatherContacts = this._gatherContacts.bind(this);
      this._gatherTemplates = this._gatherTemplates.bind(this);
      this._toggleSelection = this._toggleSelection.bind(this);
      this.gatherMoreContacts = this.gatherMoreContacts.bind(this);
      this.changeGrid = this.changeGrid.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if ((newProps.search !== this.props.search) || 
        (newProps.search_term !== this.props.search_term)) {
      this.setState({page: 1});
      this._gatherContacts(1, newProps.search, newProps.search_term);
    }
  }

  _gatherContacts(page, search=[], search_term='') {
    const { dispatch, session } = this.props,
          searchTagsAndTerm = [...search, search_term];
    dispatch(remoteContactSearch(
        searchTagsAndTerm || [], 
        1, 
        localStorage.token, 
        (response) => {
          if (!Array.isArray(response)) {
            dispatch(addToast('Error loading existing contacts', 'critical'));
          }
          this.setState({ 
            hasMoreItems: response.length < this.state.perpage ? false : true , 
            pageLoaded: true 
          });
        }
    ));
  }

  gatherMoreContacts() { 
    const { dispatch, search, search_term } = this.props;
    if (this.state.hasMoreItems && this.state.pageLoaded) {
      let page = this.state.page + 1,
          searchTagsAndTerm = [...search, search_term];

      this.setState({hasMoreItems: false});
      dispatch(remoteContactSearch(
        searchTagsAndTerm || [], 
        page, 
        localStorage.token , 
        (response) => {
          this.setState({ showLoading: false });
          if (response.length === 0 || 
              response.length < this.state.perpage) { 
            this.setState({ hasMoreItems: false });
          } else {
            this.setState({ page: page, hasMoreItems: true });
          }
        }
      ));
    }
  }

  _gatherTemplates() {
    this.props.dispatch(loadTemplates(localStorage.getItem('token')));
  }


  _isPrimarySelection(contact) {
      let { selected } = this.props;
      if (selected.primary.includes(contact)) {
          return true;
      }
      return false;
  }

  _isSecondarySelection(contact) {
      let { selected } = this.props;
      if (selected.secondary.includes(contact)) {
          return true;
      }
      return false;
  }

  _isSelected(contact) {
      if (this._isPrimarySelection(contact) 
          || this._isSecondarySelection(contact)) {
          return true;
      }
      return false;
  }

  _onMore() {
    this.setState({ showLoading: true });
    if (this.state.hasMoreItems 
        || this.props.search.length > 0){
      this.gatherMoreContacts();
    }
  }


  _toggleSelection(contact) {
      let { dispatch, selected } = this.props;
      this._clearSearchBar();
      if (this._isPrimarySelection(contact) 
          || this._isSecondarySelection(contact)) {
          dispatch(deselectContact(contact));
        } else {
          dispatch(selectContact(contact, selected));
          if((selected.secondary.length >= 0) 
              && (selected.primary.length > 0)) {
              this.props.makeIntroducntionLayer();
          }
        }
    }
    
  _clearSearchBar() {
    this.props.dispatch(clearSearch());
    this.props.dispatch(updateSearchClear(true));
  }
  
  componentDidMount() {
    this._gatherContacts(this.state.page, this.props.search, this.props.search_term);
    this._gatherTemplates();
  }
    changeGrid(type){
      this.setState({
          gridStatus: type
      });
    }

    render() {
        const loader = <div className="loader">Loading ...</div>;
        const { classes } = this.props;
        let { contacts, search, search_term } = this.props;
        let contactStatus = 'fullOpacity contact-tile';
        if(this.state.gridStatus == 'list'){
            contactStatus = 'contact-tile listTheme';
        }
        let contactItems = contacts.map((contact) => {
            return (
                <div
                    className={contactStatus}
                    key={contact.id}
                >
                    <ContactCard   contact={contact}
                                   editContact={this.props.editContact}
                                   toggleSeclect={this._toggleSelection}
                                   gridStatus={this.state.gridStatus} />
                </div>
            )
        });
        if (contactItems.length <= 0) {
            contactItems =
                <div className={'contact-tile'} onClick={this.props.addContact}>
                        <p className="header-title">Contact Not Found?</p>
                        <div className="add-user">
                            <AccountCircle />
                            <a href='#'>
                                 Click here to Add Contact now!
                            </a>
                        </div>
                </div>;
        }
        let gridClass = '';
        if(this.state.gridStatus == 'grid'){
            gridClass = 'active'
        };
        let listClass = '';
        if(this.state.gridStatus == 'list'){
            listClass = 'active';
        };

        return (
        <div className='dashboardTabContent'>
            <Grid container spacing={16} className={'gridContainer'}>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={16}>
                        <Typography className={'ContactHeadertitle'}>
                            Contacts
                        </Typography>
                        <Typography className={'ContactHeaderSubtitle'}>
                            Last updated 20 min ago
                        </Typography>
                    </Grid>
                    <Grid item className={gridClass}>
                        <i className="material-icons" onClick={() => { this.changeGrid('grid') }}>
                            view_column
                        </i>
                    </Grid>
                    <Grid item className={listClass}>
                        <i className="material-icons" onClick={() => { this.changeGrid('list') }}>
                            reorder
                        </i>
                    </Grid>
                </Grid>
            </Grid>

            <div className='contactList'>
                    {contactItems}
            </div>
                    {this.state.showLoading || this.props.contact_search_loading ? <div className={classes.progressPosition}><CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} /></div> : null}
            {
                this.state.hasMoreItems ?
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Button variant="contained" size="large" color="primary" className={classes.button} onClick={this._onMore}>
                        Load More
                    </Button>
                </div> :
                null
            }
      </div>
        );
    }
}

const select = state => ({
    contacts: state.contacts.list,
    selected: state.contacts.selected,
    search: state.search.tags,
    search_term: state.search.term,
    contact_search_loading: state.contacts.loading
});

export default withStyles(styles)(connect(select)(ContactList));