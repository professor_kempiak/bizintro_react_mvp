import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';


import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Pause from '@material-ui/icons/Pause';
import CheckCircle from '@material-ui/icons/CheckCircle';
import { selectContact, deselectContact } from '../actions/contacts';

const styles = (theme) => ({

})

class SelectContactButton extends Component {
  constructor(props) {
    super(props);

    this._selectContact = this._selectContact.bind(this);
    this._deselectContact = this._deselectContact.bind(this);
  }

  _deselectContact(e) {
    this.props.dispatch(deselectContact(this.props.contact));
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    return true;
  }

  _selectContact(e) {
    this.props.dispatch(selectContact(this.props.contact, this.props.selected));
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    return true;
  }

  _isPrimarySelection() {
    let { selected, contact } = this.props;
    if (selected.primary.includes(contact)) {
      return true;
    }
    return false;
  }

  _isSecondarySelection() {
    let { selected, contact } = this.props;
    if (selected.secondary.includes(contact)) {
      return true;
    }
    return false;
  }

  render() {
    let button = null;
    let { selected, contact } = this.props;
    if (selected.primary.length < 1) {
      button = <MenuItem><a onClick={this._selectContact}><Pause /></a></MenuItem>;
    } else {
          button = <MenuItem>
            <a onClick={this._selectContact} primary={true} reverse={true}>
              <Pause /> Set as Secondary Contact</a></MenuItem>;
    }
    if (this._isPrimarySelection()) {
      button = <MenuItem><a onClick={this._deselectContact}><CheckCircle />Primary Contact</a></MenuItem>;
    }
    if (this._isSecondarySelection()) {
      button = <MenuItem><a onClick={this._deselectContact}><CheckCircle />Secondary Contact</a></MenuItem>;
    }
    return (button);
  }
}

const select = state => ({
  session: state.session,
  selected: state.contacts.selected
});

export default withStyles(styles)(connect(select)(SelectContactButton));
