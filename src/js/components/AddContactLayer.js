import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { addContact } from '../actions/contacts';
import { addToast } from '../actions/toasts';
import CustomReactQuill from './CustomReactQuill';
const ReactTags = require('react-tag-autocomplete')

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

const variantIcon = {
  error: ErrorIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

class AddContactLayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notification: null,
      first_name: '',
      last_name: '',
      email: '',
      mobile: '',
      address: '',
      title: '',
      company: '',
      bio: '',
      tags: [],
      suggestions: [
        { id: 3, name: "Developer"},
        { id: 4, name: "Designer"},
        { id: 5, name: "QA"},
        { id: 6, name: "Project Manager"},
        { id: 7, name: "Sales Analyst"},
        { id: 8, name: "Master Data Departmental"},
        { id: 9, name: "Finance Departmental "},
        { id: 10, name: "Cost Analyst"},
        { id: 11, name: "Finance Manager"},
        { id: 12, name: "Finance Director"},      
        { id: 13, name: "Marketing Manager"},
        { id: 14, name: "Export Order Manager"}            
      ]      
    }

    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleQuillChange = this._handleQuillChange.bind(this);
    this._submitAddContactForm = this._submitAddContactForm.bind(this);
  }
  
  handleDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }
 
  handleAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
  }

  _submitAddContactForm() {
    let { session, dispatch } = this.props;
    let tags = [];
    this.state.tags.map(tag => {
      tags.push(tag.name);
    })
    tags=tags.join(',');
    let contact = {
      id: Date.now(),
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      mobile: this.state.mobile,
      address: this.state.address,
      title: this.state.title,
      company: this.state.company,
      occupation: this.state.title + ' at ' + this.state.company,
      bio: this.state.bio,
      tags: tags,
      avatar_link: 'https://www.gravatar.com/avatar/e866318b7f8e4fe276bd620a15e34c0e?s=80&d=mm'
    }
    dispatch(addContact(contact, session.token, (response) => {
      if (response.contact_id) {
        dispatch(addToast(response.message, 'ok'));
        this.props.onClose();
      } else {
        this.setState({
          notification: response.message
        })
      }
    }));
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  _handleQuillChange(content, delta, source, editor) {
    this.setState({
      bio: content
    });
  }


  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <DialogTitle id="form-dialog-title">Add New Contact</DialogTitle>
        <DialogContent>
          {
            this.props.uploadContacts ?
              <a href="JavaScript:Void(0);" onClick={this.props.uploadContacts}>
                Do you want to upload a list of users from a csv file?
          </a> :
              null
          }
          <form onSubmit={this._submitAddContactForm}>
            {
              this.state.notification ? 
              <div>
                <Snackbar
                  open={true}
                  // onClose={this.handleClose}
                  className={classes.snackbar}
                  autoHideDuration={2000}
                >
                  <MySnackbarContentWrapper
                    // onClose={this.handleClose}
                    variant="error"
                    message={this.state.notification}
                  />
                </Snackbar>
              </div> : 
              null
            }
            <div className="for-content">
              <div className="first-name">
                <TextField
                  className={classes.margin}
                  label="First Name"
                  id="firstName"
                  name="first_name"
                  placeholder="John"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="last-name">
                <TextField
                  className={classes.margin}
                  label="Last Name"
                  id="lastName"
                  name="last_name"
                  placeholder="Doe"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="email">
                <TextField
                  className={classes.margin}
                  label="Email"
                  id="email"
                  name="email"
                  placeholder="johnDoe@gmail.com"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="mobile">
                <TextField
                  className={classes.margin}
                  label="Mobile"
                  id="mobile"
                  name="mobile"
                  placeholder="xxx-xxx-xxxx"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="address">
                <TextField
                  className={classes.margin}
                  label="Address"
                  id="address"
                  name="address"
                  placeholder="123 Steel Street"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="title">
                <TextField
                  className={classes.margin}
                  label="Title"
                  id="title"
                  name="title"
                  placeholder="Electrician"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className="company">
                <TextField
                  className={classes.margin}
                  label="Company"
                  id="company"
                  name="company"
                  placeholder="Vancouver Electric"
                  onChange={this._handleInputChange}
                />
              </div>
              <div className='no-overflow'>
                <ReactTags
                  classNames={{ root: 'react-tags tagsContainer', searchInput: 'react-tags__search-input tagsSearchInput', selectedTag: 'react-tags__selected-tag tagsSelectedTag', selectedTagName: 'react-tags__selected-tag-name tagsSelectedTagName' }}
                  tags={this.state.tags}
                  allowNew={true}
                  suggestions={this.state.suggestions}
                  handleDelete={this.handleDelete.bind(this)}
                  handleAddition={this.handleAddition.bind(this)} />
              </div>
              <div>
                <div pad='small'>
                  <CustomReactQuill
                    onChange={this._handleQuillChange}
                    value={this.state.bio} />
                </div>
              </div>
            </div>
            <div className={classes.buttonContainer}>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className={classes.button}
                onClick={this._submitAddContactForm}
              >
                Upload Contacts
              </Button>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    );
  }
}

const select = state => ({
  session: state.session
});

export default withStyles(styles)(connect(select)(AddContactLayer));