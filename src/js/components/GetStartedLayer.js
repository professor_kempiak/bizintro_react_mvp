import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Vimeo from '@u-wave/react-vimeo';

import { uploadContactList, gatherContacts } from '../actions/contacts';
import { loadTours } from '../actions/tours';

import { addToast } from '../actions/toasts';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
});
const videoContent = [
                      {id:1,vimeoId:'151367434',title:'headline 1',content:'Here you can upload your existing contact lists. The spreadsheet used for upload must include the following fields: FirstName, LastName and EmailAddress1.'},
                      {id:2,vimeoId:'151367434',title:'headline 2',content:'Here you can upload your existing contact lists. The spreadsheet used for upload must include the following fields: FirstName, LastName and EmailAddress2.'},
                      {id:3,vimeoId:'151367434',title:'headline 3',content:'Here you can upload your existing contact lists. The spreadsheet used for upload must include the following fields: FirstName, LastName and EmailAddress3.'}
                    ]

class GetStartedLayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      notification: null,
      source: '',
      file: null,
      isUploading: false,
      contentIndex: 0,
      list: []
    }

    this._handleChange = this._handleChange.bind(this);
    this._uploadContacts = this._uploadContacts.bind(this);
    this._handleFileChange = this._handleFileChange.bind(this);
    this._changeVideo = this._changeVideo.bind(this);
  }

  _uploadContacts(e) {
    let { dispatch } = this.props;

    e.preventDefault();

    let token = localStorage.getItem('token');

    const formData = new FormData(e);
    formData.append('file', this.state.file);
    formData.append('source', this.state.source.value);
    formData.append('token', token);
    this.setState({isUploading: true})
    dispatch(uploadContactList(formData, token, (payload) => {
      if (payload.success) {
        dispatch(gatherContacts(localStorage.token, 1, (response) => {
          this.setState({isUploading: false});
          dispatch(addToast(payload.message, 'ok'));
          this.props.onClose();
        }));
      } else {
        dispatch(addToast(payload.error, 'critical'));
      }
    }));
  }

  _handleFileChange(e) {
    this.setState({
      file: e.target.files[0]
    })
  }

  _handleChange(e) {
    this.setState({
      source: e.target.value
    })
  }
  
  _changeVideo(index){ console.log('333333333333')
     this.setState({
         contentIndex: index
     })
  }

  componentWillMount() {
    let { dispatch } = this.props;
    let token = localStorage.getItem('token');
    dispatch(loadTours(token));
  }

  componentWillReceiveProps(newProps) {
    if (newProps.list.length > 0 && newProps.list !== this.props.list) {
      this.setState({list: newProps.list});
    }
  }

  render() {
    const { classes, ...other } = this.props;

    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other} className={'get-stared-modal'}>
        <DialogTitle id="form-dialog-title" className={'modal-header'}>Get Started!</DialogTitle>
        <DialogContent className={'modal-content'}>
          {this.state.notification ? this.state.notification : null}
          <div className={''}>
            <div>
                <Typography gutterBottom variant="headline" component="h2">
                  Get started by uploading some contacts...
                </Typography>
                <form onSubmit={this._uploadContacts}>
                  <div className={'upload-progress'}>
                    {this.state.isUploading ? <CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} /> : ''}
                  </div>
                  <div>
                    <Select
                      value={this.state.source}
                      onChange={this._handleChange}
                      inputProps={{
                        name: 'age',
                        id: 'age-simple',
                      }}
                    >
                      <MenuItem value="o">
                        <em>Outlook</em>
                      </MenuItem>
                      <MenuItem value='s'>Salesforc</MenuItem>
                      <MenuItem value='1'>LinkedIn</MenuItem>
                      <MenuItem value="e">Other</MenuItem>
                    </Select>
                  </div>
                  <div>
                    <TextField
                      className={classes.margin}
                      id="file"
                      name="file"
                      placeholder="upload contact list"
                      type="file"
                      onChange={this._handleFileChange}
                    />
                  </div>
                  <Button variant="contained" color="primary" onClick={this._uploadContacts} className={'submit-btn'}>
                    Upload Contacts
                  </Button>
                </form>
              </div>
              <div className={'btn-group'}>
                <Button variant="outlined" color="primary" onClick={() => this._changeVideo(0)}>
                  Gmail
                </Button>
                <Button variant="outlined" color="primary" onClick={() => this._changeVideo(1)}>
                  Office 365
                </Button>
                <Button variant="outlined" color="primary" onClick={() => this._changeVideo(2)}>
                  Outlook
                </Button>
              </div>
              {
                this.state.list.length > 0 ?
                <div className={'video-section'}>
                  <Vimeo video={this.state.list[this.state.contentIndex]['vimeoId']} />
                  <Typography gutterBottom variant="headline" component="h2">
                    {this.state.list[this.state.contentIndex]['headline']}
                  </Typography>
                  <Typography component="h2">
                    {this.state.list[this.state.contentIndex]['description']}
                  </Typography>
                </div> :
                null
              }
          </div>
        </DialogContent>
      </Dialog>
    );
  }
}

const select = state => ({
  list: state.tours.list
});

export default withStyles(styles)(connect(select)(GetStartedLayer));
