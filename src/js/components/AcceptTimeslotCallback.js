import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { getIntroductionToken, acceptAppointment, getUserApptList } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { addToast } from '../actions/toasts';
import { startAppointmentRequest } from '../api/appointments';

// https://beta.bizintro.com/accept_timeslot/223d360c-7acd-48bb-8a64-1def77b9ccf4/Fri%20Feb%20%202%2019:00:00%202018

class AcceptTimeslotCallback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      intro_uuid: this.props.match.params.intro_uuid,
      time_slot: this.props.match.params.time_slot
    };

    sessionStorage.intro_uuid = this.state.intro_uuid;
    localStorage.time_slot = this.state.time_slot;
  }

  componentWillMount() {
    const { dispatch, history } = this.props;
    const { router } = this.context;
    const intro_uuid = this.state.intro_uuid;
    const time_slot = this.state.time_slot;

    dispatch(getIntroductionToken(intro_uuid, "appt_request", (payload) => {
      let token = payload.token;
      localStorage.setItem('newUser', payload.new);
      localStorage.username = payload.first_name;
      if (payload.token) {
        dispatch(acceptAppointment(intro_uuid, time_slot, payload.token, (payload) => {
          if (payload.status && payload.status != 'error') {
            dispatch(getUserApptList(token, (payload) => {
              if (payload.length > 0) {
                payload.map((val) => {
                  if (val.fields.uuid === sessionStorage.intro_uuid) {
                    localStorage.email = val.fields.recipient_email;
                    if (localStorage.getItem('newUser') === "true") {
                      history.push('/landingpage2');
                      dispatch(addToast('Appointment request created successfully!', 'ok'));
                    } else {
                      history.push('/');
                      dispatch(addToast('Appointment request created successfully!', 'ok'));
                    }
                  }
                });
              }
            }));
          } else {
            history.push('/');
            dispatch(addToast(payload.message, 'critical'));
          }
        }));
      }
    }));
  }

  render() {
    return (
      <LoadingSpinner />
    );
  }
}

const select = state => ({

});

export default connect(select)(AcceptTimeslotCallback);
