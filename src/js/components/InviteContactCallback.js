import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoadingSpinner from '../components/LoadingSpinner';
import { addToast } from '../actions/toasts';
import { getIntroductionToken } from '../actions/appointments';
import { getUserIntroductionList } from '../actions/introductions';
import { verifyToken } from '../actions/session';
import { updateInviteContact } from '../actions/contacts';
import { startAppointmentRequest, postAppointmentReRequest } from '../api/appointments';
import EditContactLayer from './EditContactLayer';
import { getContact } from '../actions/contacts';

class InviteContactCallback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.contact_id,
      token: this.props.match.params.token,
      layer: null
    };

  }

  componentWillMount() {
    const { dispatch, history } = this.props;
    dispatch(verifyToken(this.state.id, this.state.token, (payload) => {
      if (payload.verify) {
        localStorage.setItem('token', this.state.token);
        localStorage.setItem('name', payload.user.name);
        localStorage.setItem('email', payload.user.email);
        localStorage.setItem('onlyProfile', '1');
        dispatch(updateInviteContact(payload.user));
        dispatch(addToast('Verified your request!', 'ok'));
        this.props.history.push('/update_contact');
      } else {
        dispatch(addToast('Invalid Token', 'critical'));
        this.props.history.push('/signup');
      }
    }));
  }

  render() {
    return (
      <div>
        <LoadingSpinner />
      </div>
    );
  }
}

export default connect()(InviteContactCallback);
