import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { Layer, Article, Header, Heading, Section, Box, Footer, Button, Form ,Menu,Title  ,Value,Paragraph,Label} from 'grommet';
import ReactQuill from 'react-quill';
// import UserIcon from 'grommet/components/icons/base/User';
import { sendNewIntroductions, clearSelectedContacts } from '../actions/contacts'
import { addToast } from '../actions/toasts';
import CustomReactQuillIntroductions from './CustomReactQuillIntroductions';
// import Tiles from 'grommet/components/Tiles';
// import Tile from 'grommet/components/Tile';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import AddIcon from '@material-ui/icons/Add';

const styles = {
    card: {
        maxWidth: 345,
    },
    cardTitle: {
        backgroundColor: '#1b39a8',
        padding: '8px',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};

import CustomContactList from '../components/CustomContactList';

class CustomLayerHeaderCard extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        const { contact ,contacts, selected, secondaryIndex ,curStep, classes } = this.props;
        // let secondary_contacts = contacts.list.filter((contact) => {
        //     if(contact.id == contactId){
        //         return true;
        //     }
        // });

        // let contact = secondary_contacts[0];
        let title = contact.occupation ? contact.occupation.split(' at ')[0] : 'Title Not Specified';
        return (
            <Card className={classes.card}>
                <div className={classes.cardTitle} onClick={(e) => this.props.toggleSeclect(contact)}>
                    <Typography gutterBottom variant="headline" component="h2">
                        <span style={{ color: 'white', fontWeight: '600' }}>{contact.first_name.slice(0, 1).toUpperCase() + contact.last_name.slice(0, 1).toUpperCase()}</span>
                    </Typography>
                </div>
                <CardContent>
                    <Typography gutterBottom variant="headline" component="h2">
                        <span onClick={(e) => this.props.toggleSeclect(contact)}>
                            {contact.first_name} {contact.last_name}
                        </span>
                    </Typography>
                    <Typography variant="subheading" component="h3">
                        {title ? title : 'Title Not Specified'}
                    </Typography>
                </CardContent>
            </Card>
        );
    }
}

const select = state => ({
    session: state.session,
    contacts: state.contacts,
    selected: state.contacts.selected
});

export default withStyles(styles)(connect(select)(CustomLayerHeaderCard));