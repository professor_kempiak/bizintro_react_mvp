import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Box } from 'grommet';
import Spinning from 'grommet/components/icons/Spinning';
import purple from '@material-ui/core/colors/purple';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

class LoadingSpinner extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} />
      </div>
    );
  }
}

export default withStyles(styles)(connect()(LoadingSpinner));
