import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { Layer, Article, Header, Heading, Section, Box, Footer, Button, Form ,Menu,Title  ,Value,Paragraph,Label} from 'grommet';
import ReactQuill from 'react-quill';
// import UserIcon from 'grommet/components/icons/base/User';
import { sendNewIntroductions, clearSelectedContacts } from '../actions/contacts'
import { addToast } from '../actions/toasts';
import CustomReactQuillIntroductions from './CustomReactQuillIntroductions';
// import CloseIcon from 'grommet/components/icons/base/Close';
// import Card from 'grommet/components/Card';
// import Tiles from 'grommet/components/Tiles';
// import Tile from 'grommet/components/Tile';
// import Image from 'grommet/components/Image';
// import Pulse from 'grommet/components/icons/Pulse'
// import NextIcon from 'grommet/components/icons/base/Next';
// import PreviousIcon from 'grommet/components/icons/base/Previous';
// import AddIcon from 'grommet/components/icons/base/Add';

import CustomLayerHeaderCard from '../components/CustomLayerHeaderCard';
// import CheckmarkIcon from 'grommet/components/icons/base/Checkmark';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import AddIcon from '@material-ui/icons/Add';

const styles = {
    card: {
        maxWidth: 345,
    },
    cardTitle: {
        backgroundColor: '#1b39a8',
        padding: '8px',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};

class CustomLayerHeder extends Component {
    constructor(props) {
        super(props);

    }

    render() {

        const {contacts, curStep , addContactStatus, classes } = this.props;

        let selectItems = contacts.selected.secondary.map((selectCont, index) => {
            return (
                <CustomLayerHeaderCard key={ 'custom_card_key_'+index}
                                       contact={selectCont} secondaryIndex={index} curStep={curStep} />
            )
        });
        // let primary_contacts = contacts.list.filter((contact) => {
        //     let index = contacts.selected.primary.indexOf(contact.id);
        //     if (index > -1) {
        //         return true;
        //     }
        // });
        let rightIcon = <IconButton onClick={this.props.addContact}><AddIcon /></IconButton>;
        if(addContactStatus){
            rightIcon = <IconButton onClick={this.props.confirmContacts}><CheckIcon /></IconButton>;
        }
        // let contact = primary_contacts[0];
        let contact = contacts.selected.primary[0];

        let title = contact.occupation ? contact.occupation.split(' at ')[0] : 'Title Not Specified';
        return (
            <div className='introCardsHeader'>
                <div className={curStep == 1 ? 'fullOpacity introCard' : 'introCard'}>
                    <div className='introCardContainer'>
                        <Card className={classes.card}>
                            <div className={classes.cardTitle} onClick={(e) => this.props.toggleSeclect(contact)}>
                                <Typography gutterBottom variant="headline" component="h2">
                                    <span style={{ color: 'white', fontWeight: '600' }}>{contact.first_name.slice(0, 1).toUpperCase() + contact.last_name.slice(0, 1).toUpperCase()}</span>
                                </Typography>
                            </div>
                            <CardContent>
                                <Typography gutterBottom variant="headline" component="h2">
                                    <span onClick={(e) => this.props.toggleSeclect(contact)}>
                                        {contact.first_name} {contact.last_name}
                                    </span>
                                </Typography>
                                <Typography variant="subheading" component="h3">
                                    {title ? title : 'Title Not Specified'}
                                </Typography>
                            </CardContent>
                        </Card>
                    </div>
                </div>
                {selectItems}
                <div className={'layerAddContactBtn'}>
                    {rightIcon}
                </div>
            </div>
        );
    }
}

const select = state => ({
    session: state.session,
    contacts:state.contacts
});

export default withStyles(styles)(connect(select)(CustomLayerHeder));