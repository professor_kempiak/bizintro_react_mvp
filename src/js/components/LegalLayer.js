import React, { Component } from 'react';
import { connect } from 'react-redux';

// import Box from 'grommet/components/Box';
// import Layer from 'grommet/components/Layer';
// import Headline from 'grommet/components/Headline';
// import Heading from 'grommet/components/Heading';
// import Paragraph from 'grommet/components/Paragraph';

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({

})

class LegalLayer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
        <DialogContent>
          <Typography gutterBottom variant="headline" component="h2">
            <span style={{ color: 'white', fontWeight: '600' }}>Create Appointment Request</span>
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            <span style={{ color: 'white', fontWeight: '600' }}>Create Appointment Request</span>
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            <span style={{ color: 'white', fontWeight: '600' }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
            </span>
          </Typography>
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(styles)(connect()(LegalLayer));
