import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../actions/session';
import Auth0 from '../../Auth/Auth';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

class Auth0Callback extends Component {
  constructor(props) {
    super(props);
    //const auth0 = new Auth0();
    this._handleAuth0Authentication = this._handleAuth0Authentication.bind(this);
  }

  _handleAuth0Authentication(nextState) {
    if (/access_token|id_token|error/.test(nextState.location.hash)) {
      this.auth0.handleAuthentication();
    }
  }

  componentDidMount() {
    this._handleAuth0Authentication(this.props);
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} size={50} />
      </div>
    );
  }
}

const select = state => ({

});

export default withStyles(styles)(connect(select)(Auth0Callback));
