import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactQuill from 'react-quill';
import { sendNewIntroductions, clearSelectedContacts } from '../actions/contacts'
import { loadPrimaryTemplate, loadSecondaryTemplate } from '../actions/templates'
import { addToast } from '../actions/toasts';
import CustomReactQuillIntroductions from './CustomReactQuillIntroductions';
import { newTemplate } from '../actions/templates';
import TemplateForm from '../components/TemplateForm';
import CustomContactList from '../components/CustomContactList';
import CustomLayerHeder from '../components/CustomLayerHeder';

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import Navigate_Next from '@material-ui/icons/NavigateNext';
import Navigate_Before from '@material-ui/icons/NavigateBefore';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        width: '100% !important',
        margin: '8px 0',
    },
    button: {
        margin: theme.spacing.unit,
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
});

const localStorage = window.localStorage;

class MakeIntroductionLayer extends Component {

    constructor(props) {
        super(props);

        let { contacts } = this.props;
        let username = localStorage.getItem('name');
        // let primary_contacts = contacts.list.filter((contact) => {
        //     let index = contacts.selected.primary.indexOf(contact.id);
        //     if (index > -1) {
        //         return true;
        //     }
        // });
        let primary_contacts = [contacts.selected.primary[0]];

        // let secondary_contacts = contacts.list.filter((contact) => {
        //     let index = contacts.selected.secondary.indexOf(contact.id);
        //     if (index > -1) {
        //         return true;
        //     }
        // });
        let secondary_contacts = [contacts.selected.secondary[0]];

        let templates = this.props.templates.filter((template) => {
            if (template.type === 'introduction') {
                return true;
            }
        })

        this.state = {
            step: 1,
            step_1_contact: primary_contacts[0],
            step_2_contact: secondary_contacts[0],
            step_1_body: '',
            step_2_body: '',
            introductions: [],
            templates: templates,
            addContactStatus: false,
            showLayer: false,
            layer: null,
            quicksave_template_name: '',
            quicksave_template_subject: ''
        }

        this._addBio = this._addBio.bind(this);
        this._nextStep = this._nextStep.bind(this);
        this._prevStep = this._prevStep.bind(this);
        this._toggleLayer = this._toggleLayer.bind(this);
        this._addContacts = this._addContacts.bind(this);
        this._addTemplate = this._addTemplate.bind(this);
        this._saveTemplate = this._saveTemplate.bind(this);
        this._confirmContacts = this._confirmContacts.bind(this);
        this._sendIntroductions = this._sendIntroductions.bind(this);
        this._handleInputChange = this._handleInputChange.bind(this);
        this._submitNewTemplate = this._submitNewTemplate.bind(this);
        this._updateIntroductionMessages = this._updateIntroductionMessages.bind(this);
    }

    componentWillMount() {
        let { dispatch } = this.props;
        const token = localStorage.getItem('token');
        dispatch(loadPrimaryTemplate(token));
        dispatch(loadSecondaryTemplate(token));
    }

    componentWillReceiveProps(newProps) {
        if (newProps.primary.sucess && newProps.primary !== this.state.step_1_body) {
            this.setState({step_1_body: this.getTemplate(newProps.primary.template.body)})
        } 
        if (newProps.secondary.sucess && newProps.secondary !== this.state.step_2_body) {
            this.setState({step_2_body: this.getTemplate(newProps.secondary.template.body)})
        } 
        
    }

    getTemplate(template_body) {
        let username = localStorage.getItem('name');

        template_body = template_body.replace(/({primary_contact_first_name})/g, this.state.step_1_contact.first_name);
        template_body = template_body.replace(/({primary_contact_last_name})/g, this.state.step_1_contact.last_name);
        template_body = template_body.replace(/({secondary_contact_first_name})/g, this.state.step_2_contact.first_name);
        template_body = template_body.replace(/({secondary_contact_last_name})/g, this.state.step_2_contact.last_name);
        template_body = template_body.replace(/({maker_name})/g, username);


        return template_body;
    }

    _handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }

    _nextStep() {
        this.setState({
            step: this.state.step + 1
        });
    }

    _saveStep(b) {
        let { introductions, step } = this.state;
        let stepString = 'step_' + step + '_body';
        
        introductions[step] = {
            receipient_a: this.state.step_1_contact,
            receipient_a_message: this.getTemplate(this.state.step_1_body),
            receipient_b: b,
            receipient_b_message: this.getTemplate(this.state[stepString])
        }
        this.setState({ introductions: introductions });
    }

    _saveAndNextStep(b) {
        this._saveStep(b);
        this._nextStep();
    }

    _prevStep() {
        if (this.state.step > 1) {
            this.setState({
                step: this.state.step - 1
            });
        }
    }
    _addContacts() {
        this.setState({
            addContactStatus: true
        });
    }
    _confirmContacts() {
        this.setState({
            addContactStatus: false
        });
    }
    _sendIntroductions(b) {
        let { dispatch } = this.props;
        this._saveStep(b);

        let { introductions } = this.state;

        introductions.forEach((intro) => {
            dispatch(sendNewIntroductions(intro.receipient_a, intro.receipient_b, intro.receipient_a_message, intro.receipient_b_message, this.props.session.token, () => {}));
        });

        dispatch(clearSelectedContacts(() => {
            dispatch(addToast('Introductions have been successfully sent!', 'ok'))
            this.props.onClose();
            this.props.activeTemplate(0);
        }));
    }

    _updateIntroductionMessages(content, delta, source, editor) {
        let step = this.state.step;
        let stepString = 'step_' + step + '_body';
        this.setState({
            [stepString]: content
        });
    }

    _addBio(stateItem, contact, quill) {
        if (!this.state[contact].bio) {
            return this.props.dispatch(addToast('This contact has no bio information to add at this time.', 'critical'));
        }
        let cursorPosition = quill.getSelection().index;
        quill.clipboard.dangerouslyPasteHTML(cursorPosition, this.state[contact].bio);
    }

    _addTemplate(value, quill) {
        let template = this.props.templates.filter((t) => t.name === value);
        let template_body = template[0].body;

        template_body = this.getTemplate(template_body);
        quill.clipboard.dangerouslyPasteHTML(template_body);
    }

    _saveTemplate(body, quill) {
        const { classes, ...other } = this.props;
        this.setState({
            showLayer: true,
            layer: <Dialog open={true} onClose={this.props.onClose} aria-labelledby="form-dialog-title" {...other}>
                        <DialogTitle id="form-dialog-title">Save Template</DialogTitle>
                        <DialogContent>
                            <form onSubmit={this._submitNewTemplate}>
                                <div className="form-content">
                                    <div className="template_name">
                                        <TextField
                                            className={classes.margin}
                                            label="Template Name"
                                            id="template_name"
                                            name="quicksave_template_name"
                                            placeholder="Template Name"
                                            onChange={this._handleInputChange}
                                        />
                                    </div>
                                    <div className="quicksave_template_subject">
                                        <TextField
                                            className={classes.margin}
                                            label="Template Description"
                                            id="template_subject"
                                            name="quicksave_template_subject"
                                            placeholder="Description of Template"
                                            onChange={this._handleInputChange}
                                        />
                                    </div>
                                </div>
                                <div className={classes.buttonContainer}>
                                    <Button
                                        variant="contained"
                                        size="large"
                                        color="primary"
                                        className={classes.button}
                                        onClick={this._submitNewTemplate}
                                    >
                                        Save Template
                                    </Button>
                                </div>
                            </form>
                        </DialogContent>
                    </Dialog>
        })
    }

    _toggleLayer() {
        this.setState({
            showLayer: !this.state.showLayer
        })
    }

    _submitNewTemplate() {
        const { dispatch } = this.props;
        const token = localStorage.getItem('token');
        const step = this.state.step;
        const stepString = 'step_' + step + '_body';

        let template = {
            name: this.state.quicksave_template_name,
            type: 'introduction',
            subject: this.state.quicksave_template_subject,
            body: this.state[stepString]
        }

        dispatch(newTemplate(template, token, (payload) => {
            if (payload.success) {
                dispatch(addToast('Template added successfully!', 'ok'));
                this._toggleLayer();
            } else {
                dispatch(addToast('There as an error adding your template.', 'critical'));
            }
        }));
    }

    render() {

        let footerButtons = <div className={'btnGroup'}>
            <IconButton className={'PreBtn'}><Navigate_Before /></IconButton>
            <IconButton onClick={this._nextStep} className={'NextBtn'}><Navigate_Next /></IconButton></div>;
        let heading = 'To: ' + this.state.step_1_contact.first_name + ' ' + this.state.step_1_contact.last_name + ' (' + this.state.step_1_contact.email + ') ';
        let quill =  <div className={'searchWrapper'}>
                        <IconButton className={'closeBtn'} onClick={this.props.onClose}><CloseIcon /></IconButton>
                            <form ><Typography variant="headline" component="h2">Make Introduction</Typography>
                                <div>{heading}</div>
                                <CustomReactQuillIntroductions
                                    addBio={(quill) => this._addBio('step_1_body', 'step_2_contact', quill)}
                                    addTemplate={(value, quill) => this._addTemplate(value, quill)}
                                    saveTemplate={(value, quill) => this._saveTemplate(value, quill)}
                                    templates={this.state.templates}
                                    onChange={this._updateIntroductionMessages}
                                    value={this.state.step_1_body} />
                                <div>{footerButtons}</div>
                            </form>
                    </div>;


        if (this.state.step >= 2) {

            let { contacts } = this.props;
            let secondaryIndex = this.state.step - 2;
            // let secondary_contacts = contacts.list.filter((contact) => {
            //     if (contact == contacts.selected.secondary[secondaryIndex]) {
            //         return true;
            //     }
            // });
            let secondary_contacts = [contacts.selected.secondary[secondaryIndex]];
            let username = localStorage.getItem('name');
            // let primary_contacts = contacts.list.filter((contact) => {
            //     let index = contacts.selected.primary.indexOf(contact);
            //     if (index > -1) {
            //         return true;
            //     }
            // });
            let primary_contacts = [contacts.selected.primary[0]];
            heading = 'To: ' + secondary_contacts[0].first_name + ' ' + secondary_contacts[0].last_name + ' (' + secondary_contacts[0].email + ') ';
            if (this.state.step >= (contacts.selected.secondary.length + 1)) {
                footerButtons = <div className={'btnGroup'}>
                    <IconButton onClick={this._prevStep} className={'PreBtn'}><Navigate_Before /></IconButton>
                    <IconButton onClick={this._prevStep} className={'NextBtn'}  onClick={() => this._sendIntroductions(secondary_contacts[0])}><CheckIcon /></IconButton>
                </div>;
            }
            else {
                footerButtons = <div className={'btnGroup'}>
                    <IconButton onClick={this._prevStep} className={'PreBtn'}><Navigate_Before /></IconButton>
                    <IconButton onClick={() => this._saveAndNextStep(secondary_contacts[0])} className={'NextBtn'}><Navigate_Next /></IconButton></div>;
            }
            const quillValue = 'Hi ' + secondary_contacts[0].first_name + ' ' + secondary_contacts[0].last_name + ' ' + '( ' + secondary_contacts[0].email +  ' )' + ',<br><br>I hope you are doing well.<br><br>I wanted to let you know that I have recently had a discussion with <strong>' + primary_contacts[0].first_name + '</strong> regarding you, and I think it would be mutually beneficial for you both to meet.<br><br><strong>' + primary_contacts[0].first_name + '</strong> will be reaching out using an automated introduction tool that allows him to schedule dates and times right in the email itself.<br><br>You will get an email with suggested dates and if these do not work you will be able to offer some alternates.<br><br>Let me know if you need any other information.<br><br>Thanks,<br>' + username;
            const stepString = 'step_' + this.state.step + '_body';
            quill =  <div className={'searchWrapper'}>
                        <IconButton className={'closeBtn'} onClick={this.props.onClose}><CloseIcon /></IconButton>
                        <form ><Typography variant="headline" component="h2">Make Introduction</Typography>
                            <div>{heading}</div>
                            <CustomReactQuillIntroductions
                                addBio={(quill) => this._addBio('step_' + this.state.step + '_body', 'step_' + (this.state.step - 1) + '_contact', quill)}
                                addTemplate={(value, quill) => this._addTemplate(value, quill)}
                                saveTemplate={(value, quill) => this._saveTemplate(value, quill)}
                                templates={this.state.templates}
                                onChange={this._updateIntroductionMessages}
                                value={this.state[stepString] ? this.state[stepString] : quillValue} />
                            <div>{footerButtons}</div>
                        </form>
                    </div>;


        }
        if (this.state.addContactStatus) {
            quill = <CustomContactList search={this.props.search.tags}/>
        }
        return (
            <Dialog open={true}>
                <DialogContent>
                    {this.state.showLayer ? this.state.layer : null}
                    <div direction='row' className={'introLayerWrapper'}>
                        <div>
                            <CustomLayerHeder
                                curStep={this.state.step}
                                addContact={this._addContacts}
                                addContactStatus={this.state.addContactStatus}
                                confirmContacts={this._confirmContacts} />
                        </div>
                    </div>
                    <div>
                        {quill}
                    </div>
                </DialogContent>
            </Dialog>
        );
    }
}


const select = state => ({
    session: state.session,
    contacts:state.contacts,
    templates: state.templates.list,
    search: state.search,
    primary: state.templates.primary,
    secondary: state.templates.secondary,
});

export default withStyles(styles)(connect(select)(MakeIntroductionLayer));