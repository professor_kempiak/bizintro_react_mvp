import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import NavigateNext from '@material-ui/icons/NavigateNext';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';

const styles = (theme) => ({

})


class CustomMapPlainingMeeting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notification: null,
        }

    }

    render() {

        return (
            <div className={'formTiles'}>
                <div className={'formsubTile'}>
                    <div>
                        <Typography gutterBottom variant="headline" component="h2">
                            <span style={{ fontWeight: '600' }}>What days of the week are you planning on meeting in {this.props.zone_name}?</span>
                        </Typography>
                        <div className={'panelWrapper'}>
                            <div>
                                <List>
                                    <ListItem>
                                        <span>Select Day</span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Monday-selected'
                                                name='Monday-selected'
                                                label='Monday'
                                                checked={this.props.values.Monday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Tuesday-selected'
                                                name='Tuesday-selected'
                                                label='Tuesday'
                                                checked={this.props.values.Tuesday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Wednesday-selected'
                                                name='Wednesday-selected'
                                                label='Wednesday'
                                                checked={this.props.values.Wednesday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Thursday-selected'
                                                name='Thursday-selected'
                                                label='Thursday'
                                                checked={this.props.values.Thursday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Friday-selected'
                                                name='Friday-selected'
                                                label='Friday'
                                                checked={this.props.values.Friday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Saturday-selected'
                                                name='Saturday-selected'
                                                label='Saturday'
                                                checked={this.props.values.Saturday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <RadioButton
                                                readOnly
                                                id='Sunday-selected'
                                                name='Sunday-selected'
                                                label='Sunday'
                                                checked={this.props.values.Sunday.selected}
                                                onClick={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                </List>
                            </div>
                            <div>
                                <List>
                                    <ListItem>
                                        <span>Recurring</span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Monday-recurring'
                                                toggle={true} 
                                                checked={this.props.values.Monday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Tuesday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Tuesday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Wednesday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Wednesday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Thursday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Thursday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Friday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Friday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Saturday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Saturday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                    <ListItem>
                                        <span>
                                            <Checkbox 
                                                name='Sunday-recurring'
                                                toggle={true}  
                                                checked={this.props.values.Sunday.recurring}
                                                onChange={this.props.handleRadioOrCheckboxChange} />
                                        </span>
                                    </ListItem>
                                </List>
                            </div>
                        </div>
                    </div>
                    <div className={'formBottomBtns'}>
                        <IconButton onClick={this.props.preStep}><NavigateBefore /></IconButton>
                        <IconButton onClick={this.props.nextStep}><NavigateNext /></IconButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(CustomMapPlainingMeeting);
