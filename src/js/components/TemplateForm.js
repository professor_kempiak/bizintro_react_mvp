import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { editContact } from '../actions/contacts';
import { addToast } from '../actions/toasts';
import CustomReactQuillTemplates from './CustomReactQuillTemplates';
import { editTemplate, newTemplate } from '../actions/templates';

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

class TemplateForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formMode: 'add',
      template_id: Date.now(),
      template_name: '',
      template_type: {},
      template_subject: '',
      template_body: '',
      template_last_modified: Date.now()
    }

    this._handleSubmitForm = this._handleSubmitForm.bind(this);
    this._handleQuillChange = this._handleQuillChange.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleSelectChange = this._handleSelectChange.bind(this);
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  _handleSelectChange(e) {
    this.setState({
      template_type: e.target.value
    })
  }

  _handleQuillChange(value) {
    this.setState({
      template_body: value
    });
  }

  _handleSubmitForm() {
    let { dispatch } = this.props;
    let token = localStorage.getItem('token');

    let template = {
      id: this.state.template_id,
      name: this.state.template_name,
      type: this.state.template_type,
      subject: this.state.template_subject,
      body: this.state.template_body,
      last_modified: this.state.template_last_modified
    }

    if (this.state.formMode === 'edit') {
      // Save Existing Record
      dispatch(editTemplate(template, token, (payload) => {
        if (payload.success) {
          dispatch(addToast('Template saved successfully!', 'ok'));
          this.props.onSubmit();
        } else {
          dispatch(addToast('There as an error editing your template.', 'critical'));
        }
      }));
    } else {
      // Add New
      dispatch(newTemplate(template, token, (payload) => {
        if (payload.success) {
          dispatch(addToast('Template added successfully!', 'ok'));
          this.props.onSubmit();
        } else {
          dispatch(addToast('There as an error adding your template.', 'critical'));
        }
      }));
    }

  }

  componentDidMount() {
    if (this.props.formMode) {
      this.setState({
        formMode: this.props.formMode
      });
    }

    if (this.props.id) {
      let template = this.props.templates.find((element) => {
        return element.id === this.props.id;
      });
      this.setState({
        template_id: template.id,
        template_name: template.name,
        template_type: { value: template.type, label: template.type.charAt(0).toUpperCase() + template.type.substr(1) },
        template_subject: template.subject,
        template_body: template.body,
        template_last_modified: template.last_modified
      });
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <form onSubmit={this._handleSubmitForm} className="template-form">
        <Typography gutterBottom variant="headline" component="h2">
          <span style={{ color: 'white', fontWeight: '600' }}>{this.state.formMode === 'add' ? 'Add' : 'Edit'} Template</span>
        </Typography>
        {/* {this.state.notification ? this.state.notification : null} */}
        <div className="template-name">
          <TextField
            className={classes.margin}
            label="Template Name"
            id="template_name"
            name="template_name"
            placeholder="Template Name"
            value={this.state.template_name}
            onChange={this._handleInputChange}
          />
        </div>
        <div className={'template-type'}>
          <Select
            value={this.state.template_type}
            onChange={this._handleSelectChange}
            name="templateType"
            className={classes.select}
            input={<Input name="templateType" id="day-helper" />}
          >
            <MenuItem value="introduction">Introduction</MenuItem>
            <MenuItem value="appointment">Appointment</MenuItem>
          </Select>
        </div>
        <div className="template-subject">
          <TextField
            className={classes.margin}
            label="Template Description"
            id="template_subject"
            name="template_subject"
            placeholder="Description of Template"
            value={this.state.template_subject}
            onChange={this._handleInputChange}
          />
        </div>
        <div className={'template-body'}>
          <CustomReactQuillTemplates
            onChange={this._handleQuillChange}
            value={this.state.template_body} />
        </div>
        <div className={'save-btn'}>
          <Button variant="contained" color="primary" onClick={this._handleSubmitForm}color="primary">Save Template</Button>
        </div>
      </form>
    );
  }
}

const select = state => ({
  templates: state.templates.list
});

export default withStyles(styles)(connect(select)(TemplateForm));
