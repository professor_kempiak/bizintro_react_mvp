import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import NextIcon from '@material-ui/icons/NavigateNext';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

const styles = (theme) => ({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
})

const grommetPath = '#';

class NewsFeed extends Component {
  _onClickCard(path, event) {
    event.preventDefault();
    window.location.href = path;
  }

  render() {
    const { classes } = this.props;
    const twitterIconBox = (
      <div>
        <i className="fab fa-twitter"/>
      </div>
    );

    const facebookIconBox = (
      <div>
        <i className="fab fa-facebook"/>
      </div>
    );

    const linkedinIconBox = (
      <div>
        <i className="fab fa-linkedin"/>
      </div>
    );

    const socialFeedCard1 = (
      <Card className={classes.card}>
        <CardContent>
          <Typography component="p">
            Social
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            Protect Your Digital Enterprise ipsum lorem dolores aeat el
          </Typography>
        </CardContent>
        <CardActions>
          <IconButton>
            <a href="http://www.twitter.com">{twitterIconBox}</a>
          </IconButton>
        </CardActions>
      </Card>
    );

    const socialFeedCard2 = (
      <Card className={classes.card}>
        <CardContent>
          <Typography component="p">
            Social
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            Protect Your Digital Enterprise ipsum lorem dolores aeat el
          </Typography>
        </CardContent>
        <CardActions>
          <IconButton>
            <a href="http://www.facebook.com">{facebookIconBox}</a>
          </IconButton>
        </CardActions>
      </Card>
    );

    const socialFeedCard3 = (
      <Card className={classes.card}>
        <CardContent>
          <Typography component="p">
            Social
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            Protect Your Digital Enterprise ipsum lorem dolores aeat el
          </Typography>
        </CardContent>
        <CardActions>
          <IconButton>
            <a href="http://www.linkedin.com">{linkedinIconBox}</a>
          </IconButton>
        </CardActions>
      </Card>
    );

    const blogPostCard = (
      <Card className={classes.card}>
        <CardContent>
          <Typography component="p">
            Featured Post
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            Protect Your Digital Enterprise ipsum lorem dolores aeat el
          </Typography>
        </CardContent>
        <CardActions>
          <MenuItem>
            <a href={grommetPath}><NextIcon /> Learn More</a>
          </MenuItem>
        </CardActions>
      </Card>
    );

    const featuredPostCard = (
      <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image="/img/carousel-1.png"
        />
        <CardContent>
          <Typography component="p">
            Featured Post
          </Typography>
          <Typography gutterBottom variant="headline" component="h2">
            Protect Your Digital Enterprise ipsum lorem dolores aeat el
          </Typography>
        </CardContent>
        <CardActions>
          <MenuItem>
            <a href={grommetPath}><NextIcon /> Learn More</a>
          </MenuItem>
        </CardActions>
      </Card>
    );

    return (
      <div className="card-list-container">
        <div className="card-list-content">
          {blogPostCard}
          {featuredPostCard}
          {socialFeedCard1}
          {socialFeedCard2}
          {blogPostCard}
          {featuredPostCard}
          {featuredPostCard}
          {socialFeedCard3}
        </div>
      </div>
    );
  }
};

export default withStyles(styles)(NewsFeed);
