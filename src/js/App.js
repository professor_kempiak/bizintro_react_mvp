import React, { Component } from 'react';
// import { IntlProvider, addLocaleData } from 'react-intl';
// import en from 'react-intl/locale-data/en';
// import { getCurrentLocale, getLocaleData } from 'grommet/utils/Locale';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { initialize } from './actions/session';
import store from './store';
import Main from './components/Main';

// const locale = getCurrentLocale();
// addLocaleData(en);
// let messages;
// try {
//   messages = require(`./messages/${locale}`);
// } catch (e) {
//   messages = require('./messages/en-US');
// }
// const localeData = getLocaleData(messages, locale);
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1b39a8'
    },
  },
});

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          {/* <IntlProvider locale={localeData.locale} messages={localeData.messages}> */}
          {/* <IntlProvider> */}
          <Main />
          {/* </IntlProvider> */}
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
