import {
  INTRODUCTION_LIST, 
  INTRODUCTION_STATUS,
  INTRODUCTIONS_ADD
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  list: []
};

const handlers = {
  [INTRODUCTION_LIST]: (state, action) => {
    return action.payload;
  },
  [INTRODUCTION_STATUS]: (state, action) => {
    return { list: action.payload };
  },
  [INTRODUCTIONS_ADD]: (state, action) => {
    if (action.payload.length <= 0) { return { end_of_results: true } }
    let addItems = [];
    action.payload.forEach((incomingIntroduction) => {
      let introFound = state.list.find((loadedIntroduction) => loadedIntroduction.id === incomingIntroduction.id);
      if (!introFound) {
        addItems.push(incomingIntroduction);
      }
    });
    return { list: [...state.list, ...addItems] };
  }
};

export default createReducer(initialState, handlers);
