import {
  TEMPLATE_ADD,
  TEMPLATE_EDIT,
  TEMPLATE_REMOVE,
  TEMPLATES_LOAD,
  PRIMARY_INTRO_TEMPLATE, 
  SECONDARY_INTRO_TEMPLATE, 
  ACCEPT_AVAIL_TEMPLATE
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  list: [],
  primary: {},
  secondary: {},
  accept: {}
}

const handlers = {
  [TEMPLATES_LOAD]: (state, action) => {
    return { list: action.payload }
  },
  [TEMPLATE_ADD]: (state, action) => {
    return { list: [...state.list, action.payload] }
  },
  [TEMPLATE_EDIT]: (state, action) => {
    let templates = state.list.map((template) => {
      if (template.id === action.payload.id) {
        return action.payload;
      }
      return template;
    });
    return { list: templates }
  },
  [TEMPLATE_REMOVE]: (state, action) => {
    let templates = state.list.filter((template) => {
      if (template.id === action.payload) {
        return false;
      }
      return true;
    });
    return { list: templates }
  },
  [PRIMARY_INTRO_TEMPLATE]: (state, action) => {
    return { primary: action.payload }
  },
  [SECONDARY_INTRO_TEMPLATE]: (state, action) => {
    return { secondary: action.payload }
  },
  [ACCEPT_AVAIL_TEMPLATE]: (state, action) => {
    return { accept: action.payload }
  }
};

export default createReducer(initialState, handlers);
