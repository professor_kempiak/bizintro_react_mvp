import {
  SEARCH_CLEAR,
  SEARCH_TAG_ADD,
  SEARCH_TAG_DELETE,
  SEARCH_UPDATE,
  SEARCH_TERM_UPDATE,
  SEARCH_RESET
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  tags: [],
  term: '',
  reset: false
}

const handlers = {
  [SEARCH_CLEAR]: (state, action) => {
    return initialState;
  },
  [SEARCH_RESET]: (state, action) => {
    return { reset: action.payload }
  },
  [SEARCH_TAG_ADD]: (state, action) => {
    let tags = [].concat(state.tags, action.payload);
    return { tags: tags }
  },
  [SEARCH_TAG_DELETE]: (state, action) => {
    let tags = state.tags.slice(0);
    tags.splice(action.payload, 1);
    return { tags: tags };
  },
  [SEARCH_UPDATE]: (state, action) => {
    let tags = action.payload.map((tag) => {
      return tag.name
    });
    return { tags: tags };
  },
  [SEARCH_TERM_UPDATE]: (state, action) => {
    return { term: action.payload }
  }
};

export default createReducer(initialState, handlers);
