import {
  CONTACT_ADD,
  CONTACT_EDIT,
  CONTACTS_LOAD,
  CONTACT_SELECT_PRIMARY,
  CONTACT_SELECT_SECONDARY,
  CONTACTS_INTRODUCED,
  CONTACT_SELECT_CLEAR,
  CONTACT_SELECT_REMOVE,
  CONTACTS_EMPTY,
  CONTACTS_LOADING,
  UPDATE_INVITE_CONTACT
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  list: [],
  selected: {
    primary: [],
    secondary: []
  },
  loading: false,
  invite_contact: {}
}

const handlers = {
  [CONTACT_ADD]: (state, action) => {
    return { list: [...state.list, action.payload] };
  },
  [CONTACT_EDIT]: (state, action) => {
    let contacts = state.list.map((contact) => {
      if (contact.id === action.payload.id) {
        return action.payload;
      }
      return contact;
    });
    return { list: contacts };
  },
  [CONTACTS_LOAD]: (state, action) => {
    let addItems = [];
    action.payload.forEach((new_contact) => {
      let contactFound = state.list.find((existing_contact) => existing_contact.id === new_contact.id);
      if (!contactFound) {
        addItems.push(new_contact);
      }
    });
    return { list: [...state.list, ...addItems] };
  },
  [CONTACT_SELECT_PRIMARY]: (state, action) => {
    return { selected: { primary: [...state.selected.primary, action.payload], secondary: [...state.selected.secondary] } }
  },
  [CONTACT_SELECT_SECONDARY]: (state, action) => {
    return { selected: { primary: [...state.selected.primary], secondary: [...state.selected.secondary, action.payload] } }
  },
  [CONTACT_SELECT_CLEAR]: (state, action) => {
    return { selected: initialState.selected };
  },
  [CONTACT_SELECT_REMOVE]: (state, action) => {
    let { primary, secondary } = state.selected;
    let prim_index = primary.indexOf(action.payload);
    if (prim_index > -1) {
      primary.splice(prim_index, 1);
    }
    let sec_index = secondary.indexOf(action.payload);
    if (sec_index > -1) {
      secondary.splice(sec_index, 1);
    }
    return { selected: { primary: primary, secondary: secondary } }
  },
  [CONTACTS_INTRODUCED]: (state, action) => {
    return action.payload;
  },
  [CONTACTS_LOADING]: (state, action) => {
    return { loading: action.payload }
  },
  [CONTACTS_EMPTY]: (state, action) => {
    return { list: [] };
  },
  [UPDATE_INVITE_CONTACT]: (state, action) => {
    return { invite_contact: action.payload };
  }
};

export default createReducer(initialState, handlers);
