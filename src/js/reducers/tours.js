import {
  TOUR_LIST
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  list: []
}

const handlers = {
  [TOUR_LIST]: (state, action) => {
    return { list: action.payload }
  }
};

export default createReducer(initialState, handlers);
