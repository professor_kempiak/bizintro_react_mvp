import {
  TOAST_ADD, TOASTS_CLEAR
} from '../actions';
import { createReducer } from './utils';

const initialState = {
    list: []
}

const handlers = {
  [TOAST_ADD]: (state, action) => {
      return { list: [action.payload] };
  },
  [TOASTS_CLEAR]: (state, action) => {
      return { list: [] };
  }
};

export default createReducer(initialState, handlers);
