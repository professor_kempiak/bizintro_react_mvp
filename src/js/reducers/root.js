import { combineReducers } from 'redux';

import session from './session';
import contacts from './contacts';
import search from './search';
import toasts from './toasts';
import calendar from './calendar';
import introductions from './introductions';
import templates from './templates';
import appointment from './appointment';
import zones from './zones';
import tours from './tours';

export default combineReducers({
  session,
  contacts,
  search,
  toasts,
  calendar,
  introductions,
  appointment,
  templates,
  zones,
  tours
});
