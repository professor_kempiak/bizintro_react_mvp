import {
  APPT_LIST,
  SET_APPOINTMENT
} from '../actions';
import { createReducer } from './utils';

const initialState = {
    apptList: [],
    isSetting: false
};

const handlers = {
  [APPT_LIST]: (state, action) => {
    return { apptList: action.payload };
  },
  [SET_APPOINTMENT]: (state, action) => {
  	return { isSetting: action.payload}
  },
};

export default createReducer(initialState, handlers);
