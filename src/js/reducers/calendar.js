import {
  CALENDAR_DATE_SET, CALENDAR_VIEW_SET, CALENDAR_EVENT_ADD,CALENDAR_EVENT_REMOVE,CALENDAR_EVENT_GET
} from '../actions';
import { createReducer } from './utils';

const initialState = {
    defaultView: 'week',
    defaultDate: new Date(Date.now()),
    events: [],
    g_events: []
};

const handlers = {
  [CALENDAR_DATE_SET]: (state, action) => {
    return { defaultDate: action.payload };
  },
  [CALENDAR_VIEW_SET]: (state, action) => {
    return { defaultView: action.payload };
  },
  [CALENDAR_EVENT_ADD]: (state, action) => {
    return { events: [...state.events, action.payload] };
  },
  [CALENDAR_EVENT_REMOVE]: (state, action) =>{
      let events = state.events.filter((event) => {
          if ((event.end === action.payload.end) && (event.start === action.payload.start)) {
              return false;
          }
          return true;
      });
      return { events: events }
  },
  [CALENDAR_EVENT_GET]: (state, action) => {
    return { events: [...state.events, ...action.payload] }
  }
};

export default createReducer(initialState, handlers);
