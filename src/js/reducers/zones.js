import {
    ZONE_ADD,
    ZONE_EDIT,
    ZONE_REMOVE,
    ZONES_LOAD
  } from '../actions';
  import { createReducer } from './utils';
  
  const initialState = {
    list: [],
    appointments: [
      { lat: 41.52916347, long: -87.5625333, day: "Monday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.49960695, long: -87.4015279, day: "Tuesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.52128377, long: -87.5625333, day: "Wednesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.53005939, long: -87.54981, day: "Thursday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.49313, long: -87.9548136, day: "Friday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.51811784, long: -87.957233, day: "Saturday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.53430039, long: -87.012486, day: "Sunday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.5073853, long: -87.320058, day: "Wednesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.50597426, long: -87.152339, day: "Monday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
      { lat: 41.52395143, long: -87.4165897, day: "Tuesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    ]
  }
  
  const handlers = {
    [ZONE_ADD]: (state, action) => {
      return { list: [...state.list, action.payload] }
    },
    [ZONE_EDIT]: (state, action) => {
      let zones = state.list.map((zone) => {
        if (zone.id === action.payload.id) {
          return action.payload;
        }
        return zone;
      });
      return { list: zones };
    },
    [ZONE_REMOVE]: (state, action) => {
      let zones = state.list.filter((zone) => {
        if (zone.id === action.payload) {
          return false
        }
        return true;
      });
      return { list: zones }
    },
    [ZONES_LOAD]: (state, action) => {
      return { list: action.payload }
    },
  };
  
  export default createReducer(initialState, handlers);