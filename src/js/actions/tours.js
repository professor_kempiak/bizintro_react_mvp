import { TOUR_LIST } from '../actions';
import { fetchTours } from '../api/tours';
import { addToast } from './toasts';

export function loadTours(token) {
  return (dispatch) => {
    fetchTours(token)
      .then((payload) => {
        dispatch({ type: TOUR_LIST, payload: JSON.parse(payload) });
      });
  }
}
