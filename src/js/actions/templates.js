import { TEMPLATES_LOAD, TEMPLATE_ADD, TEMPLATE_EDIT, TEMPLATE_REMOVE, PRIMARY_INTRO_TEMPLATE, SECONDARY_INTRO_TEMPLATE, ACCEPT_AVAIL_TEMPLATE } from '../actions';
import { fetchTemplates, createTemplate, updateTemplate, deleteTemplate, fetchMainTemplates } from '../api/templates';
import { addToast } from './toasts';

export function loadTemplates(token) {
  return (dispatch) => {
    fetchTemplates(token)
      .then((payload) => {
        dispatch({ type: TEMPLATES_LOAD, payload: JSON.parse(payload) });
      });
  }
}

export function newTemplate(template, token, cb) {
  return (dispatch) => {
    createTemplate(template, token)
      .then((payload) => {
        dispatch({ type: TEMPLATE_ADD, payload: template });
        cb(payload);
      });
  }
}

export function editTemplate(template, token, cb) {
  return (dispatch) => {
    updateTemplate(template, token)
      .then((payload) => {
        dispatch({ type: TEMPLATE_EDIT, payload: template });
        cb(payload);
      });
  }
}

export function removeTemplate(id, token) {
  return (dispatch) => {
    deleteTemplate(id, token)
      .then((payload) => {
        let incomingPayload = JSON.parse(payload);
        if (incomingPayload.success === true) {
          dispatch(addToast('Template has been deleted successfully!', 'ok'));
          dispatch({ type: TEMPLATE_REMOVE, payload: id });
        } else {
          dispatch(addToast('An error occured trying to delete your template.', 'critical'));
        }
      });
  }
}

export function loadPrimaryTemplate(token) {
  return (dispatch) => {
    fetchMainTemplates(token, 'primary_introduction')
      .then((payload) => {
        dispatch({ type: PRIMARY_INTRO_TEMPLATE, payload: JSON.parse(payload) });
      });
  }
}

export function loadSecondaryTemplate(token) {
  return (dispatch) => {
    fetchMainTemplates(token, 'secondary_introduction')
      .then((payload) => {
        dispatch({ type: SECONDARY_INTRO_TEMPLATE, payload: JSON.parse(payload) });
      });
  }
}


export function loadAcceptTemplate(token) {
  return (dispatch) => {
    fetchMainTemplates(token, 'appointment_availability')
      .then((payload) => {
        dispatch({ type: ACCEPT_AVAIL_TEMPLATE, payload: JSON.parse(payload) });
      });
  }
}
