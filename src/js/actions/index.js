// See https://github.com/acdlite/flux-standard-action

// Session
export const SESSION_LOAD = 'SESSION_LOAD';
export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';

// Contacts
export const CONTACT_ADD = 'CONTACT_ADD';
export const CONTACT_EDIT = 'CONTACT_EDIT';
export const CONTACTS_LOAD = 'CONTACTS_LOAD';
export const CONTACTS_INTRODUCED = 'CONTACTS_INTRODUCED';
export const CONTACTS_SEARCH_ADD = 'CONTACTS_SEARCH_ADD';
export const CONTACT_SELECT_PRIMARY = 'CONTACT_SELECT_PRIMARY';
export const CONTACT_SELECT_SECONDARY = 'CONTACT_SELECT_SECONDARY';
export const CONTACT_SELECT_CLEAR = 'CONTACT_SELECT_CLEAR';
export const CONTACT_SELECT_REMOVE = 'CONTACT_SELECT_REMOVE';
export const CONTACTS_LOADING = 'CONTACTS_LOADING';
export const CONTACTS_EMPTY = 'CONTACTS_EMPTY';
export const UPDATE_INVITE_CONTACT = 'UPDATE_INVITE_CONTACT';
// Search
export const SEARCH_UPDATE = 'SEARCH_UPDATE';
export const SEARCH_TERM_UPDATE = 'SEARCH_TERM_UPDATE';
export const SEARCH_CLEAR = 'SEARCH_CLEAR';
export const SEARCH_TAG_ADD = 'SEARCH_TAG_ADD';
export const SEARCH_TAG_DELETE = 'SEARCH_TAG_DELETE';
export const SEARCH_RESET = 'SEARCH_RESET';

// Toasts
export const TOAST_ADD = 'TOAST_ADD';
export const TOASTS_CLEAR = 'TOASTS_CLEAR';

// Introductions
export const INTRODUCTION_LIST = 'INTRODUCTION_LIST';
export const INTRODUCTION_STATUS = 'INTRODUCTION_STATUS';
export const INTRODUCTIONS_ADD = 'INTRODUCTIONS_ADD';

// Calendar
export const CALENDAR_DATE_SET = 'CALENDAR_DATE_SET';
export const CALENDAR_VIEW_SET = 'CALENDAR_VIEW_SET';
export const CALENDAR_EVENT_ADD = 'CALENDAR_EVENT_ADD';
export const CALENDAR_EVENT_GET = 'CALENDAR_EVENT_GET';
export const CALENDAR_EVENT_REMOVE = 'CALENDAR_EVENT_REMOVE';

// Templates
export const TEMPLATES_LOAD = 'TEMPLATES_LOAD';
export const TEMPLATE_ADD = 'TEMPLATE_ADD';
export const TEMPLATE_EDIT = 'TEMPLATE_EDIT';
export const TEMPLATE_REMOVE = 'TEMPLATE_REMOVE';

// Appointments
export const USER_APPT_LIST = 'USER_APPT_LIST';
export const USER_MAP_APPT_LIST = 'USER_MAP_APPT_LIST';
export const APPT_LIST = 'APPT_LIST';
export const SET_APPOINTMENT = 'SET_APPOINTMENT';

// Zones
export const ZONE_ADD = 'ZONE_ADD';
export const ZONE_EDIT = 'ZONE_EDIT';
export const ZONE_REMOVE = 'ZONE_REMOVE';
export const ZONES_LOAD = 'ZONES_LOAD';

// Tours
export const TOUR_LIST = 'TOUR_LIST';

// main templates
export const PRIMARY_INTRO_TEMPLATE = 'PRIMARY_INTRO_TEMPLATE';
export const SECONDARY_INTRO_TEMPLATE = 'SECONDARY_INTRO_TEMPLATE';
export const ACCEPT_AVAIL_TEMPLATE = 'ACCEPT_AVAIL_TEMPLATE';

// rank
export const UPDATE_RANK_CONTACT = 'UPDATE_RANK_CONTACT';