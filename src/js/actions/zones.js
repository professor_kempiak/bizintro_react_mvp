import {
  ZONE_ADD,
  ZONE_EDIT,
  ZONE_REMOVE,
  ZONES_LOAD
} from '../actions';
import { fetchZones, updateZone, createZone, deleteZone } from '../api/zones';
import { addToast } from './toasts';

export function addZone(zone, token) {
  return (dispatch) => {
    createZone(zone, token)
      .then((payload) => {
        if (payload.success) {
          dispatch(getZones(token));
          dispatch(addToast('Zone created successfully', 'ok'));
        } else {
          dispatch(addToast('Error creating zone', 'critical'));
        }
      })
  }
}

export function editZone(zone, token) {
  return (dispatch) => {
    updateZone(zone, token)
      .then((payload) => {
        if (payload.success) {
          dispatch({ type: ZONE_EDIT, payload: zone });
          dispatch(addToast('Zone updated successfully', 'ok'));
        } else {
          dispatch(addToast('Error updating zone', 'critical'));
        }
      })
  }
}

export function removeZone(zone_id, token) {
  return (dispatch) => {
    deleteZone(zone_id, token)
      .then((payload) => {
        const response = JSON.parse(payload);
        if (response.success) {
          dispatch({ type: ZONE_REMOVE, payload: zone_id });
          dispatch(addToast('Zone removed successfully', 'ok'));
        } else {
          dispatch(addToast('Error removing zone', 'critical'));
        }
      });
  }
}

export function getZones(token) {
  return (dispatch) => {
    fetchZones(token)
      .then((payload) => {
        let zoneData = JSON.parse(payload);
        dispatch({ type: ZONES_LOAD, payload: zoneData });
      })
  }
}