import { 
  SEARCH_CLEAR,
  SEARCH_TAG_ADD,
  SEARCH_TAG_DELETE,
  SEARCH_UPDATE,
  SEARCH_TERM_UPDATE,
  SEARCH_RESET
} from '../actions';

export function updateSearchTags(tags) {
  return (dispatch) => {
    dispatch({ type: SEARCH_UPDATE, payload: tags })
  }
}

export function updateSearchTerm(term) {
  return (dispatch) => {
    dispatch({ type: SEARCH_TERM_UPDATE, payload: term });
  }
}

export function updateSearchClear(bool) {
  return (dispatch) => {
    dispatch({ type: SEARCH_RESET, payload: bool });
  }
}

export function clearSearch() {
  return (dispatch) => {
    dispatch({ type: SEARCH_CLEAR });
  }
}

export function addSearchTag(tag) {
  return (dispatch) => {
    dispatch({ type: SEARCH_TAG_ADD, payload: tag });
  }
}

export function removeSearchTag(index) {
  return (dispatch) => {
    dispatch({ type: SEARCH_TAG_DELETE, payload: index });
  }
}
