import { SESSION_LOGIN, INTRODUCTION_LIST, USER_APPT_LIST, APPT_LIST, SET_APPOINTMENT } from '../actions';
import { getAutoLoginToken, getIntroductionList, getApptRequestList,getApptMapList, postAppointmentRequest, postAppointmentAccept, getApptListAPI } from '../api/appointments';
import { updateHeaders } from '../api/utils';

export function getIntroductionToken(uuid, type, done) {
  return dispatch => (
    getAutoLoginToken(uuid, type, done)
      .then((payload) => {
        updateHeaders({ Auth: payload.token });
        dispatch({ type: SESSION_LOGIN, payload });
        try {
          localStorage.setItem('newUser', payload.new)
          localStorage.setItem('token', payload.token);
        } catch (e) {
          alert(
            'Unable to preserve session, probably due to being in private ' +
            'browsing mode.'
          );
        }
        done(payload);
      })
      .catch((payload) => {
        dispatch({
          type: SESSION_LOGIN,
          error: true,
          payload: {
            statusCode: payload.status,
            message: payload.statusText
          }
        })
      })
  );
}


export function requestAppointment(event, cb) {
  return (dispatch) => {
    postAppointmentRequest(event)
      .then((payload) => {
        cb(payload);
      })
  }
}

export function acceptAppointment(uuid, timeslot, token, cb) {
  return (dispatch) => {
    postAppointmentAccept(uuid, timeslot, token)
      .then((payload) => {
        cb(payload);
      })
  };
}

export function getUserApptList(token, cb, isCallback=true) {
  return dispatch => (
      getApptRequestList(token)
      .then((payload) => {
        if (isCallback) {
          cb(payload);          
        }

        dispatch({
          type: USER_APPT_LIST,
          payload
        })
      })
  );
}

export function getUserMapApptList(token, cb, isCallback=true) {
    return dispatch => (
        getApptMapList(token)
            .then((payload) => {
                if (isCallback) {
                    cb(payload);
                }

                dispatch({
                    type: USER_MAP_APPT_LIST,
                    payload
                })
            })
    );
}

export function getApptList(token) {
  return dispatch => (
      getApptListAPI(token)
      .then((payload) => {
        dispatch({
          type: APPT_LIST,
          payload
        })
      })
  );
}

export function setAppointment(isSetting) {
  return (dispatch) => {
    dispatch({
      type: SET_APPOINTMENT,
      payload: isSetting
    })
  }
}