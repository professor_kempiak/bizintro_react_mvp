import { TOAST_ADD, TOASTS_CLEAR } from '../actions';

export function addToast(message, status) {
  return (dispatch) => {
    let toast = {
      message: message,
      status: status
    }
    dispatch({ type: TOAST_ADD, payload: toast });
  }
}

export function clearToasts() {
  return (dispatch) => {
    dispatch({ type: TOASTS_CLEAR });
  }
}
