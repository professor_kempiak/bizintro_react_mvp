import { CALENDAR_DATE_SET, CALENDAR_VIEW_SET, CALENDAR_EVENT_ADD ,CALENDAR_EVENT_REMOVE, CALENDAR_EVENT_GET} from '../actions';
import { getApiCalendarEvents } from '../api/calendar';

export function setCalendarDate(date) {
  return (dispatch) => {
    dispatch({ type: CALENDAR_DATE_SET, payload: date })
  }
}

export function addCalendarEvent(event) {
  return (dispatch) => {
    dispatch({ type: CALENDAR_EVENT_ADD, payload: event });
  }
}
export function removeCalendarEvent(event) {
    return (dispatch) => {
        dispatch({ type: CALENDAR_EVENT_REMOVE, payload: event });
    }
}
export function setCalendarView(view) {
  return (dispatch) => {
    dispatch({ type: CALENDAR_VIEW_SET, payload: view });
  }
}

export function getCalendarEvents(token) {
  return (dispatch) => {
    getApiCalendarEvents(token)
    .then((payload) => {
      let events = JSON.parse(payload).map(e => {
        return {
          'title': e.fields.title,
          'start': new Date(e.fields.start_date_time),
          'end': new Date(e.fields.end_date_time),
          'allDay': false, 
          'new_event': false,
          'google': true
        }
      })
      dispatch({ type: CALENDAR_EVENT_GET, payload: events})
    })
  }
}