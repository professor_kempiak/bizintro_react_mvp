import { CONTACTS_LOAD, CONTACTS_EMPTY, CONTACTS_SEARCH_ADD, CONTACTS_INTRODUCED, CONTACT_EDIT, CONTACT_SELECT, CONTACT_ADD, CONTACT_SELECT_CLEAR, CONTACT_SELECT_PRIMARY, CONTACT_SELECT_SECONDARY, CONTACT_SELECT_REMOVE, CONTACTS_LOADING, UPDATE_INVITE_CONTACT, UPDATE_RANK_CONTACT } from '../actions';
import { createContact, loadContact, loadContacts, makeIntroductions, saveContact, postContactList, postContactSearch, inviteToFillOutProfile, updateRankContact } from '../api/contacts';

export function addContact(contact, token, callback) {
  return (dispatch) => {
    createContact(contact, token)
      .then((payload) => {
        if (payload.contact_id) {
          let localContact = {
            id: payload.contact_id,
            first_name: contact.first_name,
            last_name: contact.last_name,
            phone_number: contact.mobile,
            phone_numbers: [contact.mobile],
            address: contact.address,
            addresses: [contact.address],
            email: contact.email,
            emails: [contact.email],
            occupation: contact.title && contact.company ? contact.title + ' at ' + contact.company : '',
            bio: contact.bio,
            tags: contact.tags.split(','),
            avatar_link: 'https://www.gravatar.com/avatar/90448b6f4474548afd71cddb0d74865e?s=80&d=mm'
          }
          dispatch({ type: CONTACT_ADD, payload: localContact });
          callback(payload);
        } else {
          callback(payload);
        }
      })
  }
}

export function editContact(contact, token, callback) {
  return (dispatch) => {
    saveContact(contact, token)
      .then((payload) => {
        let localContact = {
          id: contact.id,
          first_name: contact.first_name,
          last_name: contact.last_name,
          phone_number: contact.primary_phone_number,
          phone_numbers: [contact.primary_phone_number],
          address: contact.primary_address,
          addresses: [contact.primary_address],
          email: contact.primary_email,
          emails: [contact.primary_email],
          occupation: contact.title && contact.company ? contact.title + ' at ' + contact.company : '',
          bio: contact.bio,
          tags: contact.tags,
          avatar_link: 'https://www.gravatar.com/avatar/90448b6f4474548afd71cddb0d74865e?s=80&d=mm'
        }
        if (payload.success) {
          dispatch({ type: CONTACT_EDIT, payload: localContact });
          callback(payload);
        } else {
          callback(payload);
        }
      })
  }
}

export function getContact(id, token, callback) {
  return (dispatch) => {
    loadContact(id, token)
      .then((payload) => {
        callback(JSON.parse(payload));
      })
  }
}

export function gatherContacts(token, page, callback) {
  return (dispatch) => {
    loadContacts(token, page)
      .then((payload) => {
        if (Array.isArray(payload)) {
          dispatch({ type: CONTACTS_LOAD, payload });
          callback(payload);
        } else {
          callback(payload);
        }
      });
  };
}

export function selectContact(contact, selected) {
  return (dispatch) => {
    if (selected.primary.length < 1) {
      dispatch({
        type: CONTACT_SELECT_PRIMARY, payload: contact
      });
    } else {
      dispatch({
        type: CONTACT_SELECT_SECONDARY, payload: contact
      });
    }
  };
}

export function deselectContact(contact) {
  return (dispatch) => {
    dispatch({
      type: CONTACT_SELECT_REMOVE, payload: contact
    });
  };
}

export function sendNewIntroductions(contact_a, contact_b, msg_a, msg_b, token, callback) {
  return (dispatch) => {
    makeIntroductions(contact_a, contact_b, msg_a, msg_b, token)
      .then((payload) => {
        dispatch({
          type: CONTACTS_INTRODUCED, payload
        });
        callback();
      })
  };
}

export function clearSelectedContacts(cb) {
  return (dispatch) => {
    dispatch({
      type: CONTACT_SELECT_CLEAR
    });
    cb();
  }
}

export function uploadContactList(formData, token, cb) {
  return (dispatch) => {
    postContactList(formData)
      .then((payload) => {
        cb(JSON.parse(payload));
      });
  }
}

export function remoteContactSearch(tags, page, token,  cb, rank=false) {
  let searchTags = tags.filter((tag) => tag !== "");

  return (dispatch) => {
    if (page == 1){
      dispatch({ type: CONTACTS_EMPTY });
    }
    dispatch({ type: CONTACTS_LOADING, payload: true });
    postContactSearch(searchTags, page, token, rank)
      .then((payload) => {
        let responseContacts = payload;
        dispatch({ type: CONTACTS_LOADING, payload: false });
        dispatch({ type: CONTACTS_LOAD, payload: responseContacts });
        cb(payload)
      });
  }
}

export function inviteToFillProfile(id, token, cb) {
  return (dispatch) => {
    inviteToFillOutProfile(id, token)
    .then((payload) => {
      cb(payload);
    })
  }
}

export function updateInviteContact(contact) {
  return (dispatch) => {
    dispatch({type: UPDATE_INVITE_CONTACT, payload: contact});
  }
}

export function saveRankContacts(rankArray, token, cb) {
  return (dispatch) => {
    updateRankContact(rankArray, token)
    .then((payload) => {
      dispatch({type: UPDATE_RANK_CONTACT, payload: rankArray})
      cb(payload)
    })
  }
}