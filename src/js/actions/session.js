import { SESSION_LOAD, SESSION_LOGIN, SESSION_LOGOUT } from '../actions';
import { postSession, postGoogleLogin, postUserRegistration, verifyUserToken, updateProfile } from '../api/session';
import { updateHeaders } from '../api/utils';

const localStorage = window.localStorage;

export function initialize() {
  return (dispatch) => {
    const { email, name, token } = localStorage;
    if (email && token) {
      dispatch({
        type: SESSION_LOAD, payload: { email, name, token }
      });
      if (localStorage.getItem('onlyProfile') == '1' && !window.location.href.includes('update_contact')) {
        window.location = '/update_contact';  
      }
    } else {
      window.location = '/login';
    }
  };
}

export function registerUser(user, cb) {
  return (dispatch) => {
    postUserRegistration(user)
      .then((payload) => {
        cb(payload);
      })
  }
}

export function login(email, password, done) {
  return (dispatch) => {
    removeCache();
    postSession(email, password)
      .then((payload) => {
        updateHeaders({ Auth: payload.token });
        dispatch({ type: SESSION_LOGIN, payload });
        try {
          localStorage.email = payload.email;
          localStorage.name = payload.name;
          localStorage.token = payload.token;
        } catch (e) {
          alert(
            'Unable to preserve session, probably due to being in private ' +
            'browsing mode.'
          );
        }
        done();
      })
      .catch(payload => dispatch({
        type: SESSION_LOGIN,
        error: true,
        payload: {
          statusCode: payload.status, message: payload.statusText
        }
      }))
  };
}

export function loginWithGoogle(code, callback) {
  return (dispatch) => {
    removeCache();
    postGoogleLogin(code)
      .then((payload) => {
        if (payload.status) {
          localStorage.email = payload.data.email;
          localStorage.name = payload.data.first_name;
          localStorage.token = payload.data.token;
          localStorage.setItem('loggedBy', 'google');
          updateHeaders({ Auth: payload.data.token });
          dispatch({ type: SESSION_LOGIN, payload: payload.data });
          callback(payload);
        } else {
          callback(payload);
        }
      })
      .catch((payload) => {
        dispatch({
          type: SESSION_LOGIN,
          error: true,
          payload: {
            statusCode: payload.status,
            message: payload.statusText
          }
        });
      })
  }
}

export function logout() {
  return (dispatch) => {
    dispatch({ type: SESSION_LOGOUT });
    updateHeaders({ Auth: undefined });
    try {
      removeCache()
    } catch (e) {
      // ignore
    }
    window.location.href = '/login'; // reload fully
  };
}

export function verifyToken(id, token, cb) {
  return (dispatch) => {
    verifyUserToken(id, token)
    .then(payload => {
      cb(JSON.parse(payload));
    })
  }
}

export function updateUserProfile(user, token, cb) {
  return (dispatch) => {
    updateProfile(user, token)
    .then(payload => {
      // dispatch({type: UPDATE_INVITE_USER, payload: payload.user});
      cb(payload)
    })
  }
}

function removeCache() {
  localStorage.removeItem('firstname');
  localStorage.removeItem('lastname');
  localStorage.removeItem('timeslots_available');
  localStorage.removeItem('email');
  localStorage.removeItem('name');
  localStorage.removeItem('token');
  localStorage.removeItem('loggedBy')
  localStorage.removeItem('newUser');
  localStorage.removeItem('username');
  localStorage.removeItem('uuid');
  localStorage.removeItem('time_slot');
  sessionStorage.removeItem('intro_uuid');
  sessionStorage.removeItem('contact_id');
  sessionStorage.removeItem('recipient_email');
  localStorage.removeItem('onlyProfile');
}