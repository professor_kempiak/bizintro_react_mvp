import { INTRODUCTION_LIST, INTRODUCTION_STATUS, INTRODUCTIONS_ADD, TOAST_ADD } from '../actions';
import { getIntroductionList, getIntroductionStatus, nudgeIntroduction, archiveIntroduction, getIntroductionsByPage } from '../api/introductions';

export function getUserIntroductionList(done) {
  const getToken = localStorage.token;
  return dispatch => (
    getIntroductionList(getToken)
      .then((payload) => {
        dispatch({ type: INTRODUCTION_LIST, payload });
        done(payload);
      })
      .catch(payload => dispatch({
        type: INTRODUCTION_LIST,
        error: true,
        payload: {
          statusCode: payload.status,
          message: payload.statusText
        }
      }))
  );
}


export function getUserIntroductionStatus(token, cb) {
  return (dispatch) => {
    getIntroductionStatus(token)
      .then((payload) => {
        if (!Array.isArray(payload)) {
          let toast = {
            message: payload.detail,
            status: 'critical'
          }
          cb(payload);
          return dispatch({ type: TOAST_ADD, payload: toast });
        }
        dispatch({ type: INTRODUCTION_STATUS, payload });
        cb(payload);
      })
  }
}

export function getIntroductions(token, page, perpage, cb) {
  return (dispatch) => {
    getIntroductionsByPage(token, page, perpage)
      .then((payload) => {
        if (!Array.isArray(payload)) {
          let toast = {
            message: payload.detail,
            status: 'critical'
          }
          dispatch({ type: TOAST_ADD, payload: toast });
          cb(payload);
        }
        dispatch({ type: INTRODUCTIONS_ADD, payload });
        cb(payload);
      });
  }
}


export function postIntroductionNudge(uuid, token, cb) {
  return dispatch => (
    nudgeIntroduction(uuid, token)
      .then((payload) => {
        cb(payload);
      })
  );
}

export function postIntroductionArchive(id, token, cb) {
  return dispatch => {
    archiveIntroduction(id, token)
      .then(payload => {
        cb(payload);
      })
  }
}