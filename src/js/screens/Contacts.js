import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Article, Box, Toast, Notification } from 'grommet';

import HeaderBar from '../components/HeaderBar';
import ContactList from '../components/ContactList';
import AddContactLayer from '../components/AddContactLayer';
import EditContactLayer from '../components/EditContactLayer';
import UploadContactsLayer from '../components/UploadContactsLayer';
import MakeIntroductionLayer from '../components/MakeIntroductionLayer';
import ContactHeder from '../components/ContactHeader';

import { initialize } from '../actions/session';
import { headers, parseJSON } from '../api/utils';
import { gatherContacts } from '../actions/contacts';
import { loadTemplates } from '../actions/templates';
import { updateSearchTags } from '../actions/search';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Repeat from '@material-ui/icons/Repeat'
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

const variantIcon = {
    error: ErrorIcon,
};

const styles1 = theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing.unit,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
});

function MySnackbarContent(props) {
    const { classes, className, message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={classNames(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
                </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
            {...other}
        />
    );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

class Contacts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLayer: false,
            layer: null,
            toasts: null,
            notifications: null,
            page: 1,
            pageLoaded: false,
            end_of_pages: false
        }

        this._toggleLayer = this._toggleLayer.bind(this);
        this._gatherTemplates = this._gatherTemplates.bind(this);
        this._gatherMoreContacts = this._gatherMoreContacts.bind(this);
        this._prepAddContactLayer = this._prepAddContactLayer.bind(this);
        this._prepEditContactLayer = this._prepEditContactLayer.bind(this);
        this._allowMakeIntroduction = this._allowMakeIntroduction.bind(this);
        this._prepUploadContactsLayer = this._prepUploadContactsLayer.bind(this);
        this._prepMakeIntroductionLayer = this._prepMakeIntroductionLayer.bind(this);
    }

    _gatherContacts(page) {
        const { dispatch, session } = this.props;
        // console.log("contacts gather contacts");
        dispatch(gatherContacts(localStorage.token, page, (response) => {
            if (!Array.isArray(response)) {
                this.setState({
                    notifications: response.detail
                });
            }

            if (response.length == 0) {
                this.setState({
                    toasts: <Toast status='warning' >Your contacts are loading from your account. Please be patient.</Toast>
                });
            }

            this.setState({ pageLoaded: true });
        }));
    }

    _gatherMoreContacts() {
        const { dispatch } = this.props;
        if (!this.state.end_of_pages && this.state.pageLoaded) {
            let page = this.state.page + 1;
            dispatch(gatherContacts(localStorage.token, page, (response) => {
                if (response.length === 0) {
                    this.setState({ end_of_pages: true });
                } else {
                    this.setState({ page: page });
                }
            }));
        }
    }

    _gatherTemplates() {
        this.props.dispatch(loadTemplates(localStorage.getItem('token')));
    }

    _prepMakeIntroductionLayer() {
        this.setState({
            layer: <MakeIntroductionLayer
                contacts={this.props.contacts}
                onClose={this._toggleLayer} />
        })
        this._toggleLayer();
    }

    _prepAddContactLayer() {
        this.setState({
            layer: <AddContactLayer
                onClose={this._toggleLayer} />
        })
        this._toggleLayer();
    }

    _prepEditContactLayer(contact, e) {
        this.setState({
            layer: <EditContactLayer
                contact={contact}
                showShareButton={true}
                onClose={this._toggleLayer} />
        })
        this._toggleLayer();
    }

    _prepUploadContactsLayer() {
        this.setState({
            layer: <UploadContactsLayer
                onClose={this._toggleLayer} />
        })
        this._toggleLayer();
    }

    _toggleLayer() {
        this.setState({ showLayer: !this.state.showLayer });
        if (this.state.showLayer) {
            const { dispatch } = this.props;
            // const tags = [];
            // dispatch(updateSearchTags(tags));
        }
    }

    _allowMakeIntroduction() {
        let { contacts } = this.props;
        return contacts.selected.primary.length >= 1 && contacts.selected.secondary.length >= 1;
    }

    componentDidMount() {
        this.props.dispatch(initialize());
        this._gatherContacts(this.state.page);
        this._gatherTemplates();
    }

    render() {
        const { contacts, classes } = this.props;
        let contactHeder = '';
        if (contacts.selected.primary.length > 0) {
            contactHeder = (<div className={'contactHederWrapper'}>
                <div>
                    <ContactHeder makeIntroducntionLayer={this._prepMakeIntroductionLayer} />
                </div>
            </div>);
        }
        return (
            <div className = {'contactList pageContent'}>
                {this.state.toasts}
                <HeaderBar />
                <div className={'contentWrapper'} >
                    {/* <Button icon={<UserAddIcon />}
                            label='Add New Contact'
                            onClick={this._prepAddContactLayer}
                            primary={true} /> */}
                    <Button
                            variant="contained"
                            size="large"
                            color="primary"
                            className={classes.button}
                            onClick={this._prepAddContactLayer}
                    >
                        <AccountCircle />
                        Add New Contact
                    </Button>
                    {/* <Button icon={<TransactionIcon />}
                            label='Make Introduction'
                            onClick={this._allowMakeIntroduction() ? this._prepMakeIntroductionLayer : null}
                            primary={true} /> */}
                    <Button
                        variant="contained"
                        size="large"
                        color="primary"
                        className={classes.button}
                        onClick={this._allowMakeIntroduction() ? this._prepMakeIntroductionLayer : null}
                    >
                        <Repeat />
                        Make Introduction
                    </Button>
                    {/* <Button icon={<TransactionIcon />}
                            label='Upload Contacts'
                            onClick={this._prepUploadContactsLayer}
                            primary={true} /> */}
                    <Button
                        variant="contained"
                        size="large"
                        color="primary"
                        className={classes.button}
                        onClick={this._prepUploadContactsLayer}
                    >
                        <Repeat />
                        Upload Contacts
                    </Button>
                </div>
                { contactHeder }
                {this.state.notifications ? <div>
                    <Snackbar
                        open={true}
                        // onClose={this.handleClose}
                        className={classes.snackbar}
                        autoHideDuration={2000}
                    >
                        <MySnackbarContentWrapper
                            // onClose={this.handleClose}
                            variant="error"
                            message={this.state.notifications}
                        />
                    </Snackbar>
                </div> : null}
                <div className={'contactHederWrapper'}>
                    <div>
                        <ContactList
                            search={this.props.search}
                            addContact={this._prepAddContactLayer}
                            editContact={this._prepEditContactLayer}
                            makeIntroducntionLayer={this._prepMakeIntroductionLayer}
                            onMore={this.state.end_of_pages || this.props.search.length > 0 ? null : this._gatherMoreContacts} />
                        {this.state.showLayer ? this.state.layer : null}
                    </div>
                </div>
            </div>
        );
    }
}

const select = state => ({
    contacts: state.contacts,
    session: state.session,
    search: state.search.tags
});

export default withStyles(styles)(connect(select)(Contacts));