import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import HeaderBar from '../components/HeaderBar';
import ReactMapboxGl, { Layer, Feature, GeoJSONLayer, Marker, Cluster } from "react-mapbox-gl";
import DetailAppointmentLayer from '../components/DetailAppointmentLayer';
import { getApptList } from "../actions/appointments";
import CustomMapTimeZone from '../components/CustomMapTimeZone';
import CustomMapTimeBuffer from '../components/CustomMapTimeBuffer';
import CustomMapPlainingMeeting from '../components/CustomMapPlainingMeeting';
import CustomMapDrawBoundry from '../components/CustomMapDrawBoundry';
import CustomMapCompleteStep from '../components/CustomMapCompleteStep';
import { getUserApptList } from "../actions/appointments";
import { getZones, editZone, removeZone } from "../actions/zones";
import DrawControl from 'react-mapbox-gl-draw';
import { addZone } from '../actions/zones';
import { addToast } from '../actions/toasts';

import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';

import ScheduleOtherAppointment from '../components/scheduleOtherAppointment';
import moment from 'moment';

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiamlhbmcyMDIwIiwiYSI6ImNqZWdzNHRrMDF5NHIyeG1rMzVkbWIwZ3AifQ.57jhLiNkcAccrUij-EMt-Q"
});

const MapMarker = <Image src='/img/carousel-1.png' />;


const appointmentData = [
    { lat: 41.52916347, long: -87.5625333, day: "Monday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.49960695, long: -87.4015279, day: "Tuesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.52128377, long: -87.5625333, day: "Wednesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.53005939, long: -87.54981, day: "Thursday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.49313, long: -87.9548136, day: "Friday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.51811784, long: -87.957233, day: "Saturday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.53430039, long: -87.012486, day: "Sunday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.5073853, long: -87.320058, day: "Wednesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.50597426, long: -87.152339, day: "Monday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
    { lat: 41.52395143, long: -87.4165897, day: "Tuesday", title: "Meeting About VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." },
]

const dayWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

const InitialUserPostion = [-87.5625333, 41.726845];

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    select: {
        marginTop: theme.spacing.unit * 2,
    },
});

class AppointmentMap extends Component {
    constructor(props) {
        super(props);

        let drawControl;

        this.state = {
            mapCenter: InitialUserPostion,
            showLayer: false,
            layer: null,
            otherPikerLayer: null,
            showFormLayer: false,
            formLayer: null,
            selectDay: "All",
            appointments: [],
            zone_id: false,
            step: 1,
            showZoneToolbar: false,
            newZoneSaved: false,
            newZoneName: '',
            newZoneTimeBuffer: 15,
            newZoneMeetingPlanning: {
                Monday: { selected: false, recurring: false },
                Tuesday: { selected: false, recurring: false },
                Wednesday: { selected: false, recurring: false },
                Thursday: { selected: false, recurring: false },
                Friday: { selected: false, recurring: false },
                Saturday: { selected: false, recurring: false },
                Sunday: { selected: false, recurring: false }
            },
            newZoneGeometry: {},
            polygons: this.props.multiPolyZone,
            index: 0,
            polygonID: '',
            polygonColor: '#73d8ff',
            dateTimePicker: false,
            isGetGeoData: true,

        }

        this.preStep = this.preStep.bind(this);
        this.nextStep = this.nextStep.bind(this);
        this.anotherZone = this.anotherZone.bind(this);
        this.setZoneState = this.setZoneState.bind(this);
        this.setColor = this.setColor.bind(this);
        this.updateDay = this.updateDay.bind(this);
        this.getMiddleValue = this.getMiddleValue.bind(this);
        this._toggleLayer = this._toggleLayer.bind(this);
        this._toggleOtherPcikerLayer = this._toggleOtherPcikerLayer.bind(this);
        this._saveNewZone = this._saveNewZone.bind(this);
        this._deleteZone = this._deleteZone.bind(this);
        this.updateBoundaries = this.updateBoundaries.bind(this);
        this._zoneCreateLayer = this._zoneCreateLayer.bind(this);
        this._toggleFormLayer = this._toggleFormLayer.bind(this);
        this.createFormContent = this.createFormContent.bind(this);
        this._handleInputChange = this._handleInputChange.bind(this);
        this._saveZoneBoundaries = this._saveZoneBoundaries.bind(this);
        this._activateZoneToolbar = this._activateZoneToolbar.bind(this);
        this._handleRadioOrCheckboxChange = this._handleRadioOrCheckboxChange.bind(this);
        this._verifyAndRenderCurrentBoundaries = this._verifyAndRenderCurrentBoundaries.bind(this);
        this.getLatLongData = this.getLatLongData.bind(this);
        this._updateDayByPicker = this._updateDayByPicker.bind(this);

        // this.oppointments = appointmentData;
        this.oppointments = this.state.appointments;
    }

    componentWillReceiveProps(newProps) {
        if (newProps.apptList !== this.props.apptList) {
            this.getLatLongData(newProps.apptList)
                .then(list => {
                    this.oppointments = list;
                    this.setState({ appointments: list });
                });
        }
    }

    componentDidMount() {
        let { dispatch, apptList } = this.props;
        if (apptList.length !== 0) {
            this.getLatLongData(apptList)
                .then(list => {
                    this.oppointments = list;
                    this.setState({ appointments: list });
                });
        } else {
            dispatch(getApptList(localStorage.token));
        }
        dispatch(getZones(localStorage.getItem('token')));

        navigator.geolocation.getCurrentPosition(
            ({ coords }) => {
                const { latitude, longitude } = coords;

                this.setState({
                    mapCenter: [longitude, latitude],
                });
            },
            err => {
                console.error('Cannot retrieve your current position', err);
            }
        );
    }

    setZoneState(map, zone) {
        this.setState({ showZoneToolbar: true });
        var test = map.map.queryRenderedFeatures(map.point);
        let boundaries = this.drawControl.draw.add(test[0]);
        this.drawControl.draw.changeMode('direct_select', { featureId: boundaries[0] });
        this.setState({
            zone_id: zone.id,
            step: 1,
            newZoneSaved: false,
            newZoneName: zone.name,
            newZoneTimeBuffer: zone.time_buffer,
            newZoneMeetingPlanning: zone.weekdays,
            newZoneGeometry: zone.coordinates,
            showFormLayer: true,
            poylgonID: zone.poylgon_id,
            polygonColor: zone.color
        });
    }

    resetZoneState() {
        this.setState({
            zone_id: false,
            step: 1,
            showZoneToolbar: false,
            newZoneSaved: false,
            newZoneName: '',
            newZoneTimeBuffer: 15,
            newZoneMeetingPlanning: {
                Monday: { selected: false, recurring: false },
                Tuesday: { selected: false, recurring: false },
                Wednesday: { selected: false, recurring: false },
                Thursday: { selected: false, recurring: false },
                Friday: { selected: false, recurring: false },
                Saturday: { selected: false, recurring: false },
                Sunday: { selected: false, recurring: false }
            },
            newZoneGeometry: {},
            polygonID: '',
            polygonColor: '#73d8ff',
        });
    }

    getMiddleValue(value) {
        var data;
        var lat = 0, long = 0, counter = 0;
        for (let i = 0; i < value[0].length; i++ , counter++) {
            lat += value[0][i][0];
            long += value[0][i][1];
        }
        data = [lat / counter, long / counter];
        return data;
    }

    getLatLongData(apptList) {
        let geocoder = new google.maps.Geocoder(),
            _apptList = [],
            nonGeoCount = 0;

        _apptList = _apptList.filter((appt) => {
            return appt.fields.status == 'c';
        });

        return new Promise((resolve, reject) => {
            apptList.map((appt) => {
                let address = appt.fields.locailized_address;
                geocoder.geocode({ 'address': address.address }, (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        let latitude = results[0].geometry.location.lat();
                        let longitude = results[0].geometry.location.lng();
                        let attendee = appt.fields.user_profile.first_name + ' ' + appt.fields.user_profile.last_name + ' & ' + appt.fields.recipient_user_profile.first_name + ' ' + appt.fields.recipient_user_profile.last_name
                        _apptList.push({
                            'lat': latitude,
                            'long': longitude,
                            'day': dayWeek[(new Date(appt.fields.start_date_time)).getDay()],
                            'title': appt.fields.title,
                            'attendees': attendee,
                            'datetime': (new Date(appt.fields.start_date_time)).toDateString(),
                            'location': address.address,
                            'notes': appt.fields.comments
                        });
                        if (_apptList.length == (apptList.length - nonGeoCount)) {
                            resolve(_apptList);
                        }
                    }
                    else {
                        nonGeoCount ++;
                        if (_apptList.length == (apptList.length - nonGeoCount)) {
                            resolve(_apptList);
                        }
                    }
                });
            });
        })
    }
    updateGeoData(apptList){
        if(this.state.isGetGeoData) {
            this.getLatLongData(apptList)
                .then(list => {
                    this.oppointments = list;
                    this.setState({appointments: list,isGetGeoData: false});
                });
        }
    }
    _toggleOtherPcikerLayer() {
        this.setState({ dateTimePicker: !this.state.dateTimePicker });
    }
    _updateDayByPicker(starDate, endDate){
        let appointmentData = this.state.appointments
        this.oppointments = appointmentData.filter(function (appt) {
            return (moment(starDate) < moment(appointmentData[0].datetime)) && (moment(endDate) > moment(appointmentData[0].datetime));
        })
        this.setState({
            dateTimePicker: false,
        });
    }
    updateDay(newDay) {
        if (newDay.target.value === 'All') {
            //this is hardcoded, need to data from API
            // this.oppointments = appointmentData;
            this.oppointments = this.state.appointments
        } else if (newDay.target.value === 'Other') {
            this.setState({
                otherPikerLayer: <ScheduleOtherAppointment
                    updateDay={this._updateDayByPicker}
                    onClose={this._toggleOtherPcikerLayer} />,
            });
            this._toggleOtherPcikerLayer();
        } else {
            let appointmentData = this.state.appointments
            this.oppointments = appointmentData.filter(function (appt) {
                return appt.day == newDay.target.value;
            })
        }
        if (newDay.target.value !== 'Other') {
            this.setState({
                selectDay: newDay.target.value,
                dateTimePicker: false,
            });
        }
    }

    _prepDetailAppointmentLayer(appointment) {
        this.setState({
            layer: <DetailAppointmentLayer
                appointment={appointment}
                onClose={this._toggleLayer} />
        })
        this._toggleLayer();
    }

    _toggleLayer() {
        this.setState({ showLayer: !this.state.showLayer });
    }


    nextStep() {
        if (this.state.step === 4) {
            if (this.state.zone_id) {
                this._saveZoneBoundaries(() => {
                    this.updateBoundaries();
                });
            } else {
                if (!this._saveZoneBoundaries()) {
                    return false;
                }
            }
        }

        if (this.state.step < 5) {
            this.setState({
                step: this.state.step + 1
            });
        }
    }

    preStep() {
        if (this.state.step > 1) {
            this.setState({
                step: this.state.step - 1
            });
        }
    }

    setColor(color) {
        const temp = color.hex;

        this.setState({
            polygonColor: temp,
        });
    }

    updateBoundaries() {
        if (this.state.zone_id) {
            let zone = {
                id: this.state.zone_id,
                name: this.state.newZoneName,
                time_buffer: this.state.newZoneTimeBuffer,
                weekdays: this.state.newZoneMeetingPlanning,
                coordinates: this.state.newZoneGeometry,
                color: this.state.polygonColor,
                polygon_id: this.state.polygonID
            }

            this.props.dispatch(editZone(zone, localStorage.getItem('token')));
        }
        this.setState({ newZoneSaved: true });
    }

    anotherZone() {
        if (this.state.step > 1) {
            this.resetZoneState();
        }
    }

    createFormContent() {
        // console.log(this.state.step);
    }

    _zoneCreateLayer() {
        this.resetZoneState();
        this._toggleFormLayer();
    }

    _toggleFormLayer() {
        this.setState({ showFormLayer: !this.state.showFormLayer });
    }

    _handleInputChange(e) {
        let name = e.target.name;
        let type = e.target.type;
        let value = e.target.value;

        this.setState({
            [name]: value
        });
    }

    _handleRadioOrCheckboxChange(e) {
        this.setState({
            newZoneSaved: false,
        })
        let name = e.target.name;
        let nameSplit = name.split('-');
        let dayOfWeek = nameSplit[0];
        let type = nameSplit[1];
        let stateCopy = Object.assign({}, this.state.newZoneMeetingPlanning);

        stateCopy[dayOfWeek][type] = !stateCopy[dayOfWeek][type];

        this.setState({
            newZoneMeetingPlanning: stateCopy
        });

    }

    _activateZoneToolbar() {
        this.setState({
            showZoneToolbar: true
        });
    }

    _verifyAndRenderCurrentBoundaries() {
        let zoneGeometry = this.state.newZoneGeometry;
        let keys = Object.keys(zoneGeometry);

        if (keys.length > 0) {
        }

    }

    _saveZoneBoundaries(callback = () => { }) {
        let boundaries = this.drawControl.draw.getSelected();
        let geometry = [];

        if (boundaries.features.length > 0) {
            geometry = boundaries.features[0].geometry.coordinates;
            this.setState({
                newZoneGeometry: geometry
            }, () => {
                callback();
            });
            return true;
        } else {
            if (this.state.newZoneGeometry.length > 0) {
                return true;
            } else {
                this.props.dispatch(addToast('Must enter zone boundaries to continue', 'critical'));
            }
        }
        return false;
    }

    _saveNewZone() {
        if (!this.state.zone_id) {
            let zone = {
                id: Date.now(),
                name: this.state.newZoneName,
                time_buffer: this.state.newZoneTimeBuffer,
                weekdays: this.state.newZoneMeetingPlanning,
                coordinates: this.state.newZoneGeometry,
                color: this.state.polygonColor,
                polygon_id: this.state.polygonID
            }
            this.props.dispatch(addZone(zone, localStorage.getItem('token')));
        }
        this.setState({ newZoneSaved: true });
    }

    _deleteZone() {
        let zone_id = this.state.zone_id;
        let token = localStorage.getItem('token');
        this.props.dispatch(removeZone(zone_id, token));
        this.setState({ showFormLayer: false });
        this.resetZoneState();
    }



    render() { 
        if(this.props.apptList.length > 0){
            this.updateGeoData(this.props.apptList);
        }
        let stepContent = '';

        if (this.state.step == 1) {
            stepContent = <CustomMapTimeZone zone_id={this.state.zone_id} deleteZone={this._deleteZone} handleInputChange={this._handleInputChange} value={this.state.newZoneName} nextStep={this.nextStep} preStep={this.preStep} />;
        }
        else if (this.state.step == 2) {
            stepContent = <CustomMapTimeBuffer polygonColor={this.state.polygonColor} setColor={this.setColor} handleInputChange={this._handleInputChange} value={this.state.newZoneTimeBuffer} nextStep={this.nextStep} preStep={this.preStep} zone_name={this.state.newZoneName} />;
        }
        else if (this.state.step == 3) {
            stepContent = <CustomMapPlainingMeeting handleRadioOrCheckboxChange={this._handleRadioOrCheckboxChange} values={this.state.newZoneMeetingPlanning} nextStep={this.nextStep} preStep={this.preStep} zone_name={this.state.newZoneName} />;
        }
        else if (this.state.step == 4) {
            stepContent = <CustomMapDrawBoundry zone_id={this.state.zone_id} activateZoneToolbar={this._activateZoneToolbar} verifyAndRenderCurrentBoundaries={this._verifyAndRenderCurrentBoundaries} saveBoundaries={this._saveZoneBoundaries} updateBoundaries={this.updateBoundaries} nextStep={this.nextStep} preStep={this.preStep} />;
        }
        else if (this.state.step >= 5) {
            stepContent = <CustomMapCompleteStep saveNewZone={this._saveNewZone} anotherZone={this.anotherZone} preStep={this.preStep} />;
        }

        var markers = [];
        var noPoly = true;
        var noSymbolZones = true;
        var polyZones = this.props.multiPolyZone.map((zone, index) => {
            const color = {
                'fill-color': zone.color,
                'fill-opacity': 0.5,
                'fill-outline-color': zone.color,
            };
            return (
                <Layer type="fill" paint={color} id={index.toString() + "-zonelayer"} key={index.toString() + "-zone"} className="polygons" >
                    <Feature coordinates={zone.coordinates} onClick={(map) => this.setZoneState(map, zone)} />
                </Layer>
            );
        });

        var symbolZones = this.props.multiPolyZone.map((zone, index) => {
            const center = this.getMiddleValue(zone.coordinates);
            const text = zone.name;
            const layoutLayer = {
                'text-field': text,
                'text-size': 12,
            }
            return (
                <Layer type="symbol" layout={layoutLayer} key={index.toString() + "-label"} className="poly-labels" >
                    <Feature coordinates={center} />
                </Layer>
            );
        });

        if (polyZones.length > 0) {
            noPoly = false;
            noSymbolZones = false;
        }
        const { classes } = this.props;

        return (
            <div className={this.props.showHeader ? 'scheduleTabContent' : 'scheduleTabContent pageContent'}>
                {this.props.showHeader ? null : <HeaderBar />}
                <div className={this.props.showHeader ? 'mapSelectDay1' : 'mapSelectDay'}>
                    <Select
                        value={this.state.selectDay}
                        onChange={this.updateDay}
                        name="day"
                        className={classes.select}
                        input={<Input name="day" id="day-helper" />}
                    >
                        <MenuItem value="All">All</MenuItem>
                        <MenuItem value="Monday">Monday</MenuItem>
                        <MenuItem value="Tuesday">Tuesday</MenuItem>
                        <MenuItem value="Wednesday">Wednesday</MenuItem>
                        <MenuItem value="Thursday">Thursday</MenuItem>
                        <MenuItem value="Friday">Friday</MenuItem>
                        <MenuItem value="Saturday">Saturday</MenuItem>
                        <MenuItem value="Sunday">Sunday</MenuItem>
                        <MenuItem value="Other">Other</MenuItem>
                    </Select>
                </div>

                {this.state.dateTimePicker ? this.state.otherPikerLayer : null}
                <div className={'mapWrapper'}>
                    <Map
                        style="mapbox://styles/mapbox/streets-v8"
                        center={this.state.mapCenter}
                        zoom={[9]}
                        onStyleLoad={(map) => {
                            map.loadImage('/img/mapmarker.png',function (error, image) {
                                if (error) throw error;
                                map.addImage('cat', image);
                            })
                        }}
                        containerStyle={{
                            height: "87vh"
                        }}>
                        <Layer
                            type="symbol"
                            id="marker"
                            layout={{ "icon-image": 'cat' }}>
                            {
                                this.oppointments.map((appt, index) => (
                                    <Feature
                                        key={index}
                                        onClick={() => this._prepDetailAppointmentLayer(appt)}
                                        coordinates={[appt.long, appt.lat]}
                                    />
                                ))
                            }
                        </Layer>
                        {noPoly ? null : polyZones}
                        {noSymbolZones ? null : symbolZones}
                        {this.state.showFormLayer ? stepContent : null}
                        {this.state.showZoneToolbar ? <DrawControl ref={(drawControl) => { this.drawControl = drawControl }} /> : null}
                        {/*<Button icon={<Pulse />} className={'bottomPulse'} onClick={this._zoneCreateLayer} />*/}
                    </Map>
                </div>
                {this.state.showLayer ? this.state.layer : null}
            </div>
        );
    }
}

const select = state => ({
    apptList: state.appointment.apptList,
    multiPolyZone: state.zones.list,
});

export default withStyles(styles)(connect(select)(AppointmentMap));