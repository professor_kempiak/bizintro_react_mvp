import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { initialize } from '../actions/session';
import HeaderBar from '../components/HeaderBar';
import AddCalendarEventLayer from '../components/AddCalendarEventLayer';
import CreateAppointmentRequestLayer from '../components/CreateAppointmentRequestLayer';
import CustomFullCalendar from '../components/CustomFullCalendar';
import { addCalendarEvent, getCalendarEvents } from '../actions/calendar';
import { requestAppointment, getApptList, setAppointment } from '../actions/appointments';
import { addToast } from '../actions/toasts';
import moment from 'moment';
import  CustomBigCalendar  from '../components/CustomBigCalendar';
import { loadTemplates } from '../actions/templates';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';  
import AddIcon from '@material-ui/icons/Add';
import Snackbar from '@material-ui/core/Snackbar';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

const variantIcon = {
  error: ErrorIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const localStorage = window.localStorage;

class AppointmentCalendar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      layer: null,
      notifications: null,
      hideHeaderBar: this.props.showHeader || false,
      isShowApptLayer: false,
      appointments:[],
      isSetting: true
      // isSetting: false
    };

    this._addEvents = this._addEvents.bind(this);
    this._toggleLayer = this._toggleLayer.bind(this);
    this._gatherTemplates = this._gatherTemplates.bind(this);
    this._createAppointment = this._createAppointment.bind(this);
    this._showAddCalendarEventLayer = this._showAddCalendarEventLayer.bind(this);
    this._verifyIntroductionInProgress = this._verifyIntroductionInProgress.bind(this);
    this._showCreateAppointmentRequestLayer = this._showCreateAppointmentRequestLayer.bind(this);
  }

  _verifyIntroductionInProgress() {
    if (sessionStorage.getItem('recipient_email') !== null) {
      this.setState({
        hideHeaderBar: this.props.showHeader ? true : false,
        notifications: `Select a date to continue sending appointment request to ${sessionStorage.recipient_email}`,
        isSetting: true
      })
      let { dispatch } = this.props;
      dispatch(setAppointment(true));
      return true;
    }
    return false;
  }

  _showCreateAppointmentRequestLayer() {
    this.setState({
      layer: <CreateAppointmentRequestLayer onClose={this._toggleLayer} onSave={this._createAppointment} />
    });
  }

  _showAddCalendarEventLayer(slotInfo, currentView, sendOrNot) {
    this._addEvents(slotInfo);
    
    if (sendOrNot) {
      this._showCreateAppointmentRequestLayer();
      this._toggleLayer();
    }
  }

  _toggleLayer() {
    this.setState({
      showLayer: !this.state.showLayer
    })
  }

  _closeNotification() {
    this.setState({
      notifications: null
    });
  }

  _addEvents(events) {
    let { dispatch } = this.props;
    let dates = Object.keys(events);

    let newEvents = dates.map((date) => {
      let newTimeSlots = events[date].map((timeslot) => {
        let split = timeslot.split('_');
        let start = moment(date + 'T' + split[0]);
        let end = moment(date + 'T' + split[1]);
        let event = {
          title: 'Appointment',
          start: new Date(start.year(), start.month(), start.date(), start.hour(), start.minute()),
          end: new Date(end.year(), end.month(), end.date(), end.hour(), end.minute()),
          allDay: false,
          new_event: true
        }
        dispatch(addCalendarEvent(event));
        return event;
      });
      return newTimeSlots;
    });
  }

  _createAppointment(event) {
    let { dispatch } = this.props;

    event.token = localStorage.token;
    event.intro_uuid = sessionStorage.intro_uuid;
    event.contact_id = sessionStorage.contact_id;
    event.recipient_email = sessionStorage.recipient_email;
    event.timeslots_available = '';

    event.appt_events.forEach((item, index) => {
      event.timeslots_available += moment(item.start).format('ddd MMM DD YYYY kk:mm:ss').toString() + '_' + moment(item.end).format('ddd MMM DD YYYY kk:mm:ss').toString();
      if (index !== (event.appt_events.length - 1)) {
        event.timeslots_available += ',';
      }
    });

    dispatch(requestAppointment(event, (payload) => {
      if (payload.status !== 'success') {
        dispatch(addToast('Error has occured while creating appointment request.', 'critical'));
      } else {
        sessionStorage.removeItem('contact_id');
        sessionStorage.removeItem('intro_uuid');
        sessionStorage.removeItem('recipient_email');
        sessionStorage.removeItem('rerequest');
        sessionStorage.removeItem('re_appt');
        this.setState({ notifications: null, isSetting: false });
        dispatch(setAppointment(false));
        dispatch(addToast(payload.message, 'ok'));
        this._toggleLayer();

        if (localStorage.getItem('newUser') === 'true') 
          this.props.history.push('/landingpage');
      }
    }));
  }

  _gatherTemplates() {
    this.props.dispatch(loadTemplates(localStorage.getItem('token')));
  }

  _showCreateLayerByPulse() {
    this.setState({isShowApptLayer: !this.state.isShowApptLayer});
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.apptList !== this.props.apptList) {
      this.setState({appointments: newProps.apptList});
      this._updateEvent(newProps.apptList);
    }
  }

  componentDidMount() {
    this._gatherTemplates();
    this._verifyIntroductionInProgress();
    if (this.props.apptList.length !== 0) {
      this.setState({appointments: this.props.apptList});
      this._updateEvent(this.props.apptList);
    } else {
      this.props.dispatch(getApptList(localStorage.token));
    }
    this.props.dispatch(getCalendarEvents(localStorage.getItem('token')))
  }

  _updateEvent(apptList) {
    apptList.map((appt) => {
      if (appt.fields.start_date_time) {
        let event = {
          'uuid': appt.fields.uuid,
          'title': appt.fields.title || 'Appointment',
          'start': new Date(appt.fields.start_date_time),
          'end': new Date(appt.fields.end_date_time),
          'allDay': false,
          'confirmed': appt.fields.confirmed
          // 'new_event': false,
        };
        this.props.dispatch(addCalendarEvent(event));        
      }

      if (appt.fields.time_slot) {
        if (appt.fields.time_slot.length > 0) {
          appt.fields.time_slot.map((time_s) => {
            let event = {
              'uuid': appt.fields.uuid,
              'title': appt.fields.title || 'Appointment',
              'start': new Date(time_s.fields.start_date_time),
              'end': new Date(time_s.fields.end_date_time),
              'allDay': false,
              'confirmed': appt.fields.confirmed
              // 'new_event': false,
            };
            this.props.dispatch(addCalendarEvent(event));
          })   
        }
      }
    })
  }


  render() {
    return (
      <div className={this.state.hideHeaderBar ? 'scheduleTabContent' : 'scheduleTabContent pageContent'}>
        {this.state.showLayer ? this.state.layer : null}
        {this.state.hideHeaderBar ? null : <HeaderBar hideNavBar={true}/>}
        {this.state.notifications ? <div>
          <Snackbar
            open={true}
            // onClose={this.handleClose}
            className={classes.snackbar}
            autoHideDuration={2000}
          >
            <MySnackbarContentWrapper
              // onClose={this.handleClose}
              variant="error"
              message={this.state.notifications}
            />
          </Snackbar>
        </div> : null}
        <CustomBigCalendar
            isSetting={this.state.isSetting}
            onSelect={(slotInfo, currentView, sendOrNot) => this._showAddCalendarEventLayer(slotInfo, currentView, sendOrNot)} 
            isShowApptLayer={this.state.isShowApptLayer}/>
        {this.state.isSetting ? 
          // <Pulse onClick={this._showCreateLayerByPulse.bind(this)} className='floating-button pulse-right' /> : ''
          <IconButton
              onClick={this._showCreateLayerByPulse.bind(this)}
              className='floating-button pulse-right'
          ><AddIcon /></IconButton> : ''
        }
      </div>
    );
  }
}

const select = state => ({ 
  calendar: state.calendar,
  apptList: state.appointment.apptList
});

export default withStyles(styles)(connect(select)(AppointmentCalendar));
