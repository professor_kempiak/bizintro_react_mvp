import React, { Component } from 'react';
import { connect } from 'react-redux';

import HeaderBar1 from '../components/HeaderBar1';
import ContactList from '../components/ContactList';
import IntroductionList from '../components/IntroductionList';
import AddContactLayer from '../components/AddContactLayer';
import EditContactLayer from '../components/EditContactLayer';
import UploadContactsLayer from '../components/UploadContactsLayer';
import MakeIntroductionLayer from '../components/MakeIntroductionLayer';

import ContactHeder from '../components/ContactHeader';
import { initialize } from '../actions/session';
import { gatherContacts } from '../actions/contacts';
import { updateSearchTags, updateSearchTerm } from '../actions/search';
import { remoteContactSearch } from '../actions/contacts';
import GetStartedLayer from '../components/GetStartedLayer';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';


import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';


import LeftSidebar1 from '../components/LeftSidebar1';

const drawerWidth = 80;
const realWidth = 0;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    'content-left': {
        marginLeft: -realWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    button: {
        margin: theme.spacing.unit,
        position: 'fixed',
        bottom: '1.3em',
        right: '6em',
        minWidth: 0,
        zIndex: '99'
    },
});


class Dashboard1 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      layer: null,
      showLayer: false,
      activeTab: 0,
        open: false,
        anchor: 'left',
    }

    this._toggleLayer = this._toggleLayer.bind(this);
    this._closeGetStarted = this._closeGetStarted.bind(this);
    this._prepMakeIntroductionLayer = this._prepMakeIntroductionLayer.bind(this);
    this._setActiveTab = this._setActiveTab.bind(this);
    this._prepAddContactLayer = this._prepAddContactLayer.bind(this);
  }


    handleDrawerOpen = () => {
        this.setState({ open: true });
    };

    handleDrawerClose = () => {
        this.setState({ open: !this.state.open });
    };

    handleChangeAnchor = event => {
        this.setState({
            anchor: event.target.value,
        });
    };

  handleChange = (event, value) => {
    this.setState({ activeTab: value });
  };

  _toggleLayer() {
    this.setState({ showLayer: !this.state.showLayer });
  }

  _prepMakeIntroductionLayer() {
    this.setState({
      layer: <MakeIntroductionLayer
          contacts={this.props.contacts}
          onClose={this._toggleLayer}
          activeTemplate={this._setActiveTab}/>
    });
    this._toggleLayer();
  }

  _prepUploadContactLayer() {
    this.setState({
      layer: <UploadContactsLayer
        onClose={this._toggleLayer} />
    });
  }

  _prepAddContactLayer() {
    this.setState({
      layer: <AddContactLayer
        uploadContacts={this._prepUploadContactLayer.bind(this)}
        onClose={this._toggleLayer} />
    });
    this._toggleLayer();
  }

  _prepEditContactLayer(contact, e) {
    this.setState({
      layer: <EditContactLayer
        contact={contact}
        showShareButton={true}
        onClose={this._toggleLayer} />
    });
    this._toggleLayer();
  }

  _handleSearchInputChange(value) {
    let { dispatch, search } = this.props;

    if (value.length > 2) {
      if (this.state.activeTab !== 1) {
        this.setState({ activeTab: 1 })
      }
    }

    let searchTagsAndTerm = search.map((tag) => {
      return { name: tag }
    });

    searchTagsAndTerm.push({ name: value });

    dispatch(updateSearchTerm(value));
    dispatch(remoteContactSearch(
      searchTagsAndTerm,
      1,
      localStorage.getItem('token')));
  }

  _setActiveTab(index) {
    this.setState({ activeTab: index });
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.search && newProps.search !== this.props.search) {
      this._setActiveTab(1)
    }
  }

  componentDidMount() {
    this.props.dispatch(gatherContacts(localStorage.getItem('token'), 1, () => { }));

    let showGettingStarted = sessionStorage.getItem('showGettingStarted') == "true";

    if (showGettingStarted) {
      this.setState({
        showLayer: true,
        layer: <GetStartedLayer
                  onClose={this._closeGetStarted} />
      })
    }
  }

  _closeGetStarted() {
    sessionStorage.setItem('showGettingStarted', false);
    this.setState({
      showLayer: false,
      layer: null
    })
  }

  render() {
      const { contacts, classes } = this.props;
      let contactHeder = '';
      if (contacts.selected.primary.length > 0) {
          contactHeder = (
            <div className={'contactHederWrapper'}>
              <div className='contactHeaderCard'>
                  <ContactHeder makeIntroducntionLayer={this._prepMakeIntroductionLayer} />
              </div>
            </div>
          );
      }
      const {  theme } = this.props;
      const { anchor, open } = this.state;

      let activeClass = 'contactList';
      if(this.state.open){
          activeClass = 'active contactList';
      }

    return (
        <div className={classes.root}>
            <div className={activeClass}>
                <div className={classes.appFrame}>
                    {this.state.toasts}
                    {this.state.notifications}
                    <HeaderBar1 open={open} anchor={anchor} searchBar={true} navBar={true} activeTab={this.state.activeTab} handleChange={this.handleChange}/>
                    <LeftSidebar1  curPage={'home'} open={this.state.open} anchor={this.state.anchor} handleDrawerClose={this.handleDrawerClose}/>

                    <main
                        className={classNames(classes.content, classes[`content-${anchor}`], {
                            [classes.contentShift]: open,
                            [classes[`contentShift-${anchor}`]]: open,
                        })}
                    >
                        <div className={classes.drawerHeader} />
                        { contactHeder }
                        <div className={'contentWrapper'}>

                            {
                                this.state.activeTab === 0 && <IntroductionList  />
                            }
                            {
                                this.state.activeTab === 1 &&
                                <div>
                                    <ContactList
                                        search={this.props.search}
                                        addContact={this._prepAddContactLayer.bind(this)}
                                        editContact={this._prepEditContactLayer.bind(this)}
                                        makeIntroducntionLayer={this._prepMakeIntroductionLayer} />
                                    <Button
                                        variant="fab"
                                        size="large"
                                        color="primary"
                                        aria-label="Add"
                                        className={classes.button}
                                        onClick={this._prepAddContactLayer.bind(this)}
                                    >
                                        <AddIcon />
                                    </Button>
                                </div>
                            }
                            {
                                this.state.activeTab === 2 && <IntroductionList archived={true} />
                            }
                        </div>
                    </main>
                    {this.state.showLayer ? this.state.layer : null}
                </div>
            </div>
        </div>

    );
  }
}

const select = state => ({
    contacts: state.contacts,
    session: state.session,
  search: state.search.tags
});

export default withStyles(styles)(connect(select)(Dashboard1));
