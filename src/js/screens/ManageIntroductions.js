import React, { Component } from 'react';
import { connect } from 'react-redux';
import TransactionIcon from 'grommet/components/icons/base/Transaction';
import UserAddIcon from 'grommet/components/icons/base/UserAdd';
import MoreIcon from 'grommet/components/icons/base/More';
import { Box, Header, Heading, Headline, Paragraph, Article, Button, TableHeader, Menu, Anchor } from 'grommet';
import HeaderBar from '../components/HeaderBar';
import { getUserIntroductionStatus, postIntroductionNudge } from '../actions/introductions';
import { addToast } from '../actions/toasts';
import moment from 'moment';
import ReactTooltip from 'react-tooltip'

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem'
import MoreHori from '@material-ui/icons/MoreHoriz';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class ManageIntroductions extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showLayer: null,
            notifications: null,
            sortAscending: false,
            sortIndex: 1,
            sortIndexLabels: []
        }

        this._updateSort = this._updateSort.bind(this);
        this._sortListItems = this._sortListItems.bind(this);
        this._sortByIntroSent = this._sortByIntroSent.bind(this);
        this._sortByPrimaryName = this._sortByPrimaryName.bind(this);
        this._sortBySecondaryName = this._sortBySecondaryName.bind(this);
        this._sortByAppointmentSet = this._sortByAppointmentSet.bind(this);
        this.introClick = this.introClick.bind(this);
        this.nudgeClick = this.nudgeClick.bind(this);
        this.appointmenSetClick = this.appointmenSetClick.bind(this);
    }

    _sendIntroductionNudge(uuid) {
        let { dispatch } = this.props;
        let token = localStorage.getItem('token');
        dispatch(postIntroductionNudge(uuid, token, (payload) => {
            if (payload.status == 'success') {
                dispatch(getUserIntroductionStatus(localStorage.getItem('token'), (payload) => {
                    dispatch(addToast('Nudge has been successfully dispatched!', 'ok'));
                }));
            } else {
                dispatch(addToast('There was an error sending a nudge', 'critical'));
            }
        }));
    }

    _updateSort(index, ascending) {
        if (index === this.state.sortIndex) {
            this.setState({ sortAscending: !this.state.sortAscending });
            return true;
        }
        this.setState({ sortIndex: index });
    }

    _sortListItems(a, b) {
        switch (this.state.sortIndex) {
            case 0:
                return this._sortByPrimaryName(a, b);
                break;
            case 1:
                return this._sortByIntroSent(a, b);
                break;
            case 2:
                return this._sortByNudgeSent(a, b);
                break;
            case 3:
                return this._sortByAppointmentSet(a, b);
                break;
            case 4:
                return this._sortBySecondaryName(a, b);
                break;
            default:
                break;
        }
    }

    _sortByPrimaryName(a, b) {
        let name_a = a.contact_a.first_name;
        let name_b = b.contact_a.first_name;

        let comparison = 0;
        if (name_a > name_b) {
            comparison = 1;
        } else {
            comparison = -1;
        }

        return this.state.sortAscending ? comparison : comparison * -1;
    }

    _sortBySecondaryName(a, b) {
        let name_a = a.contact_b.first_name;
        let name_b = b.contact_b.first_name;

        let comparison = 0;
        if (name_a > name_b) {
            comparison = 1;
        } else {
            comparison = -1;
        }

        return this.state.sortAscending ? comparison : comparison * -1;
    }

    _sortByIntroSent(a, b) {
        let intro_a = a.date_made;
        let intro_b = b.date_made;

        let comparison = 0;
        if (intro_a > intro_b) {
            comparison = 1;
        } else {
            comparison = -1;
        }

        return this.state.sortAscending ? comparison : comparison * -1;
    }

    _sortByNudgeSent(a, b) {
        let nudge_a = a.date_last_nudged ? a.date_last_nudged : '';
        let nudge_b = b.date_last_nudged ? b.date_last_nudged : '';

        let comparison = 0;
        if (nudge_a > nudge_b) {
            comparison = 1;
        } else {
            comparison = -1;
        }

        return this.state.sortAscending ? comparison : comparison * -1;
    }

    _sortByAppointmentSet(a, b) {
        let appt_a = a.appointment_details ? a.appointment_details.date_made : '';
        let appt_b = b.appointment_details ? b.appointment_details.date_made : '';

        let comparison = 0;
        if (appt_a > appt_b) {
            comparison = 1;
        } else {
            comparison = -1;
        }

        return this.state.sortAscending ? comparison : comparison * -1;
    }

    componentWillMount() {
        let { dispatch } = this.props;
        dispatch(getUserIntroductionStatus(localStorage.getItem('token'), (payload) => {
            // callback
        }));

    }
    introClick(){
        alert('intro clicked');
    }
    nudgeClick(){
      alert('nudge clicked');
    }
    appointmenSetClick(){
      alert('appointment set click');
    }

    render() {
        const { classes } = this.props;
        let { introductions } = this.props;
        if (!introductions) { introductions = [] }
        let introductionsList = introductions.length === 0 ?
            <TableRow>
                <td colSpan='6'>You have made no introductions yet! </td>
            </TableRow>
            :
            introductions.sort(this._sortListItems).map((introduction) => {
                return (
                    <TableRow key={introduction.id}>
                        <td>{introduction.contact_a.first_name + ' ' + introduction.contact_a.last_name}</td>
                        <td colSpan='3'>
                            <ul className={"progress-tracker progress-tracker--text anim-ripple"}>
                                <li className={introduction.date_made ? "progress-step is-complete" : "progress-step"} onClick={this.introClick} >
                                    <span className={ "progress-marker"} data-tip={introduction.date_made ? moment(introduction.date_made).format('YYYY-MM-DD H:m:s a') : null}></span>
                                    <span className={"progress-text"}>
                                      <h4 className={"progress-title"}>{introduction.date_made ? moment(introduction.date_made).format('YYYY-MM-DD') : null}</h4>
                                    </span>
                                </li>

                                <li className={introduction.date_last_nudged ? "progress-step is-complete" : "progress-step"} onClick={this.nudgeClick} >
                                    <span className={"progress-marker"} data-tip = {introduction.date_last_nudged ? moment(introduction.date_last_nudged).format('YYYY-MM-DD H:m:s a') : null}></span>
                                    <span className={"progress-text"}>
                                       <h4 className={"progress-title"}>{introduction.date_last_nudged ? moment(introduction.date_last_nudged).format('YYYY-MM-DD') : null}</h4>
                                   </span>
                                </li>

                                <li className={introduction.appointment_details ? "progress-step is-complete" : "progress-step"} onClick={this.appointmenSetClick} >
                                    <span className={"progress-marker"} data-tip= {introduction.appointment_details ? moment(introduction.appointment_details.date_made).format('YYYY-MM-DD H:m:s a') : null}></span>
                                    <span className={"progress-text"}>
                                        <h4 className={"progress-title"}>{introduction.appointment_details ? moment(introduction.appointment_details.date_made).format('YYYY-MM-DD') : null}</h4>
                                   </span>
                                </li>
                            </ul>
                            <ReactTooltip />
                        </td>
                        <td>{introduction.contact_b.first_name + ' ' + introduction.contact_b.last_name}</td>
                        <td>
                            <Menu icon={<MoreIcon />}>
                                <MenuItem><a href='#' onClick={() => this._sendIntroductionNudge(introduction.uuid)}>Nudge</a></MenuItem>
                                <MenuItem><a href={'mailto:' + introduction.contact_a.email + '?subject=Email from Bizintro'}>Email {introduction.contact_a.first_name}</a></MenuItem>
                                <MenuItem><a href={'mailto:' + introduction.contact_b.email + '?subject=Email from Bizintro'}>Email {introduction.contact_b.first_name}</a></MenuItem>
                            </Menu>
                        </td>
                    </TableRow>
                );
            });
        return (
            <div primary={true} className = {'pageContent'}>
                <HeaderBar />
                {/* <div pad='medium' className={'contentWrapper'}>
                    <Table>
                        <TableHeader labels={['Primary Contact', 'Intro Sent', 'Nudge Sent', 'Appointment Set', 'Secondary Contact', 'Actions']}
                                     sortIndex={this.state.sortIndex}
                                     sortAscending={!this.state.sortAscending}
                                     onSort={this._updateSort} />
                        <tbody>
                        {introductionsList}
                        </tbody>
                    </Table>
                </div> */}
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Primary Contact</CustomTableCell>
                                <CustomTableCell>Intro Sent</CustomTableCell>
                                <CustomTableCell>Nudge Sent</CustomTableCell>
                                <CustomTableCell>Appointment Set</CustomTableCell>
                                <CustomTableCell>Secondary Contact</CustomTableCell>
                                <CustomTableCell>Actions</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {introductionsList}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        );
    }
}

const select = state => ({
    introductions: state.introductions.list
});

export default withStyles(styles)(connect(select)(ManageIntroductions));
