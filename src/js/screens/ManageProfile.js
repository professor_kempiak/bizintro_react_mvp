import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToast } from '../actions/toasts';
import HeaderBar from '../components/HeaderBar';
import { initialize, updateUserProfile } from '../actions/session';
import Image from 'grommet/components/Image';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  snackbar: {
    position: 'static !important',
    transform: 'none'
  }
});

class ManageProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'first_name': '',
      'last_name': '',
      'email': '',
      'address': '',
      'title': '',
      'company': '',
      'summary': '',
      'phone': ''
    };
    this.updateProfile = this.updateProfile.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }

  updateProfile() {
    let { session, dispatch } = this.props;

    dispatch(updateUserProfile(this.state, localStorage.getItem('token'), (response) => {
      if (response.success) {
        dispatch(addToast('Updated profile successfully', 'ok'));
      } else {
        dispatch(addToast('Update profile was failed', 'critical'));
      }
    }))
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentDidMount() {
    if (localStorage.getItem('onlyProfile') == '1' && this.props.invite_user.first_name != undefined) {
      this.setState({
        'first_name': this.props.invite_user.first_name,
        'last_name': this.props.invite_user.last_name,
        'email': this.props.invite_user.email,
        'address': this.props.invite_user.address,
        'title': this.props.invite_user.title,
        'company': this.props.invite_user.company,
        'summary': this.props.invite_user.summary,
        'phone': this.props.invite_user.phone
      });
    }
  }


  render() {
    const {classes} = this.props
    return (
      <div className = {'pageContent'}>
        <HeaderBar />
        <div className={'profileWrapper'}>
          <div className={'profile-form'}>
            <div className={'profile-image'}>
              <img src='/imgs/pic1.jpg' className={'profilePhoto'} />
            </div>
            <form onSubmit={this.updateProfile}>
              {
                this.state.notification ?
                  <div>
                    <Snackbar
                      open={true}
                      // onClose={this.handleClose}
                      className={classes.snackbar}
                      autoHideDuration={2000}
                    >
                      <MySnackbarContentWrapper
                        // onClose={this.handleClose}
                        variant="error"
                        message={this.state.notification}
                      />
                    </Snackbar>
                  </div> :
                  null
              }
              <div className="for-content">
                <div className="first-name">
                  <TextField
                    className={classes.margin}
                    label="First Name"
                    id="firstName"
                    name="first_name"
                    placeholder="John"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="last-name">
                  <TextField
                    className={classes.margin}
                    label="Last Name"
                    id="lastName"
                    name="last_name"
                    placeholder="Doe"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="email">
                  <TextField
                    className={classes.margin}
                    label="Email"
                    id="email"
                    name="email"
                    placeholder="johnDoe@gmail.com"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="mobile">
                  <TextField
                    className={classes.margin}
                    label="Mobile"
                    id="mobile"
                    name="mobile"
                    placeholder="xxx-xxx-xxxx"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="address">
                  <TextField
                    className={classes.margin}
                    label="Address"
                    id="address"
                    name="address"
                    placeholder="123 Steel Street"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="title">
                  <TextField
                    className={classes.margin}
                    label="Title"
                    id="title"
                    name="title"
                    placeholder="Electrician"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="company">
                  <TextField
                    className={classes.margin}
                    label="Company"
                    id="company"
                    name="company"
                    placeholder="Vancouver Electric"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="summary">
                  <TextField
                    className={classes.margin}
                    label="Summary"
                    id="summary"
                    name="summary"
                    placeholder="Vancouver Electric"
                    onChange={this._handleInputChange}
                  />
                </div>
              </div>
              <div className={classes.buttonContainer}>
                <Button
                  variant="contained"
                  size="large"
                  color="primary"
                  className={classes.button}
                  onClick={this.updateProfile}
                >
                  Update
              </Button>
              </div>
            </form>
          </div>          
        </div>

      </div>
    );
  }
}

const select = state => ({
  introductions: state.introductions.list,
  invite_user: state.session.invite_user
});

export default withStyles(styles)(connect(select)(ManageProfile));
