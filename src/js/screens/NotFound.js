import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const style = (theme) => ({
  notFound: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
})

class NotFound extends Component {

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.notFound}>
        <Typography gutterBottom variant="headline" component="h2">
          <span style={{ fontWeight: '600' }}>404</span>
        </Typography>
        <Typography gutterBottom variant="headline" component="h2">
          <span style={{ fontWeight: '600' }}>Oops...</span>
        </Typography>
        <Typography component="h2">
          <span>It seems that you are in the wrong route. Please check your URL and try again.</span>
        </Typography>
      </div>
    );
  }
}

export default withStyles(style)(connect()(NotFound));
