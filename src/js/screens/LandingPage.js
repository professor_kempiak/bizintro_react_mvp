import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import NewsFeed from '../components/NewsFeed';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const styles = (theme) => ({

})

class LandingPage extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    render() {
        return (
            <div className={'landing-page'}>
                <div className="hero-image">
                    <div className="hero-card">
                        <Typography component="p">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                SIGN UP
                        </span>
                        </Typography>
                        <Typography gutterBottom variant="headline" component="h1">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                Accelerate your networking capacity by adding Bizintro to your relationship management toolbelt
                        </span>
                        </Typography>
                        <Typography component="p">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                Sign up today to get started making introductions and helping others network
                             </span>
                        </Typography>
                        <MenuItem><a href='#'>Activate account</a></MenuItem>
                    </div>
                </div>

                <div className={'what-is-bizintro'}>
                    <div>
                        <Typography component="h1">
                            <span style={{ fontWeight: '600' }}>
                                What is Bizintro?
                             </span>
                        </Typography>
                        <Typography component="p">
                            <span style={{ fontWeight: '600' }}>
                                Lorem ipsum dolor sit amet, dicat sonet congue ei mei, est summo
                                copiosae facilisi an. Sumo accumsan mel ea, eu ignota hendrerit
                                consequuntur me.
                             </span>
                        </Typography>
                        <Button variant="contained" color="primary">
                            Learn more
                        </Button>
                    </div>
                </div>

                <div className="news-list">
                    <div className="header">
                        <Typography component="h2">
                            <span style={{ fontWeight: '600' }}>
                                Recent News
                             </span>
                        </Typography>
                    </div>
                    <NewsFeed />
                </div>
                <div className="landing-page-footer">
                    <div className="footer-cards-container">
                        <div className="left-card">
                            <Typography component="h1">
                                <span>
                                    FEATURES
                                </span>
                            </Typography>
                            <Typography component="h2">
                                <span>
                                    Read more about the product features on Bizintro.com
                                </span>
                            </Typography>
                            <MenuItem>
                                <a href="#">Learn More</a>
                            </MenuItem>
                        </div>
                        <div className="right-card">
                            <Typography component="h1">
                                <span>
                                    AFFILIATES
                                </span>
                            </Typography>
                            <Typography component="h2">
                                <span>
                                    Sign up to become an affiliate partner for Bizintro.
                                </span>
                            </Typography>
                            <MenuItem>
                                <a href="#">Learn More</a>
                            </MenuItem>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const select = state => ({
    contacts: state.contacts,
    session: state.session,
    search: state.search.tags
});

export default withStyles(styles)(connect(select)(LandingPage));