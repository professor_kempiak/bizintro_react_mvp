import React, { Component } from 'react';
import { connect } from 'react-redux';
import HeaderBar from '../components/HeaderBar';
import ContactList from '../components/ContactList';
import IntroductionList from '../components/IntroductionList';
import AddContactLayer from '../components/AddContactLayer';
import EditContactLayer from '../components/EditContactLayer';
import UploadContactsLayer from '../components/UploadContactsLayer';
import MakeIntroductionLayer from '../components/MakeIntroductionLayer';
import Pulse from 'grommet/components/icons/Pulse'

import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import ContactHeder from '../components/ContactHeader';
import { initialize } from '../actions/session';
import { gatherContacts } from '../actions/contacts';
import { updateSearchTags, updateSearchTerm } from '../actions/search';
import { remoteContactSearch } from '../actions/contacts';
import AppointmentCalendar from './AppointmentCalendar';
import AppointmentMap from './AppointmentMap';
const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabBar: {
    marginTop: '4.1em',
    padding: '0 1.5em'
  }
});

function TabContainer(props) {
  return (
    <Material.Typography component="div" style={{ padding: 5 }}>
      {props.children}
    </Material.Typography>
  );
}

class Schedule extends Component {

  constructor(props) {
    super(props);

    this.state = {
      layer: null,
      showLayer: false,
      activeTab: 0
    }

    this._toggleLayer = this._toggleLayer.bind(this);
    this._closeGetStarted = this._closeGetStarted.bind(this);
    this._prepMakeIntroductionLayer = this._prepMakeIntroductionLayer.bind(this);
    this._setActiveTab = this._setActiveTab.bind(this);
  }

  _toggleLayer() {
    this.setState({ showLayer: !this.state.showLayer });
  }

  _prepMakeIntroductionLayer() {
    this.setState({
      layer: <MakeIntroductionLayer
          contacts={this.props.contacts}
          onClose={this._toggleLayer}
          activeTemplate={this._setActiveTab}/>
    });
    this._toggleLayer();
  }

  _prepUploadContactLayer() {
    this.setState({
      layer: <UploadContactsLayer
        onClose={this._toggleLayer} />
    });
  }

  _prepAddContactLayer() {
    this.setState({
      layer: <AddContactLayer
        uploadContacts={this._prepUploadContactLayer.bind(this)}
        onClose={this._toggleLayer} />
    });
    this._toggleLayer();
  }

  _prepEditContactLayer(contact, e) {
    this.setState({
      layer: <EditContactLayer
        contact={contact}
        showShareButton={true}
        onClose={this._toggleLayer} />
    });
    this._toggleLayer();
  }

  _handleSearchInputChange(value) {
    let { dispatch, search } = this.props;

    if (value.length > 2) {
      if (this.state.activeTab !== 1) { 
        this.setState({ activeTab: 1 }) 
      }
    }

    let searchTagsAndTerm = search.map((tag) => {
      return { name: tag }
    });

    searchTagsAndTerm.push({ name: value });

    dispatch(updateSearchTerm(value));
    dispatch(remoteContactSearch(
      searchTagsAndTerm, 
      1, 
      localStorage.getItem('token')));
  }

  _setActiveTab(event, value) {
    this.setState({ activeTab: value });
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.search && newProps.search !== this.props.search) {
      this._setActiveTab(1)
    }
  }
 
  componentDidMount() {    
    this.props.dispatch(gatherContacts(localStorage.getItem('token'), 1, () => { }));
    
    let showGettingStarted = sessionStorage.getItem('showGettingStarted') == "true";
    
    if (showGettingStarted) {
      this.setState({ 
        showLayer: true,
        layer: <GetStartedLayer 
                  onClose={this._closeGetStarted} />
      })
    }
  }

  _closeGetStarted() {
    sessionStorage.setItem('showGettingStarted', false);
    this.setState({
      showLayer: false,
      layer: null
    })
  }

  handleChange = (event, value) => {
    this.setState({ activeTab: value });
  };

  render() {
    const { contacts, classes } = this.props;
    const { activeTab } = this.state;
      
    return (
      <div className={classes.root}>
        <HeaderBar/>
        {this.state.notifications}

        <Material.AppBar position="static" color="default" className={classes.tabBar}>
          <Material.Tabs value={activeTab} onChange={this.handleChange}>
            <Material.Tab label="Calendar" />
            <Material.Tab label="Map" />            
          </Material.Tabs>
        </Material.AppBar>
        {activeTab === 0 && <TabContainer><AppointmentCalendar showHeader={true} /></TabContainer>}
        {activeTab === 1 && <TabContainer><AppointmentMap showHeader={true} /></TabContainer>}        
      </div>      
    );
  }
}

const select = state => ({
  contacts: state.contacts,
  session: state.session,
  search: state.search.tags
});

export default withStyles(styles)(connect(select)(Schedule));
