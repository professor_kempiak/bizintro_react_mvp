import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Auth from '../../Auth/Auth.js';
import { CONFIG } from "../config.js"
import { login } from '../actions/session';
import { headers } from '../api/utils';
import Sidebar from 'react-sidebar';

import LeftSidebar from '../components/LeftSidebar';
import SignUp from '../screens/SignUp';

import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        width: '100% !important',
        margin: '8px 0',
    },
    button: {
        margin: theme.spacing.unit,
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center'
    }
});

let btnStyle = {
    borderRadius: '4px'
}

let imgStyle = {
    width:'18px',
    height:'18px'
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open:false,
            userName: '',
            passwd:'',
            showPassword: false,
        }

        this._loginWithAuth0 = this._loginWithAuth0.bind(this);
        this.loginWithUsernamePassword = this.loginWithUsernamePassword.bind(this);
        this.loginGoogle = this.loginGoogle.bind(this);

        this.sidebar = <div />;
        this.onSetOpen = this.onSetOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setOfferSidebar = this.setOfferSidebar.bind(this);
        this._handleInputChange= this._handleInputChange.bind(this);
        this._handlePassChange= this._handlePassChange.bind(this);
    }

    _loginWithAuth0() {
        // const auth = new Auth();
        // auth.login();
    }
    onSetOpen(open) {
        this.setState({ open });
    }
    closeModal() {
        this.setState({ open: false });
    }
    handleClose() {
        this.setState({ isShowingModal: false });
    }

    setOfferSidebar() {
        this.setState({ open: true, isShowingModal: false });
    }

    loginWithUsernamePassword(fields) {
        const { dispatch, history } = this.props;
        const { router } = this.context;
        dispatch(login(this.state.userName, this.state.passwd, () => {
                sessionStorage.setItem('showGettingStarted', true);
                history.push('/');
            }
        ));
        this.setState({
            userName: '',
            passwd: ''
        });
    }
    componentDidMount(){
        // render google login button
        // gapi.signin2.render('signInGoogle', {
        //     'scope': 'profile email',
        //     'width': 432,
        //     'height': 40,
        //     'longtitle': true,
        //     'theme': 'light'
        // });
    }
    _handleInputChange(event) {
        const target = event.target;
        const value = target.value;

        this.setState({
            userName: value
        });
    }
    _handlePassChange(event){
        const target = event.target;
        const value = target.value;

        this.setState({
            passwd: value
        });
    }

    loginGoogle() {
        let clientId = CONFIG.clientId,
            url = CONFIG.redirectUri;

        window.location.href = "https://accounts.google.com/o/oauth2/auth?redirect_uri="+ url +"&prompt=select_account + consent&response_type=code&client_id=" + clientId + "&scope=https://www.googleapis.com/auth/plus.login+https://www.google.com/calendar/feeds/+https://www.google.com/m8/feeds+https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/gmail.readonly&access_type=offline";
        // window.location = bbburl; +https://www.googleapis.com/auth/gmail.readonly+https://www.googleapis.com/auth/gmail.modify+https://www.googleapis.com/auth/gmail.compose+https://www.googleapis.com/auth/gmail.send
    }

    handleMouseDownPassword = event => {
        event.preventDefault();
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    render() {
        const { session: { error }, classes } = this.props;
        const sidebarProps = {
            sidebar: this.sidebar,
            sidebarClassName: 'custom-sidebar-class',
            open: this.state.open,
            onSetOpen: this.onSetOpen,
        };

        return (
            <Sidebar {...sidebarProps} pullRight size='large' >
                <SignUp {...this.props} open={this.state.open} handleClose={() => { this.closeModal(); }} />;
                <div className={'test123'}>
                    <div className={'left-side'}>
                        <Typography variant="headline" component="h2">
                            Imagine a universe where everyone gives... you are there!
                        </Typography>
                    </div>
                    <div className={'right-side'}>
                        <p className={'main-title'}>
                            Bizintro
                        </p>
                        <p className={'smail-title'}>
                            Sign in with your email address below.
                        </p>
                        <div className={'login-form'}>
                            <div className="user-email">
                                <InputLabel htmlFor="user_email">User Email</InputLabel>
                                <TextField
                                    id="Useremail"
                                    name="user_email"
                                    placeholder="user@domain.com"
                                    value={this.state.userName}
                                    onChange={this._handleInputChange}
                                />
                            </div>
                            <div className="password">
                                <InputLabel htmlFor="pass_a">Password</InputLabel>
                                <Input
                                    id="adornment-password"
                                    type={this.state.showPassword ? 'text' : 'password'}
                                    value={this.state.password}
                                    onChange={this._handlePassChange}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="Toggle password visibility"
                                                onClick={this.handleClickShowPassword}
                                                onMouseDown={this.handleMouseDownPassword}
                                            >
                                                {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                            </div>
                            <p className={'errorMsg'}>
                                {[error]}
                            </p>
                            <div className={'remember-me'}><Checkbox /> Remember me</div>
                            <Button className={'login-button'} onClick={this.loginWithUsernamePassword}>Log In</Button>
                            <div 
                                id="signInGoogle" 
                                className="g-signin2" 
                                data-width="300" 
                                data-height="200" 
                                data-longtitle="true" 
                                onClick={this.loginGoogle}>
                                <div style={btnStyle} className="abcRioButton abcRioButtonLightBlue">
                                    <div className="abcRioButtonContentWrapper">
                                        <div className="abcRioButtonIcon" style={{ padding: '10px' }}>
                                            <div style={imgStyle} className="abcRioButtonSvgImageWithFallback abcRioButtonIconImage abcRioButtonIconImage18">
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 48 48" className="abcRioButtonSvg">
                                                    <g>
                                                        <path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path><path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path><path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path><path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path><path fill="none" d="M0 0h48v48H0z">
                                                        </path>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                        <span style={{ fontSize: '14px', lineHeight: '38px' }} className="abcRioButtonContents"><span id="connectedf9nonzbyplij">Signed in with Google</span></span>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <MenuItem><a href='#' onClick={this.setOfferSidebar}>Not already a Bizintro User? Sign up!</a></MenuItem>
                            <MenuItem><a href='#'>Forgot password?</a></MenuItem>
                            <div className={'brand'}>
                                <p>
                                    © 2018 Bizintro | Version 1.0.0
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </Sidebar>
        );
    }
}


Login.contextTypes = {
    // router: PropTypes.object.isRequired,
};

const select = state => ({
    session: state.session
});


//here is a comment

export default withStyles(styles)(connect(select)(Login));
