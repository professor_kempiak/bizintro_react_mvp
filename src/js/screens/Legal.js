import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import HeaderBar from '../components/HeaderBar';
import ContactList from '../components/ContactList';
import AddContactLayer from '../components/AddContactLayer';
import EditContactLayer from '../components/EditContactLayer';
import UploadContactsLayer from '../components/UploadContactsLayer';
import MakeIntroductionLayer from '../components/MakeIntroductionLayer';
import ContactHeder from '../components/ContactHeader';

import { initialize } from '../actions/session';
import { headers, parseJSON } from '../api/utils';
import { gatherContacts } from '../actions/contacts';
import { loadTemplates } from '../actions/templates';
import { updateSearchTags } from '../actions/search';

class Legal extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }
    render() {

        return (
            <div primary={true} className = {'contactList pageContent'}>
                <HeaderBar />
                 legal page
            </div>
        );
    }
}

const select = state => ({
    contacts: state.contacts,
    session: state.session,
    search: state.search.tags
});

export default connect(select)(Legal);