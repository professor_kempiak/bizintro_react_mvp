import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Box, Header, Heading, Headline, Paragraph, Article, Table, TableHeader, TableRow, Menu, Anchor } from 'grommet';
import { addToast } from '../actions/toasts';
import HeaderBar from '../components/HeaderBar';
import { initialize } from '../actions/session';
import { updateUserContact,editContact } from '../actions/contacts';
import Image from 'grommet/components/Image';
import Columns from 'grommet/components/Columns';
import FormField from 'grommet/components/FormField';
import Form from 'grommet/components/Form';
import CustomReactQuill from '../components/CustomReactQuill';

const ReactTags = require('react-tag-autocomplete')
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  }
});

class ManageContact extends Component {
  constructor(props) {
    super(props);
    let tags = [];
    if (this.props.invite_contact.tags !== undefined ) {
      this.props.invite_contact.tags.map((tag, index) => {
        if (tag.length > 0) {
          tags.push({
            id: index, 
            name: tag
          })
        }
      });      
    }

    this.state = {
      'id': '',
      'first_name': '',
      'last_name': '',
      'email': '',
      'address': '',
      'title': '',
      'company': '',
      'bio': '',
      'phone': '',
      tags:[],
      suggestions: [
        { id: 3, name: "Developer"},
        { id: 4, name: "Designer"},
        { id: 5, name: "QA"},
        { id: 6, name: "Project Manager"},
        { id: 7, name: "Sales Analyst"},
        { id: 8, name: "Master Data Departmental"},
        { id: 9, name: "Finance Departmental "},
        { id: 10, name: "Cost Analyst"},
        { id: 11, name: "Finance Manager"},
        { id: 12, name: "Finance Director"},      
        { id: 13, name: "Marketing Manager"},
        { id: 14, name: "Export Order Manager"}            
      ]  
    };
    this.updateContact = this.updateContact.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleQuillChange = this._handleQuillChange.bind(this);
  }

  updateContact() {

    let { session, dispatch } = this.props;
    let tags = [];
    this.state.tags.map(tag => {
      tags.push(tag.name);
    })
    let contact = {
      id: this.state.id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      title: this.state.title,
      company: this.state.company,
      primary_email: this.state.email,
      emails: [this.state.email],
      primary_phone_number: this.state.phone,
      phone_numbers: [this.state.phone],
      primary_address: this.state.address,
      addresses: [this.state.address],
      tags: tags,
      bio: this.state.bio
    }
    dispatch(editContact(contact, localStorage.getItem('token'), (response) => {
      if (response.success) {
        if (response.is_user == false) {
          this.props.history.push('/landingpage');
        }
        dispatch(addToast('Contact updated successfully!', 'ok'));
      } else {
        if (!response.message) { response.message = 'Error updating contact' }
        dispatch(addToast(response.message, 'critical'));
      }
    }));
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  _handleQuillChange(value) {
    this.setState({
      bio: value
    });
  }

  handleDelete (i) {
    const tags = this.state.tags.slice(0)
    tags.splice(i, 1)
    this.setState({ tags })
  }
  
  handleAddition (tag) {
    const tags = [].concat(this.state.tags, tag)
    this.setState({ tags })
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentDidMount() {
    if (localStorage.getItem('onlyProfile') == '1' && this.props.invite_contact.first_name != undefined) {
      let tags = [];
      if (this.props.invite_contact.tags !== undefined ) {
        this.props.invite_contact.tags.map((tag, index) => {
          if (tag.length > 0) {
            tags.push({
              id: index, 
              name: tag
            })
          }
        });      
      }

      this.setState({
        'id': this.props.invite_contact.id,
        'first_name': this.props.invite_contact.first_name,
        'last_name': this.props.invite_contact.last_name,
        'email': this.props.invite_contact.email,
        'address': this.props.invite_contact.address,
        'title': this.props.invite_contact.title,
        'company': this.props.invite_contact.company,
        'bio': this.props.invite_contact.summary,
        'phone': this.props.invite_contact.phone,
        tags: tags,
      });
    }
  }


  render() {
    const { classes } = this.props
    return (
      <div primary={true} className = {'pageContent'}>
        <HeaderBar searchBar={true} navBar={true} />
        <div pad='medium' className={'profileWrapper'}>
          {/* <Columns size='medium'
            justify='center'
            masonry={false}>

            <div align='center'
              pad='none'
              margin='medium'>
              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='First Name'>
                    <TextInput name="first_name" onDOMChange={this._handleInputChange} value={this.state.first_name} />
                  </FormField>
                </div>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Last Name'>
                    <TextInput name="last_name" onDOMChange={this._handleInputChange} value={this.state.last_name} />
                  </FormField>
                </div>
              </Columns>
              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Email'>
                    <TextInput name="email" onDOMChange={this._handleInputChange} value={this.state.email} />
                  </FormField>
                </div>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Phone Number'>
                    <TextInput name="phone" onDOMChange={this._handleInputChange} value={this.state.phone} />
                  </FormField>
                </div>
              </Columns>

              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Addresses'>
                    <TextInput name="address" onDOMChange={this._handleInputChange} value={this.state.address} />
                  </FormField>
                </div>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Title'>
                    <TextInput name="title" onDOMChange={this._handleInputChange} value={this.state.title} />
                  </FormField>
                </div>
              </Columns>
            </div>
            <div align='center'
              pad='none'
              margin='medium'>
              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label='Company'>
                    <TextInput name="company" onDOMChange={this._handleInputChange} value={this.state.company} />
                  </FormField>
                </div>                
              </Columns>

              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label="Tags" htmlFor="tag" className='no-overflow'>
                    <ReactTags   
                        classNames={{ root: 'react-tags tagsContainer', searchInput: 'react-tags__search-input tagsSearchInput', selectedTag: 'react-tags__selected-tag tagsSelectedTag', selectedTagName: 'react-tags__selected-tag-name tagsSelectedTagName'}}                              
                        tags={this.state.tags}
                        allowNew={true}
                        suggestions={this.state.suggestions}
                        handleDelete={this.handleDelete.bind(this)}
                        handleAddition={this.handleAddition.bind(this)} />
                  </FormField>
                </div>
                <div align='center'
                  pad='none'
                  margin='small'>
                  <FormField label="Bio" htmlFor="bio">
                    <div pad='small'>
                      <CustomReactQuill
                        onChange={this._handleQuillChange}
                        value={this.state.bio} />
                    </div>
                  </FormField>
                </div>
              </Columns>
              <Columns size='medium'
                justify='start'
                masonry={false}>
                <div direction='row' responsive={false} alignSelf='center'>
                  <Button label='Update' onClick={this.updateContact} />
                </div>
              </Columns>
            </div>
          </Columns>           */}
          <form onSubmit={this.updateContact}>
            {/* {
              this.state.notification ?
                <div>
                  <Snackbar
                    open={true}
                    // onClose={this.handleClose}
                    className={classes.snackbar}
                    autoHideDuration={2000}
                  >
                    <MySnackbarContentWrapper
                      // onClose={this.handleClose}
                      variant="error"
                      message={this.state.notification}
                    />
                  </Snackbar>
                </div> :
                null
            } */}
            <div className="for-content">
              <div>
                <div className="first-name">
                  <TextField
                    className={classes.margin}
                    label="First Name"
                    id="firstName"
                    name="first_name"
                    placeholder="John"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="last-name">
                  <TextField
                    className={classes.margin}
                    label="Last Name"
                    id="lastName"
                    name="last_name"
                    placeholder="Doe"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="email">
                  <TextField
                    className={classes.margin}
                    label="Email"
                    id="email"
                    name="email"
                    placeholder="johnDoe@gmail.com"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="mobile">
                  <TextField
                    className={classes.margin}
                    label="Phone Number"
                    id="mobile"
                    name="mobile"
                    placeholder="xxx-xxx-xxxx"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="address">
                  <TextField
                    className={classes.margin}
                    label="Address"
                    id="address"
                    name="address"
                    placeholder="123 Steel Street"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className="title">
                  <TextField
                    className={classes.margin}
                    label="Title"
                    id="title"
                    name="title"
                    placeholder="Electrician"
                    onChange={this._handleInputChange}
                  />
                </div>
              </div>
              <div>
                <div className="company">
                  <TextField
                    className={classes.margin}
                    label="Company"
                    id="company"
                    name="company"
                    placeholder="Vancouver Electric"
                    onChange={this._handleInputChange}
                  />
                </div>
                <div className='no-overflow'>
                  <ReactTags
                    classNames={{ root: 'react-tags tagsContainer', searchInput: 'react-tags__search-input tagsSearchInput', selectedTag: 'react-tags__selected-tag tagsSelectedTag', selectedTagName: 'react-tags__selected-tag-name tagsSelectedTagName' }}
                    tags={this.state.tags}
                    allowNew={true}
                    suggestions={this.state.suggestions}
                    handleDelete={this.handleDelete.bind(this)}
                    handleAddition={this.handleAddition.bind(this)} />
                </div>
                <div>
                  <div pad='small'>
                    <CustomReactQuill
                      onChange={this._handleQuillChange}
                      value={this.state.bio} />
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.buttonContainer}>
              <Button
                variant="contained"
                size="large"
                color="primary"
                className={classes.button}
                onClick={this.updateContact}
              >
                Update
              </Button>
            </div>
          </form>
        </div>

      </div>
    );
  }
}

const select = state => ({
  introductions: state.introductions.list,
  invite_contact: state.contacts.invite_contact
});

export default withStyles(styles)(connect(select)(ManageContact));
