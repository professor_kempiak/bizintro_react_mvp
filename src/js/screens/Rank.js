import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Article, Box, Button, Toast, Notification } from 'grommet';

import HeaderBar from '../components/HeaderBar';
import * as Material from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import RankList from '../components/RankList';


const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabBar: {
    marginTop: '4.1em',
    padding: '0 1.5em'
  }
});

class Rank extends Component { 
	render() { 
		return (
			<Article primary={true} className = {'pageContent'}>
				<HeaderBar />
				<Box pad='medium' className={''}>
					<RankList />
				</Box>
			</Article>
		);
	}
}

export default Rank;