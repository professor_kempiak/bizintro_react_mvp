import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import HeaderBar from '../components/HeaderBar';
import NewsFeed from '../components/NewsFeed';
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import {getZones} from "../actions/zones";
import {getApptList} from "../actions/appointments";

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

const styles = (theme) => ({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
})

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoiamlhbmcyMDIwIiwiYSI6ImNqZWdzNHRrMDF5NHIyeG1rMzVkbWIwZ3AifQ.57jhLiNkcAccrUij-EMt-Q"
});

const dayWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

const appointmentData = [
    { lat: 41.52916347, long: -87.5625333, day: "Monday", title: " VC Funding", attendees: "John Smith & Bob Arnold", datetime: "March 12, 2018 3:00 - 5:00 PM", location: "290 S Cass Ave.Westmont, IL 60559", notes: "Discuss strategy for marketing and presentation for the Marriott presentation in April." }
]


class LandingPage2 extends Component {
    constructor(props) {
        super(props);

        this.state = {

            isGetGeoData: true,
            appointments: [],
        }
        this.oppointments = appointmentData;

        this.getLatLongData = this.getLatLongData.bind(this);
    }
    componentDidMount() {
        let { dispatch, apptList } = this.props;
        if (apptList.length !== 0) {
            this.getLatLongData(apptList)
                .then(list => {
                    this.oppointments = list;
                    this.setState({ appointments: list });
                });
        } else {
            dispatch(getApptList(localStorage.token));
        }
        dispatch(getZones(localStorage.getItem('token')));
    }
    getLatLongData(apptList) {
        let geocoder = new google.maps.Geocoder(),
            _apptList = [],
            nonGeoCount = 0;

        _apptList = _apptList.filter((appt) => {
            return appt.fields.status == 'c';
        });
        return new Promise((resolve, reject) => {
            apptList.map((appt) => {
                let address = appt.fields.locailized_address;
                geocoder.geocode({ 'address': address.address }, (results, status) => {
                    let attendee = appt.fields.user_profile.first_name + ' ' + appt.fields.user_profile.last_name + ' & ' + appt.fields.recipient_user_profile.first_name + ' ' + appt.fields.recipient_user_profile.last_name;

                    if (status == google.maps.GeocoderStatus.OK) {
                        let latitude = results[0].geometry.location.lat();
                        let longitude = results[0].geometry.location.lng();
                        _apptList.push({
                            'lat': latitude,
                            'long': longitude,
                            'day': dayWeek[(new Date(appt.fields.start_date_time)).getDay()],
                            'title': appt.fields.title,
                            'attendees': attendee,
                            'datetime': (new Date(appt.fields.start_date_time)).toDateString(),
                            'location': address.address,
                            'notes': appt.fields.message
                        });
                        if (_apptList.length == (apptList.length - nonGeoCount)) {
                            resolve(_apptList);
                        }
                    }
                    else {
                        nonGeoCount ++;
                        _apptList.push({
                            'lat': '',
                            'long': '',
                            'day': dayWeek[(new Date(appt.fields.start_date_time)).getDay()],
                            'title': appt.fields.title,
                            'attendees': attendee,
                            'datetime': (new Date(appt.fields.start_date_time)).toDateString(),
                            'location': address.address,
                            'notes': appt.fields.message
                        });
                        if (_apptList.length == (apptList.length - nonGeoCount)) {
                            resolve(_apptList);
                        }
                    }
                });
            });
        })
    }
    updateGeoData(apptList){
        if(this.state.isGetGeoData) {
            this.getLatLongData(apptList)
                .then(list => {
                    this.oppointments = list;
                    this.setState({appointments: list,isGetGeoData: false});
                });
        }
    }


    render() {
        if(this.props.apptList.length > 0){
            this.updateGeoData(this.props.apptList);
        }
        const { classes } = this.props;

        return (
            <div className={'landing-page2'}>
                <div className={'hero-image'}>
                    <Card className={classes.card}>
                        <Map
                            style="mapbox://styles/mapbox/streets-v8"
                            center={[this.oppointments[0]['long'], this.oppointments[0]['lat']]}
                            zoom={[14]}
                            containerStyle={{
                                height: "200px",
                                width: "384px"
                            }}>
                            <Layer
                                type="symbol"
                                id="marker"
                                layout={{ "icon-image": "marker-15" }}>
                                {

                                    <Feature
                                        key={'key'}
                                        onClick={() => this._prepDetailAppointmentLayer(this.oppointments[0])}
                                        coordinates={[this.oppointments[0].long, this.oppointments[0].lat]}
                                    />

                                }
                            </Layer>
                        </Map>
                        <CardContent>
                            <Typography component="p">
                                Featured Post
                            </Typography>
                            <Typography gutterBottom variant="headline" component="h2">
                                Metting with {this.oppointments[0]['attendees']} about {this.oppointments[0]['title']} at {this.oppointments[0]['location']}
                            </Typography>
                        </CardContent>
                    </Card>
                    <div className={'hero-card'}>
                        <Typography component="p">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                ACTIVATE ACCOUNT
                            </span>
                        </Typography>
                        <Typography gutterBottom variant="headline" component="h1">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                Now that was easy your networking capacity by adding Bizintro to your relationship management toolbelt
                            </span>
                        </Typography>
                        <Typography component="p">
                            <span style={{ color: 'white', fontWeight: '600' }}>
                                Activate your account below to access the networking management platform
                             </span>
                        </Typography>
                        <MenuItem><a href='#'>Activate account</a></MenuItem>
                    </div>
                    {/* <Hero background={<Image src='img/bethany-legg-9248-unsplash.jpg'
                                             fit='cover'
                                             full={true} />}
                          backgroundColorIndex='dark'>
                        <Box direction='row'
                             justify='center'
                             align='center'>
                            <Box basis='1/2'
                                 align='start'
                                 pad='medium' >
                                <Card
                                    colorIndex="light-1"
                                    margin="small"
                                    contentPad="medium"
                                    thumbnail={
                                        <Map
                                            style="mapbox://styles/mapbox/streets-v8"
                                            center={[this.oppointments[0]['long'],this.oppointments[0]['lat']]}
                                            zoom = {[14]}
                                            containerStyle={{
                                                height: "200px",
                                                width: "384px"
                                            }}>
                                            <Layer
                                                type="symbol"
                                                id="marker"
                                                layout={{ "icon-image": "marker-15" }}>
                                                {

                                                    <Feature
                                                        key={'key'}
                                                        onClick={() => this._prepDetailAppointmentLayer(this.oppointments[0])}
                                                        coordinates={[this.oppointments[0].long, this.oppointments[0].lat]}
                                                    />

                                                }
                                            </Layer>
                                        </Map>}
                                    direction="column"
                                    label={this.oppointments[0]['datetime']}>
                                    <Heading tag="h2">
                                        Metting with {this.oppointments[0]['attendees']} about {this.oppointments[0]['title']} at {this.oppointments[0]['location']}
                                    </Heading>
                                </Card>
                            </Box>
                            <Box basis='1/2'
                                 align='start'
                                 pad='medium'>
                                <Card
                                    heading={
                                        <Heading strong={true}>
                                            Now that was easy your networking capacity by adding Bizintro to your relationship management toolbelt
                                        </Heading>
                                    }
                                    className={'whiteColor'}
                                    description="Activate your account below to access the networking management platform"
                                    label="ACTIVATE ACCOUNT"
                                    size="large"
                                    link={
                                        <Anchor href="#" primary={true} label="Activate account" />
                                    } />
                            </Box>
                        </Box>
                    </Hero> */}
                </div>

                <div className={'what-is-bizintro'}>
                    <div>
                        <Typography component="h1">
                            <span style={{ fontWeight: '600' }}>
                                What is Bizintro?
                             </span>
                        </Typography>
                        <Typography component="p">
                            <span style={{ fontWeight: '600' }}>
                                Lorem ipsum dolor sit amet, dicat sonet congue ei mei, est summo
                                copiosae facilisi an. Sumo accumsan mel ea, eu ignota hendrerit
                                consequuntur me.
                             </span>
                        </Typography>
                        <Button variant="contained" color="primary">
                            Learn more
                        </Button>
                    </div>
                </div>

                <div className="news-list">
                    <div className="header">
                        <Typography component="h2">
                            <span style={{ fontWeight: '600' }}>
                                Recent News
                             </span>
                        </Typography>
                    </div>
                    <NewsFeed />
                </div>
                <div className="landing-page-footer">
                    <div className="footer-cards-container">
                        <div className="left-card">
                            <Typography component="h1">
                                <span>
                                    FEATURES
                                </span>
                            </Typography>
                            <Typography component="h2">
                                <span>
                                    Read more about the product features on Bizintro.com
                                </span>
                            </Typography>
                            <MenuItem>
                                <a href="#">Learn More</a>
                            </MenuItem>
                        </div>
                        <div className="right-card">
                            <Typography component="h1">
                                <span>
                                    AFFILIATES
                                </span>
                            </Typography>
                            <Typography component="h2">
                                <span>
                                    Sign up to become an affiliate partner for Bizintro.
                                </span>
                            </Typography>
                            <MenuItem>
                                <a href="#">Learn More</a>
                            </MenuItem>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const select = state => ({
    contacts: state.contacts,
    apptList: state.appointment.apptList,
    session: state.session,
    search: state.search.tags
});

export default withStyles(styles)(connect(select)(LandingPage2));