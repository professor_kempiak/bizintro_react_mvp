import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import UserAddIcon from 'grommet/components/icons/base/UserAdd';

import { Box, Header, Article, Layer, TableHeader } from 'grommet';
import HeaderBar from '../components/HeaderBar';
import TemplateForm from '../components/TemplateForm';

import { initialize } from '../actions/session';
import { addToast } from '../actions/toasts';
import { removeTemplate, loadTemplates } from '../actions/templates';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import TrashIcon from '@material-ui/icons/Delete'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  button: {
    margin: theme.spacing.unit
  },
  addButton: {
    color: 'white',
    marginTop: '10px',
    backgroundColor: '#1b39a8'
  }
});

class ManageTemplates extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showLayer: null,
      layer: null,
      notifications: null,
      sortAscending: false,
      sortIndex: 0
    }

    this._updateSort = this._updateSort.bind(this);
    this._toggleLayer = this._toggleLayer.bind(this);
    this._sortListItems = this._sortListItems.bind(this);
    this._removeTemplate = this._removeTemplate.bind(this);
    this._sortListItemsBy = this._sortListItemsBy.bind(this);
    this._prepTemplateLayer = this._prepTemplateLayer.bind(this);
    this._prepEditTemplateLayer = this._prepEditTemplateLayer.bind(this);
  }

  _updateSort(index, ascending) {
    if (index === this.state.sortIndex) {
      this.setState({ sortAscending: !this.state.sortAscending });
      return true;
    }
    this.setState({ sortIndex: index });
  }

  _sortListItems(a, b) {
    switch (this.state.sortIndex) {
      case 0:
        return this._sortListItemsBy(a, b, 'name');
        break;
      case 1:
        return this._sortListItemsBy(a, b, 'type');
        break;
      case 2:
        return this._sortListItemsBy(a, b, 'subject');
        break;
      case 3:
        return this._sortListItemsBy(a, b, 'last_modified');
        break;
      default:
        break;
    }
  }

  _sortListItemsBy(a, b, sortBy) {
    let sortBy_a = a[sortBy];
    let sortBy_b = b[sortBy];

    let comparison = 0;
    if (sortBy_a > sortBy_b) {
      comparison = 1;
    } else {
      comparison = -1;
    }

    return this.state.sortAscending ? comparison : comparison * -1;
  }

  _toggleLayer() {
    this.setState({ showLayer: !this.state.showLayer });
  }

  _removeTemplate(id) {
    let token = localStorage.getItem('token');
    this.props.dispatch(removeTemplate(id, token));
  }

  _prepTemplateLayer() {
    const { ...other } = this.props
    this.setState({
      layer: <Dialog open={true} onClose={this._toggleLayer} aria-labelledby="form-dialog-title" { ...other } >
                <DialogTitle id="form-dialog-title">Add New Contact</DialogTitle>
                <DialogContent>
                  <TemplateForm onSubmit={this._toggleLayer} />
                </DialogContent>
              </Dialog>
    });
    this._toggleLayer();
  }

  _prepEditTemplateLayer(template_id) {
    const { ...other } = this.props
    this.setState({
      layer: <Dialog open={true} onClose={this._toggleLayer} aria-labelledby="form-dialog-title" {...other} >
              <DialogContent>
                <TemplateForm onSubmit={this._toggleLayer} formMode='edit' id={template_id} />
              </DialogContent>
            </Dialog>
    });
    this._toggleLayer();
  }

  componentWillMount() {
    let { dispatch } = this.props;
    dispatch(initialize());
    dispatch(loadTemplates(localStorage.getItem('token')));
  }

  render() {
    const { classes } = this.props;
    let { templates } = this.props;
    if (!templates) { templates = [] }
    let templatesList = templates.length === 0 ?
      <TableRow>
        <td colSpan='5'>You have no templates saved yet! Click Add Template button above!</td>
      </TableRow>
      :
      templates.sort(this._sortListItems).map((template) => {
        return (
          <TableRow key={template.id} onClick={() => this._prepEditTemplateLayer(template.id)}>
            <td>{template.name}</td>
            <td>{template.subject ? template.subject : null}</td>
            <td>{template.type.charAt(0).toUpperCase() + template.type.substr(1)}</td>
            <td>{template.last_modified ? moment.unix(template.last_modified).format('MMM Do YYYY, h:mm:ss a') : null}</td>
            <td>
              <IconButton
                onClick={(e) => { e.stopPropagation(); this._prepEditTemplateLayer(template.id) }}
              ><EditIcon /></IconButton>
              <IconButton
                onClick={(e) => { e.stopPropagation(); this._removeTemplate(template.id) }}
              ><TrashIcon /></IconButton>
            </td>
          </TableRow>
        );
      });
    return (
      <div className = {'pageContent'}>
        <HeaderBar />
        <div className={'contentWrapper'}>
        </div>
        <div  className={'contentWrapper zIndex2'}>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <CustomTableCell>Template Name</CustomTableCell>
                  <CustomTableCell>Description</CustomTableCell>
                  <CustomTableCell>Type</CustomTableCell>
                  <CustomTableCell>Last Modified</CustomTableCell>
                  <CustomTableCell>Actions</CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {templatesList}
              </TableBody>
            </Table>
          </Paper>
          <IconButton
            variant="fab"
            size="large"
            color="primary"
            aria-label="Add"
            className={classes.addButton}
            onClick={this._prepTemplateLayer}
          >
            <AddIcon />
          </IconButton>
        </div>
        {this.state.showLayer ? this.state.layer : null}
      </div>
    );
  }
}

const select = state => ({
  templates: state.templates.list
});

export default withStyles(styles)(connect(select)(ManageTemplates));
