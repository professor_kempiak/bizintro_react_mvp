import React, {Component} from 'react';
import {connect} from 'react-redux';

import Box from 'grommet/components/Box';
import Tab from 'grommet/components/Tab';
import Tabs from 'grommet/components/Tabs';
import Article from 'grommet/components/Article';
// import Card from 'grommet/components/Card';

import HeaderBar from '../components/HeaderBar';
import AddContactLayer from '../components/AddContactLayer';
import EditContactLayer from '../components/EditContactLayer';
import UploadContactsLayer from '../components/UploadContactsLayer';
import MakeIntroductionLayer from '../components/MakeIntroductionLayer';
import Heading from 'grommet/components/Heading';
import Anchor from 'grommet/components/Anchor';
import Label from 'grommet/components/Label';
import Paragraph from 'grommet/components/Paragraph';
import Pulse from 'grommet/components/icons/Pulse'
import MapIcon from 'grommet/components/icons/base/Map'
import MailIcon from 'grommet/components/icons/base/Mail'
import Hero from 'grommet/components/Hero';
import Image from 'grommet/components/Image';
import Accordion from 'grommet/components/Accordion';
import AccordionPanel from 'grommet/components/AccordionPanel';
// import Button from 'grommet/components/Button';
import Menu from 'grommet/components/Menu';

import {Timeline, TimelineEvent} from 'react-event-timeline'

import {initialize} from '../actions/session';
import {gatherContacts} from '../actions/contacts';
import {updateSearchTags, updateSearchTerm} from '../actions/search';
import {remoteContactSearch} from '../actions/contacts';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
};

class ContactDetails extends Component {

  constructor(props) {
    super(props);

    this.state = {
      layer: null,
      showLayer: false,
      activeTab: 0
    }
    this._setActiveTab = this._setActiveTab.bind(this);
  }

  _handleSearchInputChange(value) {
    let {dispatch, search} = this.props;

    if (value.length > 2) {
      if (this.state.activeTab !== 1) {
        this.setState({activeTab: 1})
      }
    }

    let searchTagsAndTerm = search.map((tag) => {
      return {name: tag}
    });
    searchTagsAndTerm.push({name: value});

    dispatch(updateSearchTerm(value));
    dispatch(remoteContactSearch(searchTagsAndTerm, 1, localStorage.getItem('token')));
  }

  _setActiveTab(index) {
    this.setState({activeTab: index});
  }

  componentWillMount() {
    this.props.dispatch(initialize());
  }

  componentDidMount() {
  }

  render() {
    const { classes } = this.props;
    return (
      <div primary={true} className={'contact-detail pageContent'}>
        {this.state.toasts}
        <HeaderBar
          searchInputChange={this._handleSearchInputChange.bind(this)}/>
        {this.state.notifications}
        <div pad='medium' className={'contentWrapper'}>
          <Tabs activeIndex={this.state.activeTab} justify='start'>
            <Tab title='Information' onClick={() => this._setActiveTab(0)}>
              <div className='information-wrapper'>
                <div>
                  {/* <Card
                    colorIndex="light-1"
                    margin="none"
                    thumbnail={'img/carousel-1.png'}
                    direction="column"
                    className='nudgeBackground'
                    label="">
                    <Heading tag="h6" className={'main-info'}>
                      thomas.stevenson@bizintroapps.com
                    </Heading>
                    <Heading tag="h6" className={'main-info'}>
                      313.563.2215
                    </Heading>
                    <Heading tag="h6" className={'main-info'}>
                      12561 Chaleston Ave.<br></br>Chicago, IL 60061
                    </Heading>
                  </Card> */}
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.media}
                      image='img/carousel-1.png'
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Typography component="h6" className={'main-info'}>
                        thomas.stevenson@bizintroapps.com
                      </Typography>
                      <Typography component="h6" className={'main-info'}>
                        313.563.2215
                      </Typography>
                      <Typography component="h6" className={'main-info'}>
                        12561 Chaleston Ave.<br></br>Chicago, IL 60061
                      </Typography>
                    </CardContent>
                  </Card>
                </div>
                <div>
                  <div>
                    <div>
                      {/* <Card heading='Tomas Stevenson'
                            description={
                              <div margin={'none'}>
                                <Paragraph margin={'none'} size={'large'}>CEO | TOP25</Paragraph>
                                <Paragraph size={'large'} margin={'none'}>AT Bizinitro Inc</Paragraph>
                                <div direction='row'
                                     justify='start'
                                     align='center'
                                     wrap={true}>
                                  <div direction='row'
                                       justify='start'
                                       align='center'
                                       wrap={true}>
                                    <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                            onClick={this.buttonClick}>
                                    TECH
                                    </Button>
                                  </div>
                                  <div direction='row'
                                       justify='start'
                                       align='center'
                                       wrap={true}>
                                    <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                            onClick={this.buttonClick}>
                                    CEO
                                    </Button>
                                  </div>
                                  <div direction='row'
                                       justify='start'
                                       align='center'
                                       wrap={true}>
                                    <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                            onClick={this.buttonClick}>
                                    BIZINITRO
                                    </Button>
                                  </div>
                                </div>
                              </div>}/> */}
                      <Card className={classes.card}>
                        <CardMedia
                          className={classes.media}
                          title="Tomas Stevenson"
                        />
                        <CardContent>
                          <div margin={'none'}>
                            <Paragraph margin={'none'} size={'large'}>CEO | TOP25</Paragraph>
                            <Paragraph size={'large'} margin={'none'}>AT Bizinitro Inc</Paragraph>
                            <div direction='row'
                              justify='start'
                              align='center'
                              wrap={true}>
                              <div direction='row'
                                justify='start'
                                align='center'
                                wrap={true}>
                                <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                  onClick={this.buttonClick}>
                                  TECH
                                    </Button>
                              </div>
                              <div direction='row'
                                justify='start'
                                align='center'
                                wrap={true}>
                                <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                  onClick={this.buttonClick}>
                                  CEO
                                    </Button>
                              </div>
                              <div direction='row'
                                justify='start'
                                align='center'
                                wrap={true}>
                                <Button variant="contained" size="large" color="primary" className={'greenBackground'}
                                  onClick={this.buttonClick}>
                                  BIZINITRO
                                    </Button>
                              </div>
                            </div>
                          </div>
                        </CardContent>
                      </Card>
                    </div>
                    <div direction='row'
                         basis='3/4'
                         className={'action-wrap'}
                         wrap={true}
                         justify='start'>
                      <div direction='row'
                           justify='center'
                           align='center'
                           basis={'1/4'}
                           wrap={true}
                           pad={'small'}
                           colorIndex='light-1'>
                        <Menu responsive={false}
                              label='Rank'
                              inline={false}
                              primary={false}
                              className={'nudgeBackground'}
                              closeOnClick={true}>
                          <MenuItem><a href='#'
                                  className='active'/>
                            First
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Second
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Third
                          </MenuItem>
                        </Menu>
                      </div>
                      <div direction='row'
                           justify='center'
                           align='center'
                           wrap={true}
                           pad={'small'}
                           basis={'1/4'}
                           colorIndex='light-1'>
                        <Menu responsive={false}
                              label='Help'
                              inline={false}
                              primary={false}
                              className={'nudgeBackground'}
                              closeOnClick={true}>
                          <MenuItem><a href='#'
                                  className='active'/>
                            First
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Second
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Third
                          </MenuItem>
                        </Menu>
                      </div>
                      <div direction='row'
                           justify='center'
                           align='center'
                           wrap={true}
                           basis={'1/4'}
                           pad={'small'}
                           colorIndex='light-1'>
                        <Menu responsive={false}
                              label='Calendar'
                              inline={false}
                              primary={false}
                              className={'nudgeBackground'}
                              closeOnClick={true}>
                          <MenuItem><a href='#'
                                  className='active'/>
                            First
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Second
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Third
                          </MenuItem>
                        </Menu>
                      </div>
                      <div direction='row'
                           justify='center'
                           align='center'
                           basis={'1/4'}
                           wrap={true}
                           pad={'small'}
                           colorIndex='light-1'>
                        <Menu responsive={true}
                              label='Edit'
                              inline={false}
                              primary={false}
                              className={'nudgeBackground'}
                              closeOnClick={true}>
                          <MenuItem><a href='#'
                                  className='active'/>
                            First
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Second
                          </MenuItem>
                          <MenuItem><a href='#'/>
                            Third
                          </MenuItem>
                        </Menu>
                      </div>
                    </div>
                  </div>
                  <div margin={'small'}
                       justify='start'
                       pad={'small'}>
                    <Accordion openMulti={true}>
                      <AccordionPanel heading='First Title'>
                        <Paragraph>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                          laboris nisi ut aliquip ex ea commodo consequat.
                        </Paragraph>
                      </AccordionPanel>
                      <AccordionPanel heading='Second Title'>
                        <Paragraph>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                          laboris nisi ut aliquip ex ea commodo consequat.
                        </Paragraph>
                      </AccordionPanel>
                      <AccordionPanel heading='Third Title'>
                        <Paragraph>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                          laboris nisi ut aliquip ex ea commodo consequat.
                        </Paragraph>
                      </AccordionPanel>
                    </Accordion>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab title='History' onClick={() => this._setActiveTab(1)}>
              <Timeline>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MapIcon size={'xsmall'} className='icon-map'/>}
                  iconColor="#01a982"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#01a982", color: "#ffffff"}}>
                  Card as timeline event with custom container and header styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MailIcon size={'xsmall'} className='icon-mail'/>}
                  iconColor="#425563"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#425563", color: "#ffffff"}}>
                  A simple card with sensible defaults for styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MapIcon size={'xsmall'} className='icon-map'/>}
                  iconColor="#01a982"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#01a982", color: "#ffffff"}}>
                  Card as timeline event with custom container and header styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MailIcon size={'xsmall'} className='icon-mail'/>}
                  iconColor="#425563"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#425563", color: "#ffffff"}}>
                  A simple card with sensible defaults for styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MapIcon size={'xsmall'} className='icon-map'/>}
                  iconColor="#01a982"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#01a982", color: "#ffffff"}}>
                  Card as timeline event with custom container and header styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MailIcon size={'xsmall'} className='icon-mail'/>}
                  iconColor="#425563"
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#425563", color: "#ffffff"}}>
                  A simple card with sensible defaults for styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MapIcon size={'xsmall'} className='icon-map'/>}
                  iconColor="#01a982"
                  buttons={<i/>}
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#01a982", color: "#ffffff"}}>
                  Card as timeline event with custom container and header styling
                </TimelineEvent>
                <TimelineEvent
                  title="Introduction to Redux in React applications"
                  createdAt="2016-09-12 10:06 PM"
                  icon={<MailIcon size={'xsmall'} className='icon-mail'/>}
                  iconColor="#425563"
                  buttons={<i/>}
                  container="card"
                  style={{boxShadow: "0 0 6px 1px #ffffff", border: "1px solid #777", borderRadius: '3px'}}
                  cardHeaderStyle={{backgroundColor: "#425563", color: "#ffffff"}}>
                  A simple card with sensible defaults for styling
                </TimelineEvent>
              </Timeline>
            </Tab>
          </Tabs>
        </div>
        {this.state.showLayer ? this.state.layer : null}
      </div>
    );
  }
}

const select = state => ({
  contacts: state.contacts,
  session: state.session,
  search: state.search.tags
});

export default withStyles(styles)(connect(select)(ContactDetails));
