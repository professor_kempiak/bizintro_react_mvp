import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { registerUser } from '../actions/session';
import { addToast } from '../actions/toasts';
import LegalLayer from '../components/LegalLayer';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    width: '100% !important',
    margin: '8px 0',
  },
  button: {
    margin: theme.spacing.unit,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center'
  }
});

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      pass_a: '',
      pass_b: '',
      checkStatus: false,
      showLayer: false,
      layer: null,
      showPassword: false,
    }

    this._submitForm = this._submitForm.bind(this);
    this._toggleLayer = this._toggleLayer.bind(this);
    this._changeCheckBox = this._changeCheckBox.bind(this);
    this._prepLegalLayer = this._prepLegalLayer.bind(this);
    this._handleInputChange = this._handleInputChange.bind(this);
  }

  _handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  _changeCheckBox(){
      this.setState({
          checkStatus : !this.state.checkStatus
      })
  }

  _submitForm() {
      if(!this.state.checkStatus){
          this.props.dispatch(addToast('Please check terms and conditions', 'critical'));
          return;
      }
    let user = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.pass_a
    }
    
    this.props.dispatch(registerUser(user, (payload) => {
      if (payload.success) {
        this.props.handleClose();
        this.props.dispatch(addToast(payload.message, 'ok'));
      } else {
        this.props.dispatch(addToast(payload.message, 'critical'));
      }
    }));
    this.setState({
        first_name: '',
        last_name: '',
        email: '',
        pass_a: '',
        pass_b: ''
    })
  }

  _prepLegalLayer() {
    this.setState({
      layer: <LegalLayer onClose={this._toggleLayer} />
    })
    this._toggleLayer();
  }

  _toggleLayer() {
    this.setState({
      showLayer: !this.state.showLayer
    })
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  render() {
    let errors = null;
    if (this.state.pass_a !== this.state.pass_b) { errors = 'Passwords do not match.'; }
    return (
      <Drawer anchor="right" open={this.props.open} onClose={this.props.handleClose} className={'signUpSlide'}>
        <div className="drawer-content">
          <p className={'main-title'}>
            Sign Up
          </p>
          <div className={'form-content'}>
            <div className={'first-name'}>
              <InputLabel htmlFor="first_name">First Name</InputLabel>
              <TextField
                // label="Useremail"
                id="firstname"
                name="first_name"
                placeholder="John"
                value={this.state.first_name}
                onChange={this._handleInputChange}
              />
            </div>
            <div className={'last-name'}>
              <InputLabel htmlFor="last_name">Last Name</InputLabel>
              <TextField
                // label="Useremail"
                id="lastname"
                name="last_name"
                placeholder="Doe"
                value={this.state.last_name}
                onChange={this._handleInputChange}
              />
            </div>
            <div className={'email'}>
              <InputLabel htmlFor="email">E-Mail Address</InputLabel>
              <TextField
                // label="Useremail"
                id="email"
                name="email"
                placeholder="user@domain.com"
                value={this.state.email}
                onChange={this._handleInputChange}
              />
            </div>
            <div className={'password'}>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                id="password"
                name="pass_a"
                type={this.state.showPassword ? 'text' : 'password'}
                value={this.state.pass_a}
                onChange={this._handleInputChange}
              />
            </div>
            <div className={'confirm-password'}>
              <InputLabel htmlFor="confirm_password">Confirm Password</InputLabel>
              <Input
                id="confirm-password"
                name="pass_b"
                type={this.state.showPassword ? 'text' : 'password'}
                value={this.state.pass_b}
                onChange={this._handleInputChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this.handleClickShowPassword}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </div>
            <div className={'terms-and-conditions'}><Checkbox /> Click here to agree to the Bizintro Terms and Conditions .</div>
            <Button onClick={errors || !this.state.checkStatus ? null : this._submitForm} className="submit-btn" color="seconday">Sign Up</Button>
            <MenuItem><a href='#' onClick={() => { this.props.handleClose(); }}>Already a Bizintro user? Click here to sign in</a></MenuItem>
          </div>
        </div>
      </Drawer>
        // <Sidebar fixed={true} size='large' className='signUpSlide'>
        //   { this.state.showLayer ? this.state.layer : null }
        //   <Split flex='left' separator={true}>
        //     <Article>
        //       <Section
        //         full={true}
        //         colorIndex='dark'
        //         texture='url(img/login.png)'
        //         pad='large'
        //         justify='center'
        //         align='center' >
        //         <Heading tag='h1' strong={true}>Take a moment that matters</Heading>
        //       </Section>
        //     </Article>
        //     <Box justify='center' align='center'  full={'vertical'} pad='none' colorIndex='neutral-1'>
        //       <Form pad='medium'>
        //         <Heading align='center'>Sign Up</Heading>
        //         <FormFields>
        //           <FormField label='First Name' htmlFor='first_name'>
        //             <TextInput name='first_name' placeHolder='John' value={this.state.first_name} onDOMChange={this._handleInputChange} />
        //           </FormField>
        //           <FormField label='Last Name' htmlFor='last_name'>
        //             <TextInput name='last_name' placeHolder='Doe' value={this.state.last_name} onDOMChange={this._handleInputChange} />
        //           </FormField>
        //           <FormField label='E-Mail Address' htmlFor='email'>
        //             <TextInput name='email' placeHolder='user@domain.com' value={this.state.email} onDOMChange={this._handleInputChange} />
        //           </FormField>
        //           <FormField label='Password' htmlFor='pass_a' error={errors}>
        //             <PasswordInput name='pass_a' value={this.state.pass_a} onChange={this._handleInputChange} />
        //           </FormField>
        //           <FormField label='Password Confirmation' htmlFor='pass_b' error={errors}>
        //             <PasswordInput name='pass_b' value={this.state.pass_b} onChange={this._handleInputChange} />
        //           </FormField>
        //         </FormFields>
        //         <Box direction='row'
        //              justify='start'
        //              align='start'
        //              wrap={true}
        //              reverse={false}
        //              className={'legalCheckbox'}>
        //             <Box direction='row'
        //                  justify='start'
        //                  align='start'
        //                  wrap={true}>
        //                 <CheckBox  onChange={this._changeCheckBox}/>
        //             </Box>
        //             <Box direction='row'
        //                  justify='start'
        //                  align='start'
        //                  wrap={true}>
        //                 <Anchor onClick={this._prepLegalLayer} label='Click here to agree to the Bizintro Terms and Conditions .' />
        //             </Box>
        //         </Box>
        //         <Box pad={{"vertical": "small"}}>
        //           <Button onClick={errors || !this.state.checkStatus ? null : this._submitForm} label='Sign up' primary={true} fill={true} />
        //         </Box>
        //         <Anchor label='Already a Bizintro user? Click here to sign in' href='#' onClick={() => { this.props.handleClose(); }}/>
        //       </Form>
        //     </Box>
        //   </Split>
        // </Sidebar>
    );
  }
}

const select = state => ({
  session: state.session
});

export default withStyles(styles)(connect(select)(SignUp));
