var express = require('express');
var request = require('request');
var formidable = require('formidable');
var FormData = require('form-data');
var fs = require('fs');

// Router for /api/
const router = express.Router();

// Domain Variable0
// const baseurl = 'http://localhost:8000';
const baseurl = 'https://apimvp.bizintro.com';
// const baseurl = 'http://18.219.209.173:8000';

// POST /api/verifyLogin
router.post('/verifyLogin', (req, res) => {
  const { email, password } = req.body;
  var token = '';
  var options = {
    method: 'POST',
    // url: baseurl + '/account/login',
    url: baseurl + '/account/login',
    headers: {
      'key': 'Content-Type',
      'value': 'application/json',
      'description': '',
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: {
      username: email,
      password: password
    },
    json: true
  };

  request(options, function (error, response, body) {
    console.log(error)
    if (!email || !password || email === 'error') {
      res.statusMessage = 'Invalid email or password';
      res.status(401).end();
    } else {
      if (body.token != '' && response.statusCode == 200) {
        const name = email.split('@')[0].replace(/\.|_/, ' '); // simulated
        const now = new Date();
        const token = body.token; // simulated
        const session = { email, name, token };
        res.json(session);
      } else {
        res.statusMessage = 'Your email address or password is incorrect.';
        res.status(401).end();
      }
    }
  });
});

// POST /api/createContact
router.post('/createContact', (req, res) => {
  const fname = req.body.fname;
  const lname = req.body.lname;
  const email = req.body.email;
  const mobile = req.body.mobile;
  const address = req.body.address;
  const title = req.body.title;
  const company = req.body.company;
  const token = req.body.token;
  const bio = req.body.bio;
  const tags = req.body.tags;

  const postdataset = {
    first_name: fname,
    last_name: lname,
    emails: [
      email
    ],
    primary_email: email,
    phone_numbers: [
      mobile
    ],
    primary_phone_number: mobile,
    addresses: [
      address
    ],
    primary_address: address,
    title: title,
    company: company,
    bio: bio,
    tags: tags.split(',')
  };

  var options = {
    method: 'POST',
    url: baseurl + '/contact/',
    headers: {
      'Authorization': "Token " + token,
      'Content-Type': 'application/json',
      'User-Agent': "request"
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    console.log(error);
    res.json(body);
  });
});

// POST /api/updateContact
router.post('/updateContact', (req, res) => {
  let contact = req.body.contact;

  var options = {
    method: 'PUT',
    url: baseurl + '/contact/' + contact.id,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: contact,
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/getContact
router.post('/getContact', (req, res) => {
  var options = {
    method: 'GET',
    url: 'https://' + domain + '/contact/' + req.body.id,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }

  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/getContacts
router.post('/getContacts', (req, res) => {

  var options = {
    method: 'GET',
    url: baseurl + '/contact/list?page=' + req.body.page + '&perpage=25',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});


// POST /api/makeIntroductions
router.post('/makeIntroductions', (req, res) => {
  const contact_a_id = req.body.contact_a_id;
  const contact_b_id = req.body.contact_b_id;
  const recipient_a_email = req.body.recipient_a_email;
  const recipient_b_email = req.body.recipient_b_email;
  const message = req.body.msg_a;
  const message2 = req.body.msg_b;
  const getToken = req.body.token;

  const postdataset = {
    "contact_a_id": contact_a_id,
    "contact_b_id": contact_b_id,
    "recipient_a_email": recipient_a_email,
    "recipient_b_email": recipient_b_email,
    "message": message,
    "message2": message2,
    "potential_intro_uuid": ""
  }
  var options = {
    method: 'POST',
    url: baseurl + '/introduction/',
    headers: {
      'Authorization': "Token " + getToken,
      'Content-Type': 'application/json',
      'User-Agent': "request"
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  });

});


// POST /api/google_code
router.post('/google_code', (req, res) => {
  const { code, clientId, redirect_uri } = req.body;
  var options = {
    method: 'POST',
    url: baseurl + '/api/login/social/token_user/google-oauth2',
    headers: {
      'key': 'Content-Type',
      'value': 'application/json',
      'description': '',
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: {
      code: code,
      clientId: clientId,
      redirect_uri: redirect_uri
    },
    json: true
  };
  var token = '';
  request(options, function (error, response, body) {
    console.log(body)
    if (body) {
      res.json({ status: true, data: body });
    }
    else {
      res.json({ status: false })
    }
  });
});

// POST /api/autoLogin
router.post('/autoLogin', (req, res) => {
  const { uuid, type } = req.body;
  var options = {
    method: 'POST',
    url: baseurl + '/account/autosignuporlogin',
    headers: {
      'Content-Type': 'application/json'
    },
    body: {
      uuid: uuid,
      type: type
    },
    json: true
  };
  var token = '';
  request(options, function (error, response, body) {
    if (!uuid || !type) {
      res.statusMessage = 'Invalid email or password';
      res.status(401).end();
    } else {
      if (body.token != '' && response.statusCode == 200) {
        const token = body.token;
        res.json(body);
      } else {
        res.statusMessage = body.message;
        res.status(401).end();
      }
    }
  });
});


// POST /api/appointmentrequest
router.post('/appointmentrequest', (req, res) => {
  const intro_uuid = req.body.intro_uuid;
  const contact_id = req.body.contact_id;
  const token = req.body.token;
  const postdataset = {
    "intro_uuid": intro_uuid,
    "contact_id": contact_id
  }
  var options = {
    method: 'POST',
    url: baseurl + '/introduction/accept',
    headers: {
      'Authorization': "Token " + token,
      'Content-Type': 'application/json',
      'User-Agent': "request"
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  });

});

router.post('/apptrequest', (req, res) => {

  let {
    intro_uuid,
    contact_id,
    recipient_email,
    appt_length,
    appt_type,
    location,
    appt_title,
    message,
    timeslots_available,
    token } = req.body;

  const postdataset = {
    "intro_uuid": intro_uuid,
    "contact_id": contact_id,
    "recipient_email": recipient_email,
    "appt_length": appt_length,
    "appt_type": appt_type,
    "location": location,
    "title": appt_title,
    "message": message,
    "timeslots_available": timeslots_available
  }
  var options = {
    method: 'POST',
    url: baseurl + '/appt_request/',
    headers: {
      'Authorization': "Token " + token,
      'Content-Type': 'application/json',
      'User-Agent': "request"
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  });
});


router.post('/appt_request_list', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/appt_request/list',
    headers: {
      'Authorization': 'Token ' + req.body.token,
    },
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});

router.post('/appt_list', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/appt_request/all_list',
    headers: {
      'Authorization': 'Token ' + req.body.token,
    },
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});

router.post('/appt_reRequest', (req, res) => {
  const { uuid } = req.body;
  const postdataset = { uuid };
  var options = {
    method: 'POST',
    url: baseurl + '/appt_request/rerequest',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json'
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});


router.post('/appointmentAccept', (req, res) => {
  let { uuid, timeslot, token } = req.body;

  const postdataset = {
    "uuid": uuid,
    "timeslot": timeslot
  }
  var options = {
    method: 'POST',
    url: baseurl + '/appt_request/accept',
    headers: {
      'Authorization': "Token " + token,
      'Content-Type': 'application/json',
      'User-Agent': "request"
    },
    body: postdataset,
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  });

});


// POST /api/introduction_list
router.post('/introduction_list', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/introduction/list',
    headers: {
      'Authorization': 'Token ' + req.body.token,
    },
    json: true
  };
  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/manageIntroductions
router.post('/manageIntroductions', (req, res) => {
  let q_str = '';

  if (req.body.page && req.body.perpage) {
    q_str = '?page=' + req.body.page + '&perpage=' + req.body.perpage;
  }
  var options = {
    method: 'GET',
    url: baseurl + '/introduction/manage' + q_str,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/nudgeIntroduction
router.post('/nudgeIntroduction', (req, res) => {
  const { intro_uuid } = req.body;

  var options = {
    method: 'POST',
    url: baseurl + '/introduction/nudge',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json'
    },
    body: { intro_uuid },
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/archiveIntroduction
router.post('/archiveIntroduction', (req, res) => {
  const { intro_id } = req.body;

  var options = {
    method: 'DELETE',
    url: baseurl + '/introduction/' + intro_id,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json'
    },
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});


// POST /api/importContacts
router.post('/importContacts', (req, res) => {
  // parse a file upload using formidable since body-parser does not support multiform data (files)
  var form = new formidable.IncomingForm();
  var formData = {}

  form.on('field', function (name, value) {
    formData[name] = value;
  });

  form.on('file', function (name, file) {
    formData[name] = fs.createReadStream(file.path);
  });

  form.parse(req, function (err, fields, files) {
    var options = {
      method: 'POST',
      url: baseurl + '/contact/import',
      headers: {
        'Authorization': 'Token ' + formData.token
      },
      formData: formData
    };

    request(options, function (error, response, body) {
      res.json(body);
    });
  });

});


router.post('/registerUser', (req, res) => {

  var options = {
    method: 'POST',
    url: baseurl + '/account/signup',
    headers: {
      'key': 'Content-Type',
      'value': 'application/json',
      'description': '',
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password,
      timezone: "America/New_York"
    },
    json: true
  };
  var token = '';
  request(options, function (error, response, body) {
    if (!req.body.email || !req.body.password || req.body.email === 'error') {
      let error = {
        success: false,
        message: 'Invalid email or password'
      }
      res.json(error);
    } else {
      if (body.token !== '' && response.statusCode == 200) {
        let message = {
          success: true,
          message: 'Account created successfully!'
        }
        res.json(message);
      } else {
        let error = {
          success: false,
          message: body.message
        }
        res.json(error);
      }
    }
  });
});

// GET /api/fetchTemplates
router.post('/fetchTemplates', (req, res) => {
  let extra = '';
  if (req.body.type) { extra = '?type=' + req.body.type }

  var options = {
    method: 'GET',
    url: baseurl + '/template/list' + extra,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/createTemplate
router.post('/createTemplate', (req, res) => {

  var options = {
    method: 'POST',
    url: baseurl + '/template/',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: req.body.template,
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// PUT /api/updateTemplate
router.post('/updateTemplate', (req, res) => {
  let template = req.body.template;

  var options = {
    method: 'PUT',
    url: baseurl + '/template/' + template.id,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: template,
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// DELETE /api/deleteTemplate
router.post('/deleteTemplate', (req, res) => {

  var options = {
    method: 'DELETE',
    url: baseurl + '/template/' + req.body.id,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/searchContacts
router.post('/searchContacts', (req, res) => {

  let tagString = '';
  req.body.tags.forEach((tag) => {
    tagString += 'term=' + tag + '&';
  });

  var options = {
    method: 'GET',
    url: baseurl + '/contact/search?page=' + req.body.page + '&perpage=25&' + tagString + '&rank=' + req.body.rank,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// POST /api/inviteToFillProfile
router.post('/inviteToFillProfile', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/contact/invite/' + req.body.id,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    json: true
  }

  request(options, function (error, response, body) {
    res.json(body);
  });
});

// GET /map/zone
router.post('/fetchZones', (req, res) => {

  var options = {
    method: 'GET',
    url: baseurl + '/map/zone',
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
});

// POST /api/verifyToken
router.post('/verifyToken', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/account/verify_user/' + req.body.id,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  }

  request(options, function (error, response, body) {
    res.json(body);
  });
});

// POST /api/createZone
router.post('/createZone', (req, res) => {

  var options = {
    method: 'POST',
    url: baseurl + '/map/zone',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: req.body.zone,
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
});

router.post('/updateProfile', (req, res) => {
  var options = {
    method: "POST",
    url: baseurl + '/account/update_profile',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: req.body.user,
    json: true
  }

  request(options, function (error, response, body) {
    res.json(body);
  })
});

// PUT /api/updateZone
router.post('/updateZone', (req, res) => {
  let zone = req.body.zone;

  var options = {
    method: 'PUT',
    url: baseurl + '/map/zone/' + zone.id,
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    body: zone,
    json: true
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
})

router.post('/get_events', (req, res) => {
  var options = {
    method: "GET",
    url: baseurl + '/calendar/event_list',
    headers: {
      'Authorization': 'Token ' + req.body.token,
      'content-type': 'application/json',
      'User-Agent': "request"
    },
    json: true
  }

  request(options, function (error, response, body) {
    res.json(body);
  });
});

// DELETE /api/deleteTemplate
router.post('/deleteZone', (req, res) => {

  var options = {
    method: 'DELETE',
    url: baseurl + '/map/zone/' + req.body.id,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
})

// GET /api/tours
router.post('/tours', (req, res) => {

  var options = {
    method: 'GET',
    url: baseurl + '/tour/list',
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
})

// GET /api/email templates
router.post('/main_templates', (req, res) => {
  var options = {
    method: 'GET',
    url: baseurl + '/template/main?type=' + req.body.type,
    headers: {
      'Authorization': 'Token ' + req.body.token
    }
  };

  request(options, function (error, response, body) {
    res.json(body);
  });
})

router.post('/saveRank', (req, res) => {
  var options = {
    method: 'POST',
    url: baseurl + '/contact/rank',
    headers: {
      'Authorization': 'Token ' + req.body.token 
    },
    body: req.body.rankArray,
    json: true
  }

  request(options, function (error, response, body) {
    res.json(body);
  });
})

module.exports = router;
